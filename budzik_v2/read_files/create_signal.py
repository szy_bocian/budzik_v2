import numpy as np
from read_files import read_config_file
import sys
import mne
import os

# todo - obecnie zrobione jest tak, że wszystkie podane kanały są identyczne, a dobrze by było zrobić, aby każdy kanał się różnił od siebie, bo podaje mu się różne zmienne do budowy

#~~~~~~~~~~~~~~~~~~~~~~~~~~create signal~~~~~~~~~~~~~~~~~~~~~~~~~~
def create_signal(signal_seed):
    '''
    Creating simulated signal with info about simulation in signal_seed. This signal will have events (simulated avtivity
    evoked to the stimulus) and background (brain activity independent from stimulus and environment noise). After
    simulation, signal will be saved to the -raw.fif file provide by output_name and output_path in the seed file.

    Parameters
    ----------
    signal_seed: dict
        Seed with every nesesery variable to create events and background for simulated signal.
    '''

    signal_seed = read_config_file(signal_seed)

    if signal_seed['meta_parameters']['seed'] != None:
        np.random.seed(signal_seed['meta_parameters']['seed'])

    try:
        Fs = signal_seed['meta_parameters']['sampling_frequency']
        T = signal_seed['meta_parameters']['signal_length']
        channels_names = signal_seed['meta_parameters']['channels_names']
        channels_types = signal_seed['meta_parameters']['channels_types']
        eeg_cap = signal_seed['meta_parameters']['eeg_cap']
    except:
        sys.exit("WRONG METAPARAMETERS: Provide sampling frequency, length in seconds, channels names, channels type and type of eeg cap for singal.")

    try:
        output_path = signal_seed["meta_parameters"]["output_path"]
        output_name = signal_seed["meta_parameters"]["output_name"]
    except:
        sys.exit("ERROR: Provide output path and file name.")

    if not os.path.isdir(output_path):
        os.mkdir(output_path)

    # ~~~~EVENTS~~~~
    signal, _, annotations = create_events(signal_seed)

    #~~~~BACKGROUND~~~~
    signal += create_background(signal_seed)

    #~~~~MNE SIGNAL~~~~
    if "amplitude_multiplayer" in signal_seed["meta_parameters"]:
        signal = signal * signal_seed["meta_parameters"]["amplitude_multiplayer"]

    info = mne.create_info(ch_names=channels_names, ch_types=channels_types, sfreq=Fs)
    info.set_montage(eeg_cap)
    signal_mne = mne.io.RawArray(signal, info)
    signal_mne.set_annotations(annotations)

    signal_mne.save(os.path.join(output_path, output_name+"-raw.fif"), overwrite=True)

    print(f"\n\n###### SIGNAL CREATED ######\n\n")

def create_events(signal_seed):
    '''
    Creating events of the simulated signal with evoked activity.

    Parameters
    ----------
    signal_seed: dict
        Seed with every nesesery variable to create events for simulated signal.

    Returns
    -------
    signal: numpy.ndarray
        Created signal with events with shape as: [number_of_channels, T*Fs].
    stim_channel: numpy.ndarray
        Stimulation channel with length as T*Fs. Shows when events occurred with their id.
    annotations: mne.Annotations
        Info about events - when and where they occurred.
    '''

    #metaparemeters of signal
    try:
        Fs = signal_seed['meta_parameters']['sampling_frequency']
        T = signal_seed['meta_parameters']['signal_length']
        channels_names = signal_seed['meta_parameters']['channels_names']
    except:
        sys.exit("WRONG METAPARAMETERS: Provide sampling frequency, length in seconds for channels of singal.")

    #metaperemeters of events
    try:
        number_of_events = signal_seed['events']['meta_parameters']['number_of_events']
        start_offset = signal_seed['events']['meta_parameters']['start_offset']
        stop_offset = signal_seed['events']['meta_parameters']['stop_offset']
        min_range_between_events = signal_seed['events']['meta_parameters']['min_range_between_events']/2
        rest_segment = signal_seed['events']['meta_parameters']['rest_segment']
    except:
        sys.exit("WRONG EVENTS METAPARAMETERS: Provide number_of_events, starting point (in seconds), ending point (in seconds), min_range_between_events for signal's events and rest_segment for signal without events.")

    #we are adding additional min_range_between_events to create wider windows (we don't want more ripped off elements)
    start_offset -= min_range_between_events #start of events window
    stop_offset += min_range_between_events #stop of event window

    #events names
    events_names = list(signal_seed['events'].keys()) #names of events
    events_names.remove('meta_parameters')

    try:
        events_probability = [signal_seed['events']['meta_parameters'][en + "_percent"] for en in events_names]
    except:
        sys.exit('WRONG EVENTS: Provide probabability of events in section [events.meta_parameters] like "eventname_percent = 0.2" - sum of every eventname_percent should be 1.')

    #prepering stim channels (arrangement of events in signal)
    stim_channel = np.zeros(int(T * Fs))

    events_arrangement = np.random.choice(a = len(events_names), size = number_of_events, replace=True, p=events_probability)+1 #random arrangment of events
    events_index = {event: e+1 for e, event in enumerate(events_names)} #events indexes like 'target': 1

    possible_events = int(np.floor(int(T*Fs)/int((stop_offset - start_offset)*Fs))) #number of possible events (the end of signal could be ripped off)
    possible_events_in_rest = int(np.floor(int(rest_segment*Fs)/int((stop_offset - start_offset)*Fs)))
    if number_of_events >= possible_events - possible_events_in_rest - 1:
        sys.exit(f'WRONG EVENTS: Provide fewer events (check number_of_events parameter in [events.meta_parameters] section) than {possible_events - possible_events_in_rest - 1} for window {start_offset}-{stop_offset} s and signal length {T} s with rest in 0-{rest_segment}s.')
    possible_events_occurence = np.sort(np.random.choice(a=np.arange(possible_events_in_rest+1, possible_events-1), size=number_of_events, replace=False))
    for eo, event_occurence in enumerate(possible_events_occurence):
        stim_channel[int(event_occurence*Fs) - int(start_offset*Fs)] = events_arrangement[eo]

    #prepering signal
    signal = np.zeros([len(channels_names), int(T * Fs)])

    for events_name in events_names:

        if 'all' in signal_seed["events"][events_name].keys():

            signal_pom = np.zeros([np.sum(events_arrangement == events_index[events_name]), int((stop_offset - start_offset)*Fs)])

            for event in range(signal_pom.shape[0]):
                for elements in signal_seed['events'][events_name]['all'].keys():
                    input_dicts = prepare_input_dicts(signal_seed['events'][events_name]['all'][elements]['function_inputs'], signal_seed['events'][events_name]['all'][elements]['num_of_elements'])
                    for element in input_dicts:
                        signal_pom[event, :] = globals()[signal_seed['events'][events_name]['all'][elements]['function']](**element, start_point=start_offset, length=stop_offset-start_offset,  Fs=Fs, signal=signal_pom[event, :], mode="event")

            for ch in range(len(channels_names)):
                for e, event_occurence in enumerate(possible_events_occurence[events_arrangement == events_index[events_name]]):
                    event_occurence = event_occurence * (stop_offset - start_offset) - start_offset #point 0 seconds on the epoch
                    t1 = int((event_occurence+start_offset)*Fs)
                    t2 = t1 + int((stop_offset - start_offset)*Fs)
                    signal[ch, t1:t2] += signal_pom[e, ]

        else:

            if list(signal_seed['events'][events_name].keys()).sort() != channels_names.sort():
                sys.exit(f"WRONG CHANNELS: Provide same channels in background module as in the list channels_names from meta_parameters module. \nchannels in {events_name} = {list(signal_seed['events'][events_name].keys())} \nchannels in metaparameters = {channels_names}")

            for ch_name in signal_seed['events'][events_name].keys():

                signal_pom = np.zeros([np.sum(events_arrangement == events_index[events_name]), int((stop_offset - start_offset)*Fs)])
                ch = channels_names.index(ch_name)

                for event in range(signal_pom.shape[0]):
                    for elements in signal_seed['events'][events_name][ch_name].keys():
                        input_dicts = prepare_input_dicts(signal_seed['events'][events_name][ch_name][elements]['function_inputs'], signal_seed['events'][events_name][ch_name][elements]['num_of_elements'])
                        for element in input_dicts:
                            signal_pom[event, :] = globals()[signal_seed['events'][events_name][ch_name][elements]['function']](**element, start_point=start_offset, length=stop_offset-start_offset,  Fs=Fs, signal=signal_pom[event, :], mode="event")

                for e, event_occurence in enumerate(possible_events_occurence[events_arrangement == events_index[events_name]]):
                    event_occurence = event_occurence * (stop_offset - start_offset) - start_offset #point 0 seconds on the epoch
                    t1 = int((event_occurence+start_offset)*Fs)
                    t2 = t1 + int((stop_offset - start_offset)*Fs)
                    signal[ch, t1:t2] += signal_pom[e, ]

    #preparing ifno about events
    annotations = mne.Annotations(onset=possible_events_occurence * (stop_offset - start_offset) - start_offset,
                                  duration=np.zeros(possible_events_occurence.shape[0]),
                                  description=[{v: k for k, v in events_index.items()}[id] for id in events_arrangement],
                                  ch_names=[channels_names]*possible_events_occurence.shape[0])

    return signal, stim_channel, annotations


def create_background(signal_seed):
    '''
    Creating background activity of the simulated signal.

    Parameters
    ----------
    signal_seed: dict
        Seed with every nesesery variable to create background activity for simulated signal.

    Returns
    -------
    signal: numpy.ndarray
        Created background with shape as: [number_of_channels, T*Fs].
    '''

    # metaparemeters of signal
    try:
        Fs = signal_seed['meta_parameters']['sampling_frequency']
        T = signal_seed['meta_parameters']['signal_length']
        channels_names = signal_seed['meta_parameters']['channels_names']
    except:
        sys.exit("WRONG METAPARAMETERS: Provide sampling frequency, length in seconds or channels of singal.")

    #preparing signal for channels
    signal = np.zeros([len(channels_names), int(T * Fs)])

    # prepare same background for all channels
    if 'all' in signal_seed['background'].keys():

        signal_pom = np.zeros(int(T * Fs))

        for elements in signal_seed['background']['all'].keys():
            input_dicts = prepare_input_dicts(signal_seed['background']['all'][elements]['function_inputs'], signal_seed['background']['all'][elements]['num_of_elements'])
            for element in input_dicts:
                signal_pom = globals()[signal_seed['background']['all'][elements]['function']](**element, Fs=Fs, signal=signal_pom, mode="background")

        for ch in range(len(channels_names)):
            signal[ch, :] = signal_pom

    # prepare background for each channels
    else:

        if list(signal_seed['background'].keys()).sort() != list(channels_names).sort():
            sys.exit(f"WRONG CHANNELS: Provide same channels in background module as in the list channels_names from meta_parameters module. \nchannels in background = {list(signal_seed['background'].keys())} \nchannels in metaparameters = {channels_names}")

        for ch_name in signal_seed['background'].keys():

            signal_pom = np.zeros(int(T * Fs))
            ch = channels_names.index(ch_name)

            for elements in signal_seed['background'][ch_name].keys():
                input_dicts = prepare_input_dicts(signal_seed['background'][ch_name][elements]['function_inputs'], signal_seed['background'][ch_name][elements]['num_of_elements'])
                for element in input_dicts:
                    signal_pom = globals()[signal_seed['background'][ch_name][elements]['function']](**element, Fs=Fs, signal=signal_pom, mode="background")

            signal[ch, :] = signal_pom

    return signal

#~~~~~~~~~~~~~~~~~~~~~~~~~~functions for simulating signal~~~~~~~~~~~~~~~~~~~~~~~~~~
def prepare_input_dicts(input_dict, num_of_elements):
    '''
    Preparing input sets for specific element of the simulated signal.

    For example:

        * input_dict = {A=1, mu=[1, 3, 0.5], sigma=0.1, start_point=0, length=80}
        * num_of_elements = 3
        Then:
        * prepared_input = [
        {A=1, mu=1.0, sigma=0.1, start_point=0, length=80},
        {A=1, mu=2.5, sigma=0.1, start_point=0, length=80},
        {A=1, mu=1.5, sigma=0.1, start_point=0, length=80}
        ]

        The mu parameter is a list, so it will be "random element" of the set. Values of the mu parameter will be drawn
        from the list [1.0, 1.5, 2.0, 2.5] (because starting point is 1.0, ending point is 3.0 and step is 0.5) with the
        same probabilities.

        Other elements of prepared_input will be the same as in input_dict and number of the input sets will be the
        same as num_of_elements.

    Parameters
    ----------
    input_dict: dict
        Dictionary with parameters provided for specific element (for example sin_curve).
    num_of_elements: int
        Number of provided input sets.

    Returns
    -------
    prepared_input: list
        List with prepared dicts of parameters for specific element. Length of this list is equal to num_of_elements.
    '''


    prepared_input = []

    for i in range(num_of_elements):
        help_dict = {}
        for j in input_dict.keys():
            if isinstance(input_dict[j], list):
                help_dict[j] = np.random.choice(np.arange(input_dict[j][0], input_dict[j][1], input_dict[j][2]))
            else:
                help_dict[j] = input_dict[j]
        prepared_input.append(help_dict)

    return prepared_input


def sin_curve(A, f, phi, start_point, length, Fs, signal, mode):
    '''
    Creating sinus wave as:

    sin(t) = A * sin(2*π*f*t/Fs + phi) where t∈[start_offset, start_offset+length]

    Parameters
    ----------
    A: float
        Amplitude of sinus.
    f: float
        Frequency [Hz] of sinus.
    phi: float
        Phase shift of sinus.
    start_point: float
        Time [s] where sinus begin.
    length: float
        Sinus length [s].
    Fs: float
        Sampling frequency [Hz] of the main signal.
    signal: numpy.ndarray
        Signal where sinus will be added.
    mode: str
        They are two modes:
            * background -> Adding sinus to the long signal which represent background activity.
            * event -> Adding sinus to the short signal wich represent evoked activity.

    Returns
    -------
    sig: numpy.ndarray
        Signal with sinus wave.
    '''

    if mode == "background":
        place = [int(start_point*Fs), int((start_point + length)*Fs)]
        place[1] = signal.shape[0] if place[1] > signal.shape[0] else place[1]
        signal[place[0]:place[1]] += A * np.sin(2 * np.pi * f * np.arange(0, length, 1 / Fs) + phi)[:(place[1] - place[0])]

    elif mode == "event":
        signal += A * np.sin(2 * np.pi * f * np.arange(start_point, start_point+length, 1 / Fs) + phi)[:signal.shape[0]]

    else:
        sys.exit(f'ERROR: Wrong mode for sin_curve function.')

    return signal

def gauss_curve(A, mu, sigma, start_point, length, Fs, signal, mode):
    '''
    Creating Gause curve as:

    N(t) = A * exp(-1* (t - mu)**2/(2*sigma**2)) where t∈[start_offset, start_offset+length]

    Parameters
    ----------
    A: float
        Amplitude of the Gauss curve.
    mu: float
        Mean of the Gauss curve.
    sigma: float
        Standard deviation of the Gauss curve.
    start_point: float
        Time [s] where Gauss curve begin.
    length: float
        Gauss curve length [s].
    Fs: float
        Sampling frequency [Hz] of the main signal.
    signal: numpy.ndarray
        Signal where Gauss curve will be added.
    mode: str
        They are two modes:
            * background -> Adding Gauss curve to the long signal which represent background activity.
            * event -> Adding Gauss curve to the short signal wich represent evoked activity.

    Returns
    -------
    sig: numpy.ndarray
        Signal with Gauss curve.
    '''

    if mode == "background":
        place = [int(start_point*Fs), int((start_point + length)*Fs)]
        place[1] = signal.shape[0] if place[1] > signal.shape[0] else place[1]
        signal_pom = A*(1/(sigma*np.sqrt(2*np.pi)))*np.exp(-1* (np.arange(0, length, 1/Fs) - mu)**2/(2*sigma**2))
        signal[place[0]:place[1]] += signal_pom[:(place[1] - place[0])]

    elif mode == "event":
        signal_pom = A*(1/(sigma*np.sqrt(2*np.pi)))*np.exp(-1* (np.arange(start_point, start_point+length, 1/Fs) - mu)**2/(2*sigma**2))
        signal += signal_pom[:signal.shape[0]]

    else:
        sys.exit(f'ERROR: Wrong mode for gauss_curve function.')

    return signal

def gabor_curve(A, f, mu, sigma, phi, start_point, length, Fs, signal, mode):
    '''
    Creating Gabor curve as:

    G(t) = A * exp(-1* (t - mu)**2/(2*sigma**2)) * cos(2*π*f*t/Fs + phi) where t∈[start_offset, start_offset+length]

    Parameters
    ----------
    A: float
        Amplitude of the Gabor curve.
    f: float
        Frequency [Hz] of Gabor curve.
    mu: float
        Mean of the Gabor curve.
    sigma: float
        Standard deviation of the Gabor curve.
    phi: float
        Phase shift of Gabor curve.
    start_point: float
        Time [s] where Gabor curve begin.
    length: float
        Gabor curve length [s].
    Fs: float
        Sampling frequency [Hz] of the main signal.
    signal: numpy.ndarray
        Signal where Gabor curve will be added.
    mode: str
        They are two modes:
            * background -> Adding Gabor curve to the long signal which represent background activity.
            * event -> Adding Gabor curve to the short signal wich represent evoked activity.

    Returns
    -------
    sig: numpy.ndarray
        Signal with Gabor curve.
    '''

    if mode == "background":
        place = [int(start_point * Fs), int((start_point + length) * Fs)]
        place[1] = signal.shape[0] if place[1] > signal.shape[0] else place[1]
        signal_pom = A*(1/(sigma*np.sqrt(2*np.pi)))*np.exp(-1 * (np.arange(0, length, 1/Fs) - mu)**2/(2*sigma**2))*np.cos(2*np.pi*f*np.arange(0, length, 1/Fs) + phi)
        signal[place[0]:place[1]] += signal_pom[:(place[1] - place[0])]

    elif mode == "event":
        signal_pom = A*(1/(sigma*np.sqrt(2*np.pi)))*np.exp(-1 * (np.arange(start_point, start_point + length, 1 / Fs) - mu) ** 2 / (2 * sigma ** 2)) * np.cos(2 * np.pi * f * np.arange(start_point, start_point + length, 1 / Fs) + phi)
        signal += signal_pom[:signal.shape[0]]

    else:
        sys.exit(f'ERROR: Wrong mode for gabor_curve function.')

    return signal

def gauss_noise(A, mu, sigma, start_point, length, Fs, signal, mode):
    '''
    Creating Gaussian noise.

    Parameters
    ----------
    A: float
        Amplitude of the Gaussian noise.
    mu: float
        Mean of the Gaussian noise.
    sigma: float
        Standard deviation of the Gaussian noise.
    start_point: float
        Time [s] where Gaussian noise begin.
    length: float
        Gaussian noise length [s].
    Fs: float
        Sampling frequency [Hz] of the main signal.
    signal: numpy.ndarray
        Signal where Gaussian noise will be added.
    mode: str
        They are two modes:
            * background -> Adding Gaussian noise to the long signal which represent background activity.
            * event -> Adding Gaussian noise to the short signal wich represent evoked activity.

    Returns
    -------
    sig: numpy.ndarray
        Signal with Gabor curve.
    '''


    if mode == "background":
        place = [int(start_point*Fs), int((start_point + length)*Fs)]
        place[1] = signal.shape[0] if place[1] > signal.shape[0] else place[1]
        signal[place[0]:place[1]] += A * np.random.normal(mu, sigma, int(length*Fs))[:(place[1] - place[0])]

    elif mode == "event":
        signal += A * np.random.normal(mu, sigma, int(length*Fs))

    else:
        sys.exit(f'ERROR: Wrong mode for gauss_noise function.')

    return signal


if __name__ == "__main__":

    create_signal(sys.argv[1])
