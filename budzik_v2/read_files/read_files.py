#Szymon Bocian 2022

import os
import sys

import mne
import tomli
from obci_readmanager.signal_processing.read_manager import ReadManager
import numpy as np

#~~~~~~~~~~~~~~~~~~~~~~~~~~config files~~~~~~~~~~~~~~~~~~~~~~~~~~
def read_config_file(config_path):
    '''
    Reads toml config file.

    Parameters
    ----------
    config_path: str
        Path to the toml config file.

    Returns
    -------
    config: dict
        The config dictionary with necessary variables.
    '''

    config = convert_strings(tomli.load(open(config_path, "rb")))  # changing anything strange in config like bool as strings
    config = check_and_prepare_config(config)  # preparing default variables if they are not specified in original config

    return config


def convert_strings(config):
    '''
    Converts strings "None", "True" and "False" to the None and bool type.

    Parameters
    ----------
    config: dict
        Dictionary of configuration variables.

    Returns
    -------
    config: dict
        Dictionary of configuration variables with correct types.
    '''

    for var in config:
        if isinstance(config[var], dict):
            config[var] = convert_strings(config[var])
        elif isinstance(config[var], str):
            if config[var] in ["inf", "np.inf", "oo", "+oo"]:
                config[var] = np.inf
            elif config[var] == "None":
                config[var] = None
            elif config[var] == "True":
                config[var] = True
            elif config[var] == "False":
                config[var] = False
            elif ("," in config[var] or len(config[var].split(" ")) >= 2):
                config[var] = "".join(config[var].split(",")).split(" ")
    return config


def check_and_prepare_config(config):
    '''
    Checking existence of specific keys in config dictionary.

    Parameters
    ----------
    config: dict
        Dictionary of configuration variables.

    Returns
    -------
    config: dict
        Dictionary of configuration variables with correct types.
    '''

    # ~~ meta PARAMS ~~
    # important sets of values for config file

    # dictionary of filters - each filter has name as key and dict of parameters as value
    # (wp, ws, gpass, gstop, ftype, and use_filtfilt) filters are created by scipy.signal package -
    # more: https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.iirdesign.html
    filters_dict = {
        "highpass_01hz": dict(wp=0.1, ws=0.03, gpass=3, gstop=10.81, ftype="butter", use_filtfilt=True),
        "highpass_05hz": dict(wp=0.5, ws=0.25, gpass=3, gstop=6.97, ftype="butter", use_filtfilt=True),
        "highpass_1hz": dict(wp=1, ws=0.5, gpass=3, gstop=6.97, ftype="butter", use_filtfilt=True),

        "lowpass_10hz": dict(wp=10, ws=20, gpass=3, gstop=12.3, ftype="butter", use_filtfilt=True),
        "lowpass_20hz": dict(wp=20, ws=40, gpass=3, gstop=12.4, ftype="butter", use_filtfilt=True),
        "lowpass_30hz": dict(wp=30, ws=60, gpass=3, gstop=12.4, ftype="butter", use_filtfilt=True),

        "stopband_50hz": dict(wp=[47.5, 52.5], ws=[49.9, 50.1], gpass=3, gstop=25, ftype="cheby2", use_filtfilt=True),
        "stopband_60hz": dict(wp=[57.5, 62.5], ws=[59.9, 60.1], gpass=3, gstop=25, ftype="cheby2", use_filtfilt=True)
    }

    # dictionary with parameters for slopes, outliers and muscles artifacts searched
    # by artifacts function in preanalyse\artifacts.py
    rejection_parameters = {
        'SlopeWindow': 0.0704,  # [s] window width for slopes and outliers
        'MusclesWindow': 0.5,  # [s] window width for muscles

        'OutliersMergeWin': .1,  # [s] width window where we are connect points classified as outliers in groups

        'MusclesFreqRange': [40, 90],  # [Hz, Hz] frequencies for muscles detection

        'SlopesAbsThr': 150,  # [µV] absolute value of amplitude peak-to-peak in SlopeWindow
        'SlopesStdThr': 7,  # peak-to-peak in window as multiple of the standard deviation
        'SlopesMedThr': 6,  # peak-to-peak in window as multiple of the median

        'OutliersAbsThr': 150,  # [µV] absolute value of amplitude
        'OutliersStdThr': 7,  # peak-to-peak in window as multiple of the standard deviation
        'OutliersMedThr': 13,  # peak-to-peak in window as multiple of the median

        'MusclesAbsThr': .150,  # [µV²] absolute value of the mean of spectral density for point in the MusclesFreqRange
        'MusclesStdThr': 7,  # amplitude as multiple of the standard deviation
        'MusclesMedThr': 5,  # amplitude as multiple of the median
    }

    # ~~ main PARAMS ~~
    # main params in config

    # [Hz] resampling frequency
    if "resample_freq" not in config.keys():
        config["resample_freq"] = 512

    # [s or None] time before and after group of events - if cover_time = None and threshold_time = None, all signal
    # before first tag (rest or event) minus 5 seconds will be not taken for the rest of the analysis
    # for more, check budzik_v2/preanalyse/channels_and_signal.py/pick_signal
    if "cover_time" not in config.keys():
        config["cover_time"] = None

    # [s or None] checking gaps beetween groups of events, if the gap is longer than this parameter, groups won't be
    # connected - if cover_time = None and threshold_time = None, all signal will before first tag (rest or event)
    # minus 5 seconds will be not taken for the rest of the analysis
    # for more, check budzik_v2/preanalyse/channels_and_signal.py/pick_signal
    if "threshold_time" not in config.keys():
        config["threshold_time"] = None

    # ~~ choosing_channels PARAMS ~~
    # parameters to find bad channels (see ValidateChannels_Noise in budzik_v2/preanalyse/channels_and_signal)

    # "name": [width, band, thresholds_type, thresholds]
    #     width [s] -> width of window to cut signal for segments
    #     band [Hz, Hz] -> frequency band where program will search for bad channels
    #     thresholds_type -> "+" (power above thresholds will be classified as good) or
    #                        "-" (power below thresholds will be classified as good)
    #     thresholds [μV^2, μV^2, μV^2] -> three threshold necessary to find bad channels to check power of
    #                                      cut segments from signal
    if "badchs_params" not in config["choosing_channels"].keys():
        config["choosing_channels"]["badchs_params"] = {"slow_oscilations": [1, [2, 10], "-", [1e3, 1e4, 1e5]],
                                                        "noise_power": [1, [52, 98], "-", [2e1, 2e2, 2e3]]}

    # ~~ filters PARAMS ~~
    # adding variables of filters to the config from filters_dict (in this file) - more about filters variables: https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.iirdesign.html

    # highpass
    if config["filters"]["highpass_name"] not in config["filters"].keys():
        if config["filters"]["highpass_name"] in filters_dict.keys():
            config["filters"][config["filters"]["highpass_name"]] = filters_dict[config["filters"]["highpass_name"]]
        else:
            sys.exit("ERROR: High pass filter is not define. Provide name and variables in filt_dict dictionary in the read_files/read_files.py. Variables should be wp, ws, gpass, gstop, ftype and use_filtfilt. More: https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.iirdesign.html.")

    # lowpass
    if config["filters"]["lowpass_name"] not in config["filters"].keys():
        if config["filters"]["lowpass_name"] in filters_dict.keys():
            config["filters"][config["filters"]["lowpass_name"]] = filters_dict[config["filters"]["lowpass_name"]]
        else:
            sys.exit("ERROR: Low pass filter is not define. Provide name and variables in filt_dict dictionary in the read_files/read_files.py. Variables should be wp, ws, gpass, gstop, ftype and use_filtfilt. More: https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.iirdesign.html.")

    # bandstop
    if config["filters"]["stopband_name"] not in config["filters"].keys():
        if config["filters"]["stopband_name"] in filters_dict.keys():
            config["filters"][config["filters"]["stopband_name"]] = filters_dict[config["filters"]["stopband_name"]]
        else:
            sys.exit("ERROR: Stop band filter is not define. Provide name and variables in filt_dict dictionary in the read_files/read_files.py. Variables should be wp, ws, gpass, gstop, ftype and use_filtfilt. More: https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.iirdesign.html.")


    # ~~ artifacts PARAMS ~~
    if "rejection_parameters" not in config["artifacts"].keys():
        config["artifacts"]["rejection_parameters"] = rejection_parameters


    # ~~ ICA_EOG PARAMS ~~
    # method to calculate ICA - possible: "fastica", "infomax" or "picard"
    if "ica_method" not in config["ICA_EOG"].keys():
        config["ICA_EOG"]["ica_method"] = "infomax"

    # n_components could be: "all" (program will take number of ICA components equal to number of channels), integer number (how many ICA components you will get - it must be equal or smaller than number of channels) or float number (from range [0, 1] and will select the smallest possible number of components required to explain cumulative variance of the signal - more here https://mne.tools/1.3/generated/mne.preprocessing.ICA.html)
    if "n_components" not in config["ICA_EOG"].keys():
        config["ICA_EOG"]["n_components"] = "all"

    # MORE: https://mne.tools/1.3/generated/mne.preprocessing.create_eog_epochs.html
    # names of eog channels - will be taken from possible channels (after dropped by the user and found bad channels)
    if "eog_channels_names" not in config["ICA_EOG"].keys():
        config["ICA_EOG"]["eog_channels_names"] = ["Fp1", "Fp2", "Fpz"]


    # MORE: https://mne.tools/1.3/generated/mne.preprocessing.ICA.html#mne.preprocessing.ICA.score_sources
    # treshold score for ica
    if "score_correlation_treshhold" not in config["ICA_EOG"].keys():
        config["ICA_EOG"]["score_correlation_treshhold"] = 0.5

    # frequency from high pass filter used in calculating score from ICA sources with one of the eog_channels_names
    if "score_l_freq" not in config["ICA_EOG"].keys():
        config["ICA_EOG"]["score_l_freq"] = 1

    # frequency from low pass filter used in calculating score from ICA sources with one of the eog_channels_names
    if "score_h_freq" not in config["ICA_EOG"].keys():
        config["ICA_EOG"]["score_h_freq"] = 9


    # ~~ epochs PARAMS ~~
    # reject dictionary, if peak-to-peak of min-max in epoch is greater than value in reject_dict, then epoch will be rejected
    if "reject_dict" not in config["epochs"].keys():
        config["epochs"]["reject_dict"] = dict(eeg=500e-6)

    # reject dictionary, if peak-to-peak of min-max in epoch is smaller than value in reject_dict, then epoch will be rejected
    if "flat_dict" not in config["epochs"].keys():
        config["epochs"]["flat_dict"] = dict(eeg=1e-6)


    # ~~ time_frequency_domain PARAMS ~~
    # color map for heatmaps of stft and wt - more cmpas: https://matplotlib.org/stable/users/explain/colors/colormaps.html
    if "cmap" not in config["time_frequency_domain"].keys():
        config["time_frequency_domain"]["cmap"] = "jet"

    if "fontsize" not in config["time_frequency_domain"].keys():
        config["time_frequency_domain"]["fontsize"] = 26

    if "figurescale" not in config["time_frequency_domain"].keys():
        config["time_frequency_domain"]["figurescale"] = 10


    return config


#~~~~~~~~~~~~~~~~~~~~~~~~~~signal files~~~~~~~~~~~~~~~~~~~~~~~~~~
def find_file_paths(dirpath):
    '''
    Searching signals in directory.

    Parameters
    ----------
    dirpath: str
        Path of the directory where program will be searching for signals.

    Returns
    -------
    list_of_filepaths: list
        List with paths to the signals found in the input directory.
    '''

    file_extension = [".raw", ".vhdr", ".fif", ".set"]  # formats of the files
    list_of_filepaths = []

    for root, _, files in os.walk(dirpath):
        for file in files:
            if os.path.splitext(file)[1] in file_extension:
                # we don't want signals created by our program
                # todo - zmienić to po zmianie nazwy paczki
                if "budzik_output__" not in os.path.join(root, file):
                    list_of_filepaths.append(os.path.join(root, file))

    return list_of_filepaths


def braintech_format(name):
    '''
    Read file for .raw, .xml and .tag formats provided by BrainTech.

    Parameters
    ----------
    name: str
        Path with name of the file (without .raw format) to the file with .raw.

    Returns
    -------
    mne_signal: mne.io.array.array.RawArray
        The read signal from the mne object with STIM channel. The STIM has occurrence of the events.
    events_desc_id: dict
        Dictionary of the names of the events and their indexes in the STIM channel - this is necessary for mapping
        events the STIM channel. The dict looks like this: {'event_name_1': 1, 'event_name_2': 2, ...}.
    '''

    raw = ReadManager(name + '.xml', name + '.raw', name + '.tag')
    raw = braintech__prepare_tags(raw)
    mne_signal = raw.get_mne_raw()

    if "OBCI_STIM" in mne_signal.info["ch_names"]:
        mne.rename_channels(mne_signal.info, {"OBCI_STIM": "STIM"})

    return mne_signal, raw.mne_events()[1]


def brainvision_format(name):
    '''
    Read file for .vhdr, .vmrk and .eeg formats provided by BrainVision.

    Parameters
    ----------
    name: str
        Path with name of the file (without .vhdr) to the file with .vhdr.

    Returns
    -------
    mne_signal: mne.io.array.array.RawArray
        The read signal from the mne object with STIM channel. The STIM has occurrence of the events.
    events_desc_id: dict
        Dictionary of the names of the events and their indexes in the STIM channel - this is necessary for mapping
        events the STIM channel. The dict looks like this: {'event_name_1': 1, 'event_name_2': 2, ...}.
    '''

    mne_signal = mne.io.read_raw_brainvision(name + '.vhdr')
    stim_raw, events_desc_id = mne.events_from_annotations(mne_signal)
    stim = np.zeros((1, mne_signal[0][0].shape[1]))
    stim[0, stim_raw[:, 0]] = stim_raw[:, 2]
    stim = mne.io.RawArray(stim, mne.create_info(["STIM"], mne_signal.info["sfreq"], ["stim"]))
    mne_signal.load_data().add_channels([stim], force_update_info=True)

    return mne_signal, events_desc_id


def fif_format(name):
    '''
    Read file for .fif format.

    Parameters
    ----------
    name: str
        Path with name of the file (without .fif) to the file with .fif.

    Returns
    -------
    mne_signal: mne.io.array.array.RawArray
        The read signal from the mne object with STIM channel. The STIM has occurrence of the events.
    events_desc_id: dict
        Dictionary of the names of the events and their indexes in the STIM channel - this is necessary for mapping
        events the STIM channel. The dict looks like this: {'event_name_1': 1, 'event_name_2': 2, ...}.
    '''

    mne_signal = mne.io.read_raw_fif(name + ".fif")
    stim_raw, events_desc_id = mne.events_from_annotations(mne_signal)
    stim = np.zeros((1, mne_signal[0][0].shape[1]))
    stim[0, stim_raw[:, 0]] = stim_raw[:, 2]
    stim = mne.io.RawArray(stim, mne.create_info(["STIM"], mne_signal.info["sfreq"], ["stim"]))
    mne_signal.load_data().add_channels([stim], force_update_info=True)

    return mne_signal, events_desc_id


def eeglab_format(name):
    '''
    Read file for .set format.

    Parameters
    ----------
    name: str
        Path with name of the file (without .set) to the file with .set.

    Returns
    -------
    mne_signal: mne.io.array.array.RawArray
        The read signal from the mne object with STIM channel. The STIM has occurrence of the events.
    events_desc_id: dict
        Dictionary of the names of the events and their indexes in the STIM channel - this is necessary for mapping
        events the STIM channel. The dict looks like this: {'event_name_1': 1, 'event_name_2': 2, ...}.
    '''

    mne_signal = mne.io.read_raw_fif(name + ".set")
    stim_raw, events_desc_id = mne.events_from_annotations(mne_signal)
    stim = np.zeros((1, mne_signal[0][0].shape[1]))
    stim[0, stim_raw[:, 0]] = stim_raw[:, 2]
    stim = mne.io.RawArray(stim, mne.create_info(["STIM"], mne_signal.info["sfreq"], ["stim"]))
    mne_signal.load_data().add_channels([stim], force_update_info=True)

    return mne_signal, events_desc_id


def read_file(filepath):
    '''
    Reads file with specific formats.

    Parameters
    ----------
    filepath: str
        Path to the file with signal.

    Returns
    -------
    signal_events: list
        Returns the list with signal in MNE format and events. If format of the file wasn't provide or something
        is wrong with files, function returns [None, None].
    '''

    name, format = os.path.splitext(filepath)

    # BrainTech
    if format == ".raw":
        if not os.path.isfile(name + '.raw') or not os.path.isfile(name + '.tag') or not os.path.isfile(name + '.xml'):
            print(f"\nWARNING: File {name} will be not analyse because the desired formats cannot be found. "
                  f"For BrainTech signal you have to provide files with this formats: .raw, .tag and .xml. "
                  f"Please check whether the files are in the right place and whether they contain any typos "
                  f"(each file should be named the same).\n")
            return [None, None]
        else:
            return braintech_format(name)

    # Brain Vision
    elif format == ".vhdr":
        if not os.path.isfile(name + '.vhdr') or not os.path.isfile(name + '.vmrk') or not os.path.isfile(
                name + '.eeg'):
            print(f"\nWARNING: File {name} will be not analyse because the desired formats cannot be found. "
                  f"For Brain Vision signal you have to provide files with this formats: .vhdr, .vmrk and .eeg. "
                  f"Please check whether the files are in the right place and whether they contain any typos "
                  f"(each file should be named the same).\n")
            return [None, None]
        else:
            return brainvision_format(name)

    # FIF
    elif format == ".fif":
        return fif_format(name)

    # EEGLAB
    elif format == ".set":
        return eeglab_format(name)

    else:
        print(f"\nWARNING: The format of the file {filepath} is not provided. File will be not analyse.\n")
        return [None, None]

#~~~~~~~~~~~~~~~~~~~~~~~~~~helping codes~~~~~~~~~~~~~~~~~~~~~~~~~~
def braintech__prepare_tags(rm):
    '''
    Checking tags for Read Manager signal and moves them.

    Parameters
    ----------
    rm: obci_readmanager.signal_processing.read_manager.ReadManager
        Signal in Read Manager type.

    Returns
    -------
    rm: obci_readmanager.signal_processing.read_manager.ReadManager
        Signal in Read Manager type.
    '''
    shift_tags_rel = 0
    tag_correction_chnls = []
    correction_window = (-0.1, 0.3)
    # tag_correction_chnls = ["Audio"]
    # correction_window = (-0.2, 0.7)
    tag_offset = 0.0
    correction_reverse = False
    correction_thr = None

    if shift_tags_rel != 0:
        braintech__shift_tags_relatively_to_signal_beginnig(rm, shift_tags_rel)

    if tag_correction_chnls:
        braintech__align_tags(rm, tag_correction_chnls,
                              start_offset=correction_window[0],
                              duration=correction_window[1],
                              thr=correction_thr,
                              reverse=correction_reverse,
                              offset=tag_offset)

    return rm


def braintech__shift_tags_relatively_to_signal_beginnig(rm, shift):
    '''
    Marian Dovgialo 2019 from https://gitlab.com/BudzikFUW/budzik-analiza-py-3.git helper_functions\helper_functions.py

    Moves all tags in the rm by some relative length. Each tag is moved by different amount dependent on its position - 
    the further from the beginning of the signal, the more it is shifted.

    Parameters
    ----------
    rm: obci_readmanager.signal_processing.read_manager.ReadManager
        Signal in Read Manager type.
    shift: float
        Shift value.
    '''

    tags = rm.get_tags()
    for tag in tags:
        tag['start_timestamp'] *= 1 + shift
        tag['end_timestamp'] *= 1 + shift
    rm.set_tags(tags)


def braintech__align_tags(rm, tag_correction_chnls, start_offset=-0.1, duration=0.3, thr=None, reverse=False, offset=0):
    '''
    Marian Dovgialo 2019 from https://gitlab.com/BudzikFUW/budzik-analiza-py-3.git helper_functions\helper_functions.py

    Aligns tags in read manager to start of sudden change std => 3 in either tag_correction_chnls list searches for that
    in window [start_offset+tag_time; tag_time+duration]. If no such change occures - does nothing to the tag.

    Parameters
    ----------
    rm: obci_readmanager.signal_processing.read_manager.ReadManager
        Signal in Read Manager type.
    tag_correction_chnls: list
        List of the channles to correct tags.
    start_offset: float
        Start of the tag.
    duration: float
        Duration of the tag.
    thr: float or None
        Threshold to check sudden changes. If None, thr = 3*std + mean.
    reverse: bool
        Searches for end of stimulation.
    offset: float
        Offset in seconds to add forcibly.
    '''
    tags = rm.get_tags()
    Fs = float(rm.get_param('sampling_frequency'))
    trigger_chnl = np.zeros(int(rm.get_param('number_of_samples')))
    for tag_correction_chnl in tag_correction_chnls:
        trigger_chnl += np.abs(rm.get_channel_samples(tag_correction_chnl))
    if not thr:
        thr = 3 * np.std(trigger_chnl) + np.mean(trigger_chnl)
        maksimum = trigger_chnl.max()
        if thr > 0.5 * maksimum:
            thr = 0.5 * maksimum

    for tag in tags:
        start = int((tag['start_timestamp'] + start_offset) * Fs)
        end = int((tag['start_timestamp'] + start_offset + duration) * Fs)
        try:
            if reverse:
                trig_pos_s_r = np.argmax(np.flipud(trigger_chnl[start:end] > thr))
                trig_pos_s = (end - start - 1) - trig_pos_s_r
            else:
                trig_pos_s = np.argmax(
                    trigger_chnl[start:end] > thr)  # will find first True, or first False if no Trues
        except ValueError:
            tag['start_timestamp'] += offset
            tag['end_timestamp'] += offset
            continue
            # Debuging code:
        #        print trig_pos_s, Fs, reverse,
        #        print 'thr', thr, 'value at pos', trigger_chnl[start+trig_pos_s], trigger_chnl[start+trig_pos_s]>thr
        #        pb.plot(np.linspace(0, (end-start)/Fs, len(trigger_chnl[start:end])), trigger_chnl[start:end])
        #        pb.axvline(trig_pos_s/Fs, color='k')
        #        pb.title(str(tag))
        #        pb.show()
        # Debug code end
        if trigger_chnl[start + trig_pos_s] > thr:
            trig_pos_t = trig_pos_s * 1.0 / Fs
            tag_change = trig_pos_t + start_offset
            tag['start_timestamp'] += tag_change
            tag['end_timestamp'] += tag_change
        tag['start_timestamp'] += offset
        tag['end_timestamp'] += offset
    rm.set_tags(tags)


