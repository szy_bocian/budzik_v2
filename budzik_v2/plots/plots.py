import mne
import numpy as np
import matplotlib.pyplot as plt
import os
import scipy.signal as ss

def plot_sig_and_heatmap(ax, signal, heatmap, time, arr_t, arr_f, f_lim_cond, title, time_label, signal_label, freq_label, colorbar_label, config):
    '''
    Prepering plot for specific ax with three parts: heatmap in the middle, time series plot in the bottom and color bar
    of the heatmap on the right.

    Parameters
    ----------
    ax: matplotlib.axes
        Axis of the figure where to plot.
    signal: numpy.ndarray or list
        Set of the signals (for example epochs) or one signal (for example ERP).
    heatmap: numpy.ndarray
        Matrix of the variables for the heatmap plot.
    time: numpy.ndarray
        Variables of the x axis for the time series plot.
    arr_t: numpy.ndarray
        Time positions of the heatmap cells (output of the scipy.signal.spectrogram).
    arr_f: numpy.ndarray
        Frequency positions of the heatmap cells (output of the scipy.signal.spectrogram).
    f_lim_cond: list or None
        List with boolean variables - they will give information which frequency of arr_f will be ploted. (None for WT)
    title: str
        Title of the ax.
    time_label: str
        Label of the horizontal axis for time series plot.
    signal_label: str
        Label of the vertical axis for time series plot.
    freq_label: str
        Label of the vertical axis for heatmap plot.
    colorbar_label: str
        Label of the colorbar.
    config: dict
        Dictionary of configuration variables.
    '''
    cmap = config["time_frequency_domain"]["cmap"]
    # fontsize = config["time_frequency_domain"]["fontsize"] #todo - doadj fontsize

    #współrzędne poszczególnych wykresów na ax
    tfAxes = ax.inset_axes(bounds=[0, 0.125, 0.9, 0.875]) # heatmapa
    sygAxes = ax.inset_axes(bounds=[0, 0, 0.9, 0.125]) # plot
    clbAxes = ax.inset_axes(bounds=[0.95, 0.125, 0.01, 0.875]) # colorbar

    ax.axis("off")  # wyłączenie osi głównego ax

    # heatmapa
    if isinstance(f_lim_cond, np.ndarray):
        if len(arr_f[f_lim_cond]) != 1:
            arr_f = arr_f[f_lim_cond]
        else:
            #gdyby nasze warunki wyswietlenia byly zbyt "male", to bierzemy jeszcze jedna wartosc do przodu
            f_lim_cond[np.where(f_lim_cond)[0]+1] = True
            arr_f = arr_f[f_lim_cond]

        heatmap = heatmap[f_lim_cond, :]

    im = tfAxes.imshow(heatmap, aspect='auto', interpolation=None, origin='lower',
                       cmap=cmap, extent=(time[0], arr_t[-1], arr_f[0], arr_f[-1]))
                       #vmin=min_h, vmax=max_h) #todo - vmin i vmax
    tfAxes.tick_params(
        axis='x', which='both', bottom=False, top=False, labelbottom=False)
    if freq_label != None:
        tfAxes.set_ylabel(freq_label)

    # plot
    if len(np.shape(signal)) == 1:
        sygAxes.plot(time, signal, "black")
    else:
        for s in signal:
            sygAxes.plot(time[:len(s)], s, "black", lw=0.15)
    sygAxes.set_xlim(time[0], arr_t[-1]) #ucinanie koncowki sygnalu, która nie była wykorzystana w tworzeniu mapy, bo okna sie nierowno podzielily
    if time_label != None:
        sygAxes.set_xlabel(time_label)
    if signal_label != None:
        sygAxes.set_ylabel(signal_label)

    # colorbar
    clb = plt.colorbar(im, cax=clbAxes)
    if colorbar_label is not None:
        clb.set_label(colorbar_label)

    #estetyka #todo - do poprawy
    if title != None:
        ax.set_title(title)
    # set_axis_fontsize(tfAxes, fontsize * figure_scale / 12)
    # sygAxes.tick_params(axis='y', labelsize=fontsize * figure_scale/18)
    # sygAxes.tick_params(axis='x', labelsize=fontsize * figure_scale/12)
    # set_axis_fontsize(clbAxes, fontsize * figure_scale / 12)
    # set_axis_fontsize(ax, fontsize * figure_scale / 8)
    # # clb.ax.tick_params(labelsize=fontsize * figure_scale / 8)
    # ax.ticklabel_format(style='plain')
    # ax.yaxis.set_major_formatter(pb.FormatStrFormatter('%.0f'))


def plot_syg_and_heatmap_stats(ax, heatmap, heatmap_signifitant, signal, time, arr_t, arr_f, ref, f_lim_cond, title, time_label, signal_label, freq_label, colorbar_label, config):
    '''
    Prepering plot for specific ax with three parts: heatmap in the middle, time series plot in the bottom and color bar
    of the heatmap on the right. On the heatmap will be marked which cells are statistical significant between two tags.

    Parameters
    ----------
    ax: matplotlib.axes
        Axis of the figure where to plot.
    heatmap: numpy.ndarray
        Matrix of the variables for the heatmap plot.
    heatmap_signifitant: numpy.ndarray
        Arrays with bool values that shows which cells are statistically significantly different.
    signal: numpy.ndarray or list
        Set of the signals (for example epochs) or one signal (for example ERP).
    time: numpy.ndarray
        Variables of the x axis for the time series plot.
    arr_t: numpy.ndarray
        Time positions of the heatmap cells (output of the scipy.signal.spectrogram).
    arr_f: numpy.ndarray
        Frequency positions of the heatmap cells (output of the scipy.signal.spectrogram).
    ref: list
        #todo - kiedyś dorobić obszar referencji, gdzie liczyć statystyki jako dwuelementową listę np [0.5, 0.7] (wtedy wylicza się statystyki w przedziale 0.5-0.7s)
    f_lim_cond: list or None
        List with boolean variables - they will give information which frequency of arr_f will be ploted. (None for WT)
    title: str
        Title of the ax.
    time_label: str
        Label of the horizontal axis for time series plot.
    signal_label: str
        Label of the vertical axis for time series plot.
    freq_label: str
        Label of the vertical axis for heatmap plot.
    colorbar_label: str
        Label of the colorbar.
    config: dict
        Dictionary of configuration variables.
    '''

    cmap = config["time_frequency_domain"]["cmap"]

    # współrzędne poszczególnych wykresów na ax
    tfAxes = ax.inset_axes(bounds=[0, 0.125, 0.9, 0.875])  # , transform=ax.transData)  # heatmapa
    sygAxes = ax.inset_axes(bounds=[0, 0, 0.9, 0.125])  # , transform=ax.transData)  # plot
    clbAxes = ax.inset_axes(bounds=[0.95, 0.125, 0.01, 0.875])  # , transform=ax.transData)  # colorbar

    ax.axis("off")  # wyłączenie osi głównego ax

    # heatmapa
    if isinstance(f_lim_cond, np.ndarray):
        if len(arr_f[f_lim_cond]) != 1:
            arr_f = arr_f[f_lim_cond]
        else:
            # gdyby nasze warunki wyswietlenia byly zbyt "male", to bierzemy jeszcze jedna wartosc do przodu
            f_lim_cond[np.where(f_lim_cond)[0] + 1] = True
            arr_f = arr_f[f_lim_cond]

        heatmap = heatmap[f_lim_cond, :]
        heatmap_signifitant = heatmap_signifitant[f_lim_cond, :]

    _ = tfAxes.imshow(heatmap, aspect='auto', interpolation=None, origin='lower',
                      cmap="gray", extent=(time[0], arr_t[-1], arr_f[0], arr_f[-1]))
                      #vmin=min_h, vmax=max_h) #todo - vmin i vmax
    im = tfAxes.imshow(heatmap_signifitant, aspect='auto', interpolation=None, origin='lower',
                       cmap=cmap, extent=(time[0], arr_t[-1], arr_f[0], arr_f[-1]))
                       #vmin=min_h, vmax=max_h) #todo - vmin i vmax

    if ref != None:
        ref_t = arr_t[np.where(((arr_t >= ref[0]) & (arr_t <= ref[1])))[0]]
        t_lines = [ref_t[0], ref_t[-1]]
        # t_lines[0] -= cell_half_width
        # t_lines[1] += cell_half_width

        _ = tfAxes.plot([t_lines[0], t_lines[0]], [arr_f[0], arr_f[-1]], "--", c="yellow")
        _ = tfAxes.plot([t_lines[1], t_lines[1]], [arr_f[0], arr_f[-1]], "--", c="yellow")

    tfAxes.tick_params(
        axis='x', which='both', bottom=False, top=False, labelbottom=False)
    if freq_label is not None:
        tfAxes.set_ylabel(freq_label)

    # plot
    if len(np.shape(signal)) == 1:
        sygAxes.plot(time, signal, "black")
    else:
        for s in signal:
            sygAxes.plot(time, s, "black", lw=0.15)
    sygAxes.set_xlim(time[0], arr_t[-1])  # ucinanie koncowki sygnalu, która nie była wykorzystana w tworzeniu mapy, bo okna sie nierowno podzielily

    if time_label is not None:
        sygAxes.set_xlabel(time_label)
    if signal_label is not None:
        sygAxes.set_ylabel(signal_label)

    # colorbar
    clb = plt.colorbar(im, cax=clbAxes)
    if colorbar_label is not None:
        clb.set_label(colorbar_label)

    # estetyka #todo - do poprawy
    if title != None:
        ax.set_title(title)
    # set_axis_fontsize(tfAxes, fontsize * figure_scale / 12)
    # sygAxes.tick_params(axis='y', labelsize=fontsize * figure_scale / 18)
    # sygAxes.tick_params(axis='x', labelsize=fontsize * figure_scale / 12)
    # set_axis_fontsize(clbAxes, fontsize * figure_scale / 12)
    # set_axis_fontsize(ax, fontsize * figure_scale / 8)
    # # clb.ax.tick_params(labelsize=fontsize * figure_scale / 8)
    # ax.ticklabel_format(style='plain')
    # ax.yaxis.set_major_formatter(pb.FormatStrFormatter('%.0f'))

def plot_signal(ax, signal, time, title, x_label, y_label, config):
    '''
    Preparing plot with one line (for example signal in the time).

    Parameters
    ----------
    ax: matplotlib.axes
        Axis of the figure where to plot.
    signal: numpy.ndarray
        Variables of the signal (for example potentials).
    time: numpy.ndarray
        Variables of the x axis for the plot.
    title: str
        Title of the ax.
    time_label: str
        Label of the horizontal axis.
    signal_label: str
        Label of the vertical axis.
    config: dict
        Dictionary of configuration variables.
    '''
    ax.plot(time, signal, "black")

    if x_label is not None:
        ax.set_xlabel(x_label)
    if y_label is not None:
        ax.set_ylabel(y_label)
    if title is not None:
        ax.set_title(title)

def prepare_topo_map(signal_info):
    '''
    Finding electrodes positions for the topographic plots.

    Parameters
    ----------
    signal_info: mne.io.meas_info.Info
        Info of the signal provided by the MNE.

    Returns
    -------
    fig: plt.figure.Figure
        Matplotlib figure.
    axs: list
        List of the plt.axes.Axes of the fig variable. They are positioned the same as the electrode positions.
    mne.viz.topo.find_layout(signal_info).names: list
        List of the electrodes names - they are the same order as signal_info["ch_names"].
    '''
    fig = plt.figure(figsize=(19.2, 10.8), dpi=1000)
    axs = []
    for pos in mne.viz.topo.find_layout(signal_info).pos:
        ax = plt.axes(pos)
        # ax.tick_params(top=False, bottom=False, left=False, right=False, labelleft=False, labelbottom=False)
        axs.append(ax)

    #todo - tu wygląda koszmarnie przy ilości kanałów >30

    return fig, axs, mne.viz.topo.find_layout(signal_info).names

def plot_cluster_test(erps, erps_num, clusters, clusters_p_values, epochs_info, config, file_name, output_path):
    '''
    Plotting parts of the time domain plot of erp where cluster test finds statistical significant difference between
    tags.

    Parameters
    ----------
    erps: dict
        Dictionary which has names of tags as keys and numpy.ndarrays as values. Each array has
        [n_channels, erp] as shape and represent set of ERPs for each channel.
    erps_num: dict
        Dictionary which has names of tags as keys and number of epochs for that tag.
    clusters: list
        List of found clusters by cluster test. Each cluster has shape [signal_length, n_channels] and show position of
        them on the signal with bool values.
    clusters_p_values: numpy.ndarray
        List of p values for each cluster.
    epochs_info: mne.io.meas_info.Info
        MNE info taken from epochs.
    config: dict
        Dictionary of configuration variables.
    file_name: str
        Name of the file with raw signal.
    output_path: str
        Destinetion path of the plot.
    '''

    t = np.linspace(config["epochs"]["start_offset"], config["epochs"]["stop_offset"], erps[list(erps.keys())[0]].shape[1])

    for ch_nr, ch_name in enumerate(epochs_info.ch_names):

        fig, ax = plt.subplots(dpi=1000)

        # plotting erps
        for tag in erps.keys():
            ax.plot(t, erps[tag][ch_nr, :]*10**6, label=f"{tag} ({erps_num[tag]})")
            ax.set_xlabel("Time [s]")
            ax.set_ylabel(r"Potential $[\mu V]$")
            ax.set_title(f"Cluster test {ch_name}")

        for cl_nr, cluster in enumerate(clusters):

            cluster_p_value = clusters_p_values[cl_nr]
            cluster = cluster.T #n_channels x time_points

            if cluster_p_value < 0.05:
                color = "blue"
                alpha = 0.3
                label = "p<0.05"
                paint_patch = True
            elif cluster_p_value < 0.1:
                color = "blue"
                alpha = 0.1
                label = "p<0.1"
                paint_patch = True
            else:
                color = "gray"
                alpha = 1 - cluster_p_value
                label = "p>0.1"
                paint_patch = False

            #plot significent p_val
            if paint_patch:
                cls_ch = np.split(np.where(cluster[ch_nr] == True)[0], np.where(np.diff(np.where(cluster[ch_nr] == True)[0]) != 1)[0]+1)
                for c, cl in enumerate(cls_ch):
                    if cl.size >= 2:
                        label=label if c==0 else None
                        ax.axvspan(t[cl[0]], t[cl[-1]], color=color, alpha=alpha, zorder=1, label=label)

        ax.legend()
        fig.savefig(os.path.join(output_path, f"{os.path.splitext(file_name)[0]}___clustertest_{ch_name}_{list(erps.keys())[0]}_vs_{list(erps.keys())[1]}.jpg"))
        plt.close(fig)

def plots_for_filters(config, filter, path, show, plot):
    '''
    Plot or show three plots (time, magnitude and delay) of config["filters"][filter].

    Parameters
    ----------
    config: dict
        Dictionary of configuration variables.
    filter: str
        Name of filter in config["filters"].
    path: str
        Path to directory with signal.
    show: bool
        If True, show the plots of filters (time, magnitude and delay).
    plot: bool
        If True, save the plots of filters (time, magnitude and delay).
    '''

    sos = ss.iirdesign(wp=config["filters"][filter]["wp"],
                       ws=config["filters"][filter]["ws"],
                       gpass=config["filters"][filter]["gpass"],
                       gstop=config["filters"][filter]["gstop"],
                       analog=0,
                       ftype=config["filters"][filter]["ftype"],
                       output="sos",
                       fs=config["sfreq"])

    if show:
        fig = mne.viz.plot_filter(dict(sos=sos), config["sfreq"], show=True, title=filter)
        plt.close()


    if plot:
        fig = mne.viz.plot_filter(dict(sos=sos), config["sfreq"], show=False, title=filter)
        fig.set_dpi(1000)
        fig.savefig(os.path.join(path, "filters_plot", filter + ".png"))
        plt.close()

def plot_BandPower(time, Pg, thrs, channel, band, width, ymin, ymax, facecolor, path, file_name):
    '''
    Preparing plot for searching bad channels via BandPower in preanalyse/channels+and_signal.py

    Parameters
    ----------
    time: numpy.ndarray
        Matrix with time.
    Pg: numpy.ndarray
        Points of average estimated power in defined window and band.
    thrs: list
        List with three floats/integers (in μV^2) for thresholds to calculate which channel has too much noise. So if
        thrs_type is:
            "+" -> then thrs[0] > thrs[1] > thrs[2]
            "-" -> then thrs[0] < thrs[1] < thrs[2]
    channel: str
        Channel name.
    band: list
        List with two floats/integers representing frequency band for the noise.
    width: float
        Function will cut windows from signal with this width (in seconds) and calculate spectral density for them.
    ymin: float
        Minimal value of Y axis (Pg).
    ymax: float
        Maximum value of Y axis (Pg).
    facecolor: str
        Facecolor of plot.
    path: str
        Output path for plot.
    file_name: str
        Name of input data.
    '''

    fig, ax = plt.subplots(1, 1)#, figsize=(19.2, 10.8), dpi=1000)

    ax.plot(time, Pg)
    ax.plot([time[0], time[-1]], [thrs[0], thrs[0]], 'g--')
    ax.plot([time[0], time[-1]], [thrs[1], thrs[1]], 'y--')
    ax.plot([time[0], time[-1]], [thrs[2], thrs[2]], 'r--')

    ax.set_yscale('log')
    ax.set_ylim([ymin, ymax])
    ax.set_xlim((time[0], time[-1]))
    if facecolor != None:
        ax.set_facecolor(facecolor)

    ax.set_title(f"{channel}: {band[0]}-{band[1]} Hz and window's width {width} s (n_win={Pg.size})")
    ax.set_xlabel("Number of window")
    ax.set_ylabel(r"Power $[\mu V^2]$")

    fig.savefig(os.path.join(path, f"{file_name}___{channel}_{band[0]}_{band[1]}Hz_{width}s.png"))
    plt.close()

def probabilities_vector_and_histogram(time, prob_vector, threshold, path, file_name):
    #todo - opis

    fig, axs = plt.subplots(2, 1, figsize=[8, 8])

    axs[0].scatter(t, prob_vector, marker=".", color="black", s=0.1, label="Probability of looking")
    axs[0].plot([t[0], time[-1]], [threshold, threshold], "r--", label=f"Looking threshold = {threshold}")
    axs[0].set_title("Time series of probablities", fontsize=10)
    axs[0].set_xlabel("Time [s]", fontsize=10)
    axs[0].set_ylabel("Probability", fontsize=10)
    axs[0].legend(loc="center right", fontsize=8)

    _ = axs[1].hist(prob_vector, bins=40, color="black", label="Probability of looking")
    ticks = axs[1].get_yticks()
    ticks_label = [i for i in range(0, int(np.ceil(ticks[-1] / len(prob_vector) * 100)), 10)]
    ticks = np.array(ticks_label) * len(x) / 100
    axs[1].set_yticks(ticks=ticks, labels=list(map(lambda t: f"{t:2.0f}%", ticks_label)))
    axs[1].plot([threshold, threshold], [0, ticks[-1]], "r--",
                label=f"Looking threshold: \n{np.sum(prob_vector <= threshold) / prob_vector.shape[0] * 100:2.2f}% for <= {threshold} threshold")
    axs[1].set_xlabel("Probability", fontsize=10)
    axs[1].set_ylabel("Frequency [%]", fontsize=10)
    axs[1].set_title("Histogram of probablities", fontsize=10)
    axs[1].legend(fontsize=8)

    plt.tight_layout()

    fig.savefig(os.path.join(path, f"{file_name}___video_probability_vector_plots.png"))

