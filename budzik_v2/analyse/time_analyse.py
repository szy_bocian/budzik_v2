#Szymon Bocian 2023

import os
import mne
import matplotlib.pyplot as plt
import numpy as np

from ..plots.plots import plot_cluster_test

def time_analysis(rest, epochs, config, path, file_name):
    '''
    Parameters
    ----------
    rest: mne.epochs.Epochs or None
        Prepared epochs from the signal. If None, REST path wasn't prepare in config file.
    epochs: mne.epochs.Epochs or None
        Prepared epochs from the signal. If None, EPOCHS path wasn't prepare in config file.
    config: dict
        Dictionary of configuration variables.
    path: str
        Path to the output directory.
    file_name: str
        Name of the file with raw signal.
    '''

    time_path = os.path.join(path, "time_analyse")
    if not os.path.isdir(time_path):
        os.mkdir(time_path)

    # # time for REST
    # if rest != None:
    #
    #     eeg_rest = rest.load_data().pick_types(eeg=True)
    #     eeg_channels = eeg_rest.info["ch_names"]
    #
    #     for tag in rest.event_id.keys():
    #         for ch in eeg_channels:
    #             fig = eeg_rest[tag].plot_image(picks=ch, title=f"{tag} ({len(eeg_rest[tag])}): {ch}", show=False)[0]
    #             fig.savefig(os.path.join(time_path, f"{os.path.splitext(file_name)[0]}___{tag}_{ch}.jpg"))
    #             plt.close(fig)
    #
    #         fig = eeg_rest[tag].average().plot(spatial_colors=True, show=False)
    #         fig.savefig(os.path.join(time_path, f"{os.path.splitext(file_name)[0]}___{tag}.jpg"))
    #         plt.close(fig)

    # time for EPOCHS
    if epochs != None:

        eeg_epochs = epochs.load_data().pick_types(eeg=True)
        eeg_channels = eeg_epochs.info["ch_names"]

        for tag in epochs.event_id.keys():
            for ch in eeg_channels:
                fig = eeg_epochs[tag].plot_image(picks=ch, title=f"{tag} ({len(eeg_epochs[tag])}): {ch}", show=False)[0]
                fig.savefig(os.path.join(time_path, f"{os.path.splitext(file_name)[0]}___{tag}_{ch}.jpg"))
                plt.close(fig)

            fig = eeg_epochs[tag].average().plot(spatial_colors=True, show=False)
            fig.savefig(os.path.join(time_path, f"{os.path.splitext(file_name)[0]}___{tag}.jpg"))
            plt.close(fig)

        cluster_test(epochs, config, file_name, time_path)


def cluster_test(epochs, config, file_name, time_path):
    '''
    Preparing cluster test with mne.stats.spatio_temporal_cluster_test
    (https://mne.tools/1.3/generated/mne.stats.spatio_temporal_cluster_test.html) and draw significant different
    clusters between tags on the time-domain plot saved in the time_path.

    Parameters
    ----------
    epochs: mne.epochs.Epochs
        Prepared epochs from the signal.
    config: dict
        Dictionary of configuration variables.
    file_name: str
        Name of the file with raw signal.
    time_path: str
        Path to the directory prepared for data from time analysis.
    '''

    labels_id = np.arange(0, len(epochs.event_id.keys()), 1)
    pairs_labels = [(a, b) for idx, a in enumerate(labels_id) for b in labels_id[idx + 1:]]  # pary indeksów tagów

    for pair in pairs_labels:

        # calculate cluster test
        t1, t2 = list(epochs.event_id.keys())[pair[0]], list(epochs.event_id.keys())[pair[1]]
        epochs_transposed = [epochs[t1].load_data().pick_types(eeg=True, exclude=epochs.info["bads"]).get_data().transpose(0, 2, 1),
             epochs[t2].load_data().pick_types(eeg=True, exclude=epochs.info["bads"]).get_data().transpose(0, 2, 1)] #because X must be: n_groups x n_epochs x time x n_channels
        _, clusters, clusters_p_values, _ = mne.stats.spatio_temporal_cluster_test(epochs_transposed, out_type="mask", **config['time_domain']['cluster_test']) #clusters is [n_clusters, 2, cluster_length] - first array of tuple is position in time of cluster, second array is position on head (which channel) for this point of time of the cluster

        erps = {t1: epochs[t1].load_data().pick_types(eeg=True, exclude=epochs.info["bads"]).average().get_data(),
                t2: epochs[t2].load_data().pick_types(eeg=True, exclude=epochs.info["bads"]).average().get_data()}
        erps_num = {t1: len(epochs[t1]), t2: len(epochs[t2])}
        # plot ERP with cluster test results for p<0.1 and p<0.05
        plot_cluster_test(erps, erps_num, clusters, clusters_p_values, epochs[t1].load_data().pick_types(eeg=True, exclude=epochs.info["bads"]).info, config, os.path.splitext(file_name)[0], time_path)

        # write down all clusters
        with open(os.path.join(time_path, f"{os.path.splitext(file_name)[0]}___clustertest_{t1}_vs_{t2}_results.txt"), "w") as file:
            for cl_nr, cluster in enumerate(clusters):
                file.write(f"Cluster nr. {cl_nr} with p = {clusters_p_values[cl_nr]}:\n")
                cluster = cluster.T
                for ch_nr, ch_name in enumerate(epochs.info["ch_names"]):
                    cls_ch = np.split(np.where(cluster[ch_nr] == True)[0],
                                      np.where(np.diff(np.where(cluster[ch_nr] == True)[0]) != 1)[0] + 1)
                    cl_times = []
                    for cl in cls_ch:
                        if cl.size >= 2:
                            cl_times.append(f"{cl[0] / epochs.info['sfreq']}s - {cl[-1] / epochs.info['sfreq']}s")
                        elif cl.size == 1:
                            cl_times.append(f"{cl[0] / epochs.info['sfreq']}s")
                    file.write(f"    {ch_name}:".ljust(10, " ") + " | ".join(cl_times) + "\n")
                file.write("\n")
        file.close()

