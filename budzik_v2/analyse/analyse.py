#Szymon Bocian 2023

from .time_analyse import time_analysis
from .frequency_analyse import frequency_analysis
from .time_frequency_analyse import time_frequency_analysis
from .statistics_table import statistics_table

import matplotlib.pyplot as plt

def analyse(rest, rest_clean, epochs, epochs_clean, data, config, path, tags_desc_id, file_name):
    '''
    Preparing analyse and plots for the prepared epochs by .

    Parameters
    ----------
    rest: mne.epochs.Epochs or None
        Prepared rest segments from the signal. If None, REST path wasn't prepare in config file.
    rest_clean: mne.epochs.Epochs or None
        Prepared rest segments from the signal without segments with artifacts. If None, REST path wasn't
        prepare in config file.
    epochs: mne.epochs.Epochs or None
        Prepared epochs from the signal. If None, EPOCHS path wasn't prepare in config file.
    epochs_clean: mne.epochs.Epochs or None
        Prepared epochs from the signal without epochs with artifacts. If None, REST path wasn't
        prepare in config file.
    data: pandas.core.frame.DataFrame
        Dataframe with basic information about tags.
    config: dict
        Dictionary of configuration variables.
    path: str
        Path to the output directory.
    tags_desc_id: dict
        Dictionary of the names of prepared tags and their values in the prepared STIM signal.
    file_name: str
        Name of the file with raw signal.
    '''

    print("MESSAGE: Preparing table with statistics.")
    statistics_table(rest, epochs, data, tags_desc_id, path, file_name)

    if "time_domain" in config.keys() and config["prepare_time_analysis"]:
        print("MESSAGE: Preparing analysis in time domain.")
        time_analysis(rest_clean, epochs_clean, config, path, file_name)

    if "frequency_domain" in config.keys() and config["prepare_frequency_analysis"]:
        print("MESSAGE: Preparing analysis in frequency domain.")
        frequency_analysis(rest_clean, epochs_clean, config, path, file_name)

    if "time_frequency_domain" in config.keys() and config["prepare_time_frequency_analysis"]:
        print("MESSAGE: Preparing analysis in time-frequency domain.")
        time_frequency_analysis(rest_clean, epochs_clean, config, path, file_name)

    plt.close()