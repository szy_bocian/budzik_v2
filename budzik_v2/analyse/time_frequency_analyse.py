#Szymon Bocian 2023

import os
import numpy as np
import scipy.signal as ss
import scipy.stats as st
import matplotlib.pyplot as plt
import sys

from budzik_v2.plots.plots import plot_sig_and_heatmap, plot_syg_and_heatmap_stats

def time_frequency_analysis(rest, epochs, config, path, file_name):
    '''
    Creates plots and statistics for the time-frequency domain of the signals.

    Parameters
    ----------
    epochs: mne.epochs.Epochs
        Prepared epochs from the signal.
    config: dict
        Dictionary of configuration variables.
    path: str
        Path to the output directory.
    file_name: str
        Name of the file with raw signal.
    '''
    tf_path = os.path.join(path, "time_frequency_analyse")
    if not os.path.isdir(tf_path):
        os.mkdir(tf_path)

    # TF maps for REST
    if rest != None:

        for transform in config["time_frequency_domain"].keys():

            # checking transform
            if isinstance(config["time_frequency_domain"][transform], dict) and transform != "statistics" and transform in ["stft", "wt"]:

                maps = {tag: [] for tag in rest.event_id.keys()}  # n_tags: n_channels x n_events x freq x time -> maps in TF domain for avery epoch
                maps_erp = {tag: [] for tag in rest.event_id.keys()}  # n_tags: n_channels x freq x time -> maps in TF domain for ERP
                signals_erp = {tag: [] for tag in rest.event_id.keys()}  # n_tags: n_channels x time -> ERPs

                # estimate TF maps for tags
                for tag in rest.event_id.keys():
                    signals = rest[tag].load_data().pick_types(eeg=True).get_data() # epochs for specific tag
                    signal_erp = np.mean(signals, axis=0) # erp for all channels from this epochs
                    signals_erp[tag] = signal_erp # saving erps for this tag

                    ch_names = rest[tag].load_data().pick_types(eeg=True).info["ch_names"]

                    for ch, channel in enumerate(ch_names):

                        # TF estimation of ERP from ERP (calculate ERP, them TF)
                        # heatmap, arr_t, arr_f, f_lim_cond = get_transform(signal_erp[ch], config, transform)
                        #
                        # fig, ax = plt.subplots(1, 1)
                        # plot_sig_and_heatmap(ax=ax, signal=signal_erp[ch, :]*10**6, heatmap=heatmap,
                        #                      time=np.arange(0, config["rest"]["window_length"]+1/rest.info["sfreq"], 1/rest.info["sfreq"]),
                        #                      arr_t=arr_t, arr_f=arr_f, f_lim_cond=f_lim_cond, title=f"{tag} ({len(signals)}) {transform}: {channel}", time_label="Time [s]",
                        #                      signal_label=r"Potential [${\mu V}^2$]", freq_label= "Frequency [Hz]", colorbar_label="Power [dB]", config=config)
                        # fig.savefig(os.path.join(tf_path, f"{os.path.splitext(file_name)[0]}___ERP_{tag}_{transform}_{channel}.png"))
                        # plt.close(fig)
                        #
                        # maps_erp[tag].append(heatmap)

                        # TF estimation of ERP from epochs (calculate TF from epochs, then average TF from TF maps)
                        maps_ch = []

                        for ep in range(signals.shape[0]):
                            heatmap, arr_t, arr_f, f_lim_cond = get_transform(signals[ep, ch, :], config, transform)
                            maps_ch.append(heatmap)

                        maps[tag].append(maps_ch)

                        fig, ax = plt.subplots(1, 1)
                        plot_sig_and_heatmap(ax=ax, signal=signals[:, ch, :]*10**6, heatmap=np.mean(maps_ch, axis=0),
                                             time=np.linspace(0, config["rest"]["window_length"], signals[:, ch, :].shape[0]),
                                             arr_t=arr_t, arr_f=arr_f, f_lim_cond=f_lim_cond, title=f"{tag} ({len(signals)}) {transform}: {channel}", time_label="Time [s]",
                                             signal_label=r"Potential [${\mu V}^2$]", freq_label= "Frequency [Hz]", colorbar_label="Power [dB]", config=config)
                        fig.savefig(os.path.join(tf_path, f"{os.path.splitext(file_name)[0]}___TF_{tag}_{transform}_{channel}.png"))
                        plt.close(fig)

    # TF maps for EPOCHS
    if epochs != None:
        for transform in config["time_frequency_domain"].keys():

            # checking transform
            if isinstance(config["time_frequency_domain"][transform], dict) and transform != "statistics" and transform in ["stft", "wt"]:

                maps = {tag: [] for tag in epochs.event_id.keys()}  # n_tags: n_channels x n_events x freq x time -> maps in TF domain for avery epoch
                maps_erp = {tag: [] for tag in epochs.event_id.keys()}  # n_tags: n_channels x freq x time -> maps in TF domain for ERP
                signals_erp = {tag: [] for tag in epochs.event_id.keys()}  # n_tags: n_channels x time -> ERPs

                # estimate TF maps for tags
                for tag in epochs.event_id.keys():
                    signals = epochs[tag].load_data().pick_types(eeg=True).get_data() # epochs for specific tag
                    signal_erp = np.mean(signals, axis=0) # erp for all channels from this epochs
                    signals_erp[tag] = signal_erp # saving erps for this tag

                    ch_names = epochs[tag].load_data().pick_types(eeg=True).info["ch_names"]

                    for ch, channel in enumerate(ch_names):

                        # TF estimation of ERP from ERP (calculate ERP, then TF)
                        heatmap, arr_t, arr_f, f_lim_cond = get_transform(signal_erp[ch], config, transform)

                        fig, ax = plt.subplots(1, 1)
                        plot_sig_and_heatmap(ax=ax, signal=signal_erp[ch, :]*10**6, heatmap=heatmap,
                                             time=np.linspace(config["epochs"]["start_offset"], config["epochs"]["stop_offset"], signal_erp[ch, :].shape[0]),
                                             arr_t=arr_t, arr_f=arr_f, f_lim_cond=f_lim_cond, title=f"{tag} ({len(signals)}) {transform}: {channel}", time_label="Time [s]",
                                             signal_label=r"Potential [$\mu V$]", freq_label= "Frequency [Hz]", colorbar_label="Power [dB]", config=config)
                        fig.savefig(os.path.join(tf_path, f"{os.path.splitext(file_name)[0]}___ERP_{tag}_{transform}_{channel}.png"))
                        plt.close(fig)

                        maps_erp[tag].append(heatmap)

                        # TF estimation of ERP from epochs (calculate TF from epochs, then average TF from TF maps)
                        maps_ch = []

                        for ep in range(signals.shape[0]):
                            heatmap, arr_t, arr_f, f_lim_cond = get_transform(signals[ep, ch, :], config, transform)
                            maps_ch.append(heatmap)

                        maps[tag].append(maps_ch)

                        fig, ax = plt.subplots(1, 1)
                        plot_sig_and_heatmap(ax=ax, signal=signals[:, ch, :]*10**6, heatmap=np.mean(maps_ch, axis=0),
                                             time=np.linspace(config["epochs"]["start_offset"], config["epochs"]["stop_offset"], signal_erp[ch, :].shape[0]),
                                             arr_t=arr_t, arr_f=arr_f, f_lim_cond=f_lim_cond, title=f"{tag} ({len(signals)}) {transform}: {channel}", time_label="Time [s]",
                                             signal_label=r"Potential [$\mu V$]", freq_label= "Frequency [Hz]", colorbar_label="Power [dB]", config=config)
                        fig.savefig(os.path.join(tf_path, f"{os.path.splitext(file_name)[0]}___TF_{tag}_{transform}_{channel}.png"))
                        plt.close(fig)


                # calculate statistics for statistical significance between cells
                if 'statistics' in config["time_frequency_domain"].keys():

                    # preparing statistical tests given by user
                    for stat in config["time_frequency_domain"]["statistics"]["stat_name"]:
                        labels_id = np.arange(0, len(epochs.event_id.keys()), 1)
                        pairs_labels = [(a, b) for idx, a in enumerate(labels_id) for b in labels_id[idx + 1:]]  # pairs of tag indexes

                        # prepare for stats for pairs of tags
                        for pair in pairs_labels:

                            t1, t2 = list(epochs.event_id.keys())[pair[0]], list(epochs.event_id.keys())[pair[1]]

                            fft = np.array(maps_erp[t1]) - np.array(maps_erp[t2]) # substract maps calculated from erps from that epochs
                            erp = np.array(signals_erp[t1]) - np.array(signals_erp[t2])

                            # max_sig = np.max(fft_all.flatten())
                            # min_sig = np.min(fft_all.flatten())

                            _, _, p_sig = resel_stats_2maps(np.array(maps[t1]), np.array(maps[t2]), stat, arr_f, transform, config)

                            fft_sig = np.copy(fft)
                            fft_sig[np.array(p_sig) != 1] = np.nan  # non-significant cells write as np.nan

                            for ch, channel in enumerate(ch_names):
                                fig, ax = plt.subplots(1, 1)
                                plot_syg_and_heatmap_stats(ax, fft[ch], fft_sig[ch], erp[ch]*10**6, np.linspace(config["epochs"]["start_offset"], len(erp[ch]) / epochs.info["sfreq"] + config["epochs"]["start_offset"], len(erp[ch])),
                                                     arr_t, arr_f, None, f_lim_cond, f"{t1} vs {t2} {transform} {stat}: {channel}", "Time [s]",
                                                     r"Potential [${\mu V}^2$]", "Frequency [Hz]", "Power [dB]", config)

                                fig.savefig(os.path.join(tf_path, f"{os.path.splitext(file_name)[0]}___{t1}-x-{t2}_{transform}_{stat}_{channel}.png"))
                                plt.close(fig)

def stft_transform(signal, config):
    '''
    The function which prepares Short Time Fourier Transform for the one-dimensional signal.

    Parameters
    ----------
    signal: np.array
        Variables of the signal.
    config: dict
        Dictionary of configuration variables.

    Returns
    -------
    fft: np.array
        Matrix of the power in the time-frequency domain estimated with STFT.
    arr_t: np.array
        Time positions of the heatmap cells (output of the scipy.signal.spectrogram).
    arr_f: np.array
        Frequency positions of the heatmap cells (output of the scipy.signal.spectrogram).
    f_lim_cond: list
        List with boolean variables - they will give information which frequency of arr_f will be plotted and consider
        during estimate statistical significance between cells.
    '''

    noverlap = config["sfreq"] * config["time_frequency_domain"]["stft"]["window_duration"] * config["time_frequency_domain"]["stft"]["window_overlap"] # how much points will be shared between windows ?
    nwindow = int(config["sfreq"] * config["time_frequency_domain"]["stft"]["window_duration"]) # length of window in the number of points

    try:
        window = ss.get_window(config["time_frequency_domain"]["stft"]["window_name"], nwindow)
    except:
        sys.exit(f'ERROR: Window name {config["time_frequency_domain"]["stft"]["window_name"]} is not provided by scipy library - more: https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.get_window.html#scipy.signal.get_window')


    arr_f, arr_t, fft = ss.spectrogram(signal, config["sfreq"], noverlap=noverlap, window=window, scaling="spectrum") # estimated spectral density in time-frequency domain calculated by short time fourier transform
    fft = 10 * np.log10(fft) # decibels for power

    arr_t += config["epochs"]["start_offset"] # adding starting point of epoch to get correct time values for our plots
    arr_t[-1] += np.abs(arr_t[0] - arr_t[1]) / 2 # adding half of the cell for aesthetic reasons

    f_lim_cond = (arr_f >= config["time_frequency_domain"]["stft"]["f_lim_start"]) & (arr_f <= config["time_frequency_domain"]["stft"]["f_lim_end"]) # condition for frequencies to show on the plots and calculate significance between tags for cells

    return fft, arr_t, arr_f, f_lim_cond

def wt_transform(signal, config):
    '''
    The function which prepares Wavelet Transform for the one-dimensional signal.

    Parameters
    ----------
    signal: np.array
        Variables of the signal.
    config: dict
        Dictionary of configuration variables.

    Returns
    -------
    fft: np.array
        Matrix of the power in the time-frequency domain estimated with WT.
    arr_t: np.array
        Time positions of the heatmap cells.
    arr_f: np.array
        Frequency positions of the heatmap cells.
    f_lim_cond: list
        List with boolean variables - they will give information which frequency of arr_f will be plotted and consider
        during estimate statistical significance between cells.
    '''

    try:
        wavelet = getattr(ss, config["time_frequency_domain"]["wt"]["wavelet_name"])
    except:
        sys.exit(f'Wavelet name {config["time_frequency_domain"]["wt"]["wavelet_name"]} is not provided by scipy library - more: https://docs.scipy.org/doc/scipy/reference/signal.html#wavelets')

    f_lim = (config["time_frequency_domain"]["wt"]["f_lim_start"], config["time_frequency_domain"]["wt"]["f_lim_end"]) # frequency band for wavelet transform
    f_lim_n = config["time_frequency_domain"]["wt"]["wavelet_f_lim_n"] # number of frequencies for wavelets between f_lim[0]-f_lim[1]
    wavelet_parameters = config["time_frequency_domain"]["wt"]["wavelet_parameters"] # additional parameters for wavelets
    Fs = config["sfreq"] # sampling frequency

    if wavelet.__name__ == "morlet2":

        widths = wavelet_parameters["w"] * Fs / (2 * np.linspace(f_lim[0], f_lim[1], f_lim_n) * np.pi)

        fft = np.abs(ss.cwt(signal, wavelet, widths, **wavelet_parameters)) ** 2 # estimated spectral density in time-frequency domain calculated by wavelet transform
        fft = 10 * np.log10(fft) # decibels for power

        arr_f = np.linspace(f_lim[0], f_lim[1], f_lim_n)
        arr_t = np.linspace(config["epochs"]["start_offset"], config["epochs"]["start_offset"] + fft.shape[1] / Fs, fft.shape[1])

        f_lim_cond = None  # because we calculate this condition during wavelet transform with widths parameter
    else:

        sys.exit(f"ERROR: {wavelet.__name__} is not provided.")

    return fft, arr_t, arr_f, f_lim_cond

def get_transform(signal, config, transform):
    '''
    The function is choosing Short Time Fourier Transform or Wavelet Transform with inputs provided by config file.

    Parameters
    ----------
    signal: np.array
        Variables of the signal (for example potentials).
    config: dict
        Dictionary of configuration variables.
    transform: str
        "stft" or "wt".

    Returns
    -------
    fft: np.array
        Matrix of the power in the time-frequency domain estimated with choosed transform.
    arr_t: np.array
        Time positions of the heatmap cells (output of the scipy.signal.spectrogram).
    arr_f: np.array
        Frequency positions of the heatmap cells (output of the scipy.signal.spectrogram).
    f_lim_cond: list or None
        List with boolean variables - they will give information which frequency of arr_f will be ploted. (None for WT)
    '''

    if transform == "stft":
        fft, arr_t, arr_f, f_lim_cond = stft_transform(signal, config)
    elif transform == "wt":
        fft, arr_t, arr_f, f_lim_cond = wt_transform(signal, config)

    return fft, arr_t, arr_f, f_lim_cond

def resel_stats_2maps(maps1, maps2, stat, arr_f, transform, config):
    '''
    Calculating statistical significance between the difference of the cells from maps1 and maps2 (so between tags).

    Parameters
    ----------
    maps1: numpy.ndarray
        Maps in TF domain of the epochs for the first tag (for example "target"). Shape is: n_channels x n_events x freq x time.
    maps2: numpy.ndarray
        Maps in TF domain of the epochs for the second tag (for example "nontarget"). Shape is: n_channels x n_events x freq x time.
    arr_f: np.array
        Frequency positions of the heatmap cells (output of the scipy.signal.spectrogram).
    transform: str
        "stft" or "wt".
    config: dict
        Dictionary of configuration variables.

    Returns
    -------
    p_values: numpy.ndarray
        P values for cells between tags for every channel. Shape is - n_channels x freq x time. Values are between 0 and 1.
    p_rej: float
        Statistical significance after correction (because we have a lot of null hypotheses to test).
    p_signifiant_areas: numpy.ndarray
        Arrays with bool values - they calculated with logical condiction like this "p_values <= p_rej" and shows
        which cells are statistically significantly different.
    '''

    statistic = get_statistics(stat) # statistic
    alpha = config['time_frequency_domain']['statistics']['alpha'] # basic statistical significance
    f_lim = [config["time_frequency_domain"][transform]["f_lim_start"], config["time_frequency_domain"][transform]["f_lim_end"]]

    p_values = [] # matrix of p values
    p_rej = 0 # statistical significance after correction (because we have a lot of null hypotheses to tested)
    p_signifiant_areas = [] # matrix from condition p_values <= p_rej

    roi_f = np.where(((arr_f >= f_lim[0]) & (arr_f <= f_lim[1])))[0] # range of interest in frequency domain
    if len(roi_f) == 0:
        roi_f = np.where(arr_f >= f_lim[0])[0][0]

    roi_data1 = maps1[:, :, roi_f, :] # range of interest for maps1
    roi_data2 = maps2[:, :, roi_f, :] # range of interest for maps2

    for ch in range(maps1.shape[0]):
        p_ch = np.ones((maps1.shape[2], maps1.shape[3]))
        p_roi = np.ones((roi_data1.shape[2], roi_data1.shape[3]))

        for f in range(roi_data1.shape[2]):
            for t in range(roi_data1.shape[3]):
                p_roi[f, t] = statistic(roi_data1[ch, :, f, t].flatten(), roi_data2[ch, :, f, t].flatten(), config)

        p_ch[roi_f, :] = p_roi

        p_flat = np.sort(p_roi.flatten())
        p_rej = np.max(np.arange(1, len(p_flat), 1) * alpha/(len(p_flat) * np.sum(1/np.arange(1, len(p_flat), 1)))) #FDR correction

        p_values.append(p_ch)
        p_signifiant_areas.append(p_ch <= p_rej)

    return p_values, p_rej, p_signifiant_areas

#todo - dorobic sprawdzenie parametrycznosci i testy nieparametryczne

def get_statistics(stat):
    '''
    The function is choosing statistic's function wrote in the config file.

    Parameters
    ----------
    config: dict
        Dictionary of configuration variables.

    Returns
    -------
    statistic: function
        Choosen statistical function.
    '''

    if stat in ['t_test']:
        return t_test
    elif stat in ['welch_test']:
        return welch_test
    elif stat in ['MannWhitney_test']:
        return MannWhitney_test
    elif stat in ['bootstrap_pseudo_t_test']:
        return bootstrap_pseudo_t_test

def welch_test(maps1, maps2, config):
    '''
    The Welch test between one cell of maps1 and maps2.

    Parameters
    ----------
    maps1: numpy.ndarray
        One cell of maps in TF domain of the epochs for the first tag (for example "target").
    maps2: numpy.ndarray
        One cell of maps in TF domain of the epochs for the second tag (for example "nontarget").
    config: dict
        Dictionary of configuration variables.

    Returns
    -------
    p_value: float
        P value for test between vectors from maps1 and maps2.
    '''

    return st.ttest_ind(maps1, maps2, equal_var=False)[1]

def t_test(maps1, maps2, config):
    '''
    The t-student test between one cell of maps1 and maps2.

    Parameters
    ----------
    maps1: numpy.ndarray
        One cell of maps in TF domain of the epochs for the first tag (for example "target").
    maps2: numpy.ndarray
        One cell of maps in TF domain of the epochs for the second tag (for example "nontarget").
    config: dict
        Dictionary of configuration variables.

    Returns
    -------
    p_value: float
        P value for test between vectors from maps1 and maps2.
    '''

    return st.ttest_ind(maps1, maps2, equal_var=True)[1]

def MannWhitney_test(maps1, maps2, config): #todo - dla generowanych sygnalow nic nie zaznaczylo, sprawdzic to
    '''
    The Mann-Whitney test between one cell of maps1 and maps2.

    Parameters
    ----------
    maps1: numpy.ndarray
        One cell of maps in TF domain of the epochs for the first tag (for example "target").
    maps2: numpy.ndarray
        One cell of maps in TF domain of the epochs for the second tag (for example "nontarget").
    config: dict
        Dictionary of configuration variables.

    Returns
    -------
    p_value: float
        P value for test between vectors from maps1 and maps2.
    '''

    st.mannwhitneyu(maps1, maps2, alternative='two-sided')[1]

def bootstrap_pseudo_t_test(maps1, maps2, config):
    '''
    The "pseudo t-student test" based on bootstrap between one cell of maps1 and maps2.

    Parameters
    ----------
    maps1: numpy.ndarray
        One cell of maps in TF domain of the epochs for the first tag (for example "target").
    maps2: numpy.ndarray
        One cell of maps in TF domain of the epochs for the second tag (for example "nontarget").
    config: dict
        Dictionary of configuration variables.

    Returns
    -------
    p_value: float
        P value for test between vectors from maps1 and maps2.
    '''

    N_bootstrap = config['time_frequency_domain']['statistics']['n_bootstrap']
    all_data = np.concatenate((maps1, maps2))
    t0 = (np.mean(maps1) - np.mean(maps2))/(np.var(maps1) + np.var(maps2))
    counts = 0

    for _ in range(N_bootstrap):
        fake_maps1 = np.random.choice(all_data, np.shape(maps1)[0], replace=True)
        fake_maps2 = np.random.choice(all_data, np.shape(maps2)[0], replace=True)

        t = (np.mean(fake_maps1) - np.mean(fake_maps2))/(np.var(fake_maps1) + np.var(fake_maps2))

        counts += (np.abs(t) > np.abs(t0))

    return counts/N_bootstrap


