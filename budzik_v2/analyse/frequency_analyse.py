#Szymon Bocian 2023

import numpy as np
import matplotlib.pyplot as plt
import os

from ..plots.plots import plot_signal

def frequency_analysis(rest, epochs, config, path, file_name):
    '''
    Creates plots of the spectral density of the ERPs.

    Parameters
    ----------
    rest: mne.epochs.Epochs or None
        Prepared epochs from the signal. If None, REST path wasn't prepare in config file.
    epochs: mne.epochs.Epochs or None
        Prepared epochs from the signal. If None, EPOCHS path wasn't prepare in config file.
    config: dict
        Dictionary of configuration variables.
    path: str
        Path to the output directory.
    file_name: str
        Name of the file with raw signal.
    '''

    freq_path = os.path.join(path, "frequency_analyse")
    if not os.path.isdir(freq_path):
        os.mkdir(freq_path)

    # frequency for REST
    if rest != None:

        fmin = rest.info["highpass"] - 10
        fmin = 0 if fmin < 0 else fmin

        fmax = rest.info["lowpass"] + 10
        fmax = rest.info["sfreq"]/2 if fmax > rest.info["sfreq"]/2 else fmax

        for tag in rest.event_id.keys():

            fig = rest[tag].compute_psd(method=config["frequency_domain"]["estimation_method"], fmin=fmin, fmax=fmax).plot(show=False)
            fig.get_axes()[0].set_title(f'PSD {tag} ({len(rest[tag].events)}) {config["frequency_domain"]["estimation_method"]}')
            fig.set_dpi(1000)
            fig.savefig(os.path.join(freq_path, f"{os.path.splitext(file_name)[0]}___{tag}.png"))
            plt.close(fig)

    # frequency for EPOCHS
    if epochs != None:

        fmin = epochs.info["highpass"] - 10
        fmin = 0 if fmin < 0 else fmin

        fmax = epochs.info["lowpass"] + 10
        fmax = epochs.info["sfreq"]/2 if fmax > epochs.info["sfreq"]/2 else fmax

        for tag in epochs.event_id.keys():

            fig = epochs[tag].compute_psd(method=config["frequency_domain"]["estimation_method"], fmin=fmin, fmax=fmax).plot(show=False)
            fig.get_axes()[0].set_title(f'PSD {tag} ({len(epochs[tag].events)}) {config["frequency_domain"]["estimation_method"]}')
            fig.set_dpi(1000)
            fig.savefig(os.path.join(freq_path, f"{os.path.splitext(file_name)[0]}___{tag}.png"))
            plt.close(fig)