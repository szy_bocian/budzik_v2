import numpy as np
import pandas as pd
import os

def statistics_table(rest, epochs, data, tags_desc_id, path, file_name):
    '''
    Adding to data new columns with statistics calculated from each tag.

    Parameters
    ----------
    rest: mne.epochs.Epochs or None
        Prepared epochs from the signal. If None, REST path wasn't prepare in config file.
    epochs: mne.epochs.Epochs or None
        Prepared epochs from the signal. If None, EPOCHS path wasn't prepare in config file.
    data: pandas.core.frame.DataFrame
        Dataframe with basic information about tags.
    tags_desc_id: dict
        Dictionary of the names of prepared tags and their values in the prepared STIM signal.
    path: str
        Path to the output directory.
    file_name: str
        Name of the file with raw signal.
    '''

    stat_path = os.path.join(path, "stats")
    if not os.path.isdir(stat_path):
        os.mkdir(stat_path)

    data_help = {
        "id": [],
        "mean_amp": [], "std_amp": [], "median_amp": [], "min_amp": [], "max_amp": [], "range_amp": [],
        "mean_abs_amp": [], "std_abs_amp": [], "median_abs_amp": [], "min_abs_amp": [], "max_abs_amp": [], "range_abs_amp": [],
        "mean_spec_dens": [], "std_spec_dens": [], "median_spec_dens": [], "min_spec_dens": [], "max_spec_dens": [], "range_spec_dens": []
    }

    if rest is not None:
        rest = rest.copy()
        rest.reject = None
        rest.flat = None

        rest = rest.load_data().pick_types(eeg=True)

        for s, segment in enumerate(rest["rest"].get_data()):

            for ch, ch_name in enumerate(rest.info["ch_names"]):

                data_help["id"].append(f"rest_{ch_name}_{s}")

                # amplitude
                rest_epoch = rest["rest"].get_data()[s, ch, :]
                data_help["mean_amp"].append(np.mean(rest_epoch))
                data_help["std_amp"].append(np.std(rest_epoch, ddof=1))
                data_help["median_amp"].append(np.median(rest_epoch))
                data_help["min_amp"].append(np.min(rest_epoch))
                data_help["max_amp"].append(np.max(rest_epoch))
                data_help["range_amp"].append(np.max(rest_epoch) - np.min(rest_epoch))

                # absolute amplitude
                abs_rest_epoch = np.abs(rest_epoch)
                data_help["mean_abs_amp"].append(np.mean(abs_rest_epoch))
                data_help["std_abs_amp"].append(np.std(abs_rest_epoch, ddof=1))
                data_help["median_abs_amp"].append(np.median(abs_rest_epoch))
                data_help["min_abs_amp"].append(np.min(abs_rest_epoch))
                data_help["max_abs_amp"].append(np.max(abs_rest_epoch))
                data_help["range_abs_amp"].append(np.max(abs_rest_epoch) - np.min(abs_rest_epoch))

                # spectral density
                fft_rest_epoch = np.abs(np.fft.rfft(rest_epoch))**2
                data_help["mean_spec_dens"].append(np.mean(fft_rest_epoch))
                data_help["std_spec_dens"].append(np.std(fft_rest_epoch, ddof=1))
                data_help["median_spec_dens"].append(np.median(fft_rest_epoch))
                data_help["min_spec_dens"].append(np.min(fft_rest_epoch))
                data_help["max_spec_dens"].append(np.max(fft_rest_epoch))
                data_help["range_spec_dens"].append(np.max(fft_rest_epoch) - np.min(fft_rest_epoch))



    if epochs is not None:
        epochs = epochs.copy()
        epochs.reject = None
        epochs.flat = None

        epochs = epochs.load_data().pick_types(eeg=True)

        s = 0

        for epoch, epoch_event in zip(epochs.get_data(), epochs.events):

            for ch, ch_name in enumerate(epochs.info["ch_names"]):

                data_help["id"].append(f"{list(tags_desc_id.keys())[epoch_event[2]-1]}_{ch_name}_{s}")

                # amplitude
                data_help["mean_amp"].append(np.mean(epoch[ch, :]))
                data_help["std_amp"].append(np.std(epoch[ch, :], ddof=1))
                data_help["median_amp"].append(np.median(epoch[ch, :]))
                data_help["min_amp"].append(np.min(epoch[ch, :]))
                data_help["max_amp"].append(np.max(epoch[ch, :]))
                data_help["range_amp"].append(np.max(epoch[ch, :]) - np.min(epoch[ch, :]))

                # absolute amplitude
                abs_epoch = np.abs(epoch[ch, :])
                data_help["mean_abs_amp"].append(np.mean(abs_epoch))
                data_help["std_abs_amp"].append(np.std(abs_epoch, ddof=1))
                data_help["median_abs_amp"].append(np.median(abs_epoch))
                data_help["min_abs_amp"].append(np.min(abs_epoch))
                data_help["max_abs_amp"].append(np.max(abs_epoch))
                data_help["range_abs_amp"].append(np.max(abs_epoch) - np.min(abs_epoch))

                # spectral density
                fft_epoch = np.abs(np.fft.rfft(epoch[ch, :]))**2
                data_help["mean_spec_dens"].append(np.mean(fft_epoch))
                data_help["std_spec_dens"].append(np.std(fft_epoch, ddof=1))
                data_help["median_spec_dens"].append(np.median(fft_epoch))
                data_help["min_spec_dens"].append(np.min(fft_epoch))
                data_help["max_spec_dens"].append(np.max(fft_epoch))
                data_help["range_spec_dens"].append(np.max(fft_epoch) - np.min(fft_epoch))

            s += 1

    data = pd.merge(data, pd.DataFrame(data_help), on="id", how="inner")
    data.to_csv(os.path.join(stat_path, f"{os.path.splitext(file_name)[0]}___stat_table.csv"))
