#Szymon Bocian 2022

import os
import sys
import copy
from datetime import datetime

from .read_files.read_files import read_file, read_config_file, find_file_paths
from .preanalyse.preanalyse import preanalyse
from .analyse.analyse import analyse

def main(config_path):
    '''
    Main function - prepare analyses.

    Parameters
    ----------
    config_path: str
        Path to config.
    filelist: list
        List of files with signals.
    '''

    # file with parameters for everything - during process program will add few parameters
    # for other parts of analysis (like sfreq or tags), so we will hold original config
    config_raw = read_config_file(config_path)

    if "input_path" in config_raw.keys():
        if os.path.isdir(config_raw["input_path"]):  # what user gave - path to directory with files or path to specific file
            list_of_filepaths = find_file_paths(config_raw["input_path"])
        else:
            list_of_filepaths = [config_raw["input_path"]]  # list of files with signals
    else:
        sys.exit('ERROR: Provide "input_path" in the config file - this is the path to the data '
                 '(for example path to BrainTech data: C:\\Users\\User1\\raw_data.raw) or to the directory with data '
                 '(for example: C:\\Users\\User1\\rabbit_eeg_data).')

    if "output_path" in config_raw.keys():
        output_path_raw = config_raw["output_path"]  # path for output data
    else:
        sys.exit('ERROR: Provide "output_path" in the config file to the directory for output files (tables, plots etc.)'
                 ' - it should be path to folder (like C:\\Users\\User1\\output_data) or None (then it takes path from '
                 'the input).')

    print("MESSAGE: Signals found:\n    * " + "\n    * ".join(list_of_filepaths) + ".\n")

    for filepath in list_of_filepaths:

        print(f"\nMESSAGE: Starting work with {filepath}. \n         "
              f"Process started: {datetime.now().strftime('%d/%m/%Y %H:%M:%S')}.\n")
        config = copy.deepcopy(config_raw)

        # _____READING FILES_____
        signal, events_desc_id = read_file(filepath)  # reading files

        # if [None, None] is output of red_file, then something is wrong with input files
        if signal is None and events_desc_id is None:
            continue

        # creating or managing output path as output_path\budzik_output__file_name\YYYYMMDD_hhmmss_analysistype
        if output_path_raw in [None, "None", ""]:
            output_path, file_name = os.path.split(filepath)  # path to the file with file_name
        else:
            _, file_name = os.path.split(filepath)
            if not os.path.isdir(output_path_raw):
                os.mkdir(output_path_raw)
            output_path = output_path_raw

        file_name = os.path.splitext(file_name)[0]
        while "." in file_name:
            file_name = file_name.replace(".", "")  # if file_name has dots like signal.obci

        output_path = os.path.join(output_path, f"budzik_output__{file_name}")  # preparing folder for outputs
        if not os.path.isdir(output_path):
            os.mkdir(output_path)

        if not config["overwrite_output"]:
            output_path = os.path.join(output_path,
                                       f"{datetime.now().strftime('%Y%m%d_%H%M%S')}_{config['prepare_rest_or_epochs']}")
            if not os.path.isdir(output_path):
                os.mkdir(output_path)

        print(f"MESSAGE: Output files and plots you can find in the {output_path}.")

        # _____PREANALYSE_____
        print(f"\nMESSAGE: Preparing preanalyse for {filepath}.  \n         {datetime.now().strftime('%d/%m/%Y %H:%M:%S')}.\n")
        rest, rest_clean, epochs, epochs_clean, tags_desc_id, data, config = preanalyse(signal, config, output_path, events_desc_id, file_name)

        # ______ANALYSE_______
        print(f"\nMESSAGE: Preparing analyse for {filepath}.  \n         {datetime.now().strftime('%d/%m/%Y %H:%M:%S')}.\n")
        analyse(rest, rest_clean, epochs, epochs_clean, data, config, output_path, tags_desc_id, file_name)

        print(f"MESSAGE: Ending work with {filepath}.  \n         Process ended: {datetime.now().strftime('%d/%m/%Y %H:%M:%S')}.\n")

        #todo - zapisywanie wypełnionego configa do .toml