# Szymon Bocian 2022

import sys
import mne
import numpy as np
import pandas as pd
import scipy.signal as ss
import os
import matplotlib.pyplot as plt

from ..plots.plots import plots_for_filters, plot_BandPower


def resample_data(signal, config):
    '''
    Resample signal.

    Parameters
    ----------
    signal: mne.io.array.array.RawArray
        Signal in mne Raw format.
    config: dict
        Dictionary of configuration variables.

    Returns
    -------
    signal: mne.io.array.array.RawArray
        Signal in mne Raw format with new sampling frequency.
    '''

    if signal.info["sfreq"] > config["resample_freq"]:
        config["sfreq"] = config["resample_freq"]
        print(f"MESSAGE: Resample data with sampling frequency {signal.info['sfreq']} Hz to {config['resample_freq']}.")
        return signal.copy().resample(sfreq=config["resample_freq"])
    else:
        print(f"MESSAGE: Data with sampling frequency {signal.info['sfreq']} Hz will be not resample.")
        return signal.copy()


def chosen_channels(signals, tags_desc_id, config):
    '''
    Select and drop channels.

    Parameters
    ----------
    signals: list
        List of signals in mne Raw format (mne.io.array.array.RawArray) with the same info (so with the same channels).
    tags_desc_id: dict
        Dictionary of the names of prepared tags and their values in the new STIM signal.
    config: dict
        Dictionary of configuration variables.

    Returns
    -------
    signals: list
        List of signals in mne Raw format (mne.io.array.array.RawArray) with chosen channels.
    '''

    signals = [signal.copy() for signal in signals]

    print(f"MESSAGE: Data was provided with these channels: {', '.join(signals[0].info['ch_names'])}.")

    # ----selected channels----
    # list of selected channels
    if isinstance(config["choosing_channels"]["selected_channels"], list):
        config["choosing_channels"]["selected_channels"] = [ch for ch in config["choosing_channels"]["selected_channels"]
                                                            if ch in signals[0].info["ch_names"]]
        if "STIM" not in config["choosing_channels"]["selected_channels"]:
            config["choosing_channels"]["selected_channels"].append("STIM")

    # all selected channels
    elif config["choosing_channels"]["selected_channels"] in ["all", "every"]:
        config["choosing_channels"]["selected_channels"] = [ch for ch in signals[0].info["ch_names"]]

    # None selected channels
    elif config["choosing_channels"]["selected_channels"] in [None, "", "None", "none"]:
        print("WARNING: You must choose at least one channel. In the next step we will choose all channels.")
        config["choosing_channels"]["selected_channels"] = [ch for ch in config["choosing_channels"]["selected_channels"]
                                                            if ch in signals[0].info["ch_names"]]
        if "STIM" not in config["choosing_channels"]["selected_channels"]:
            config["choosing_channels"]["selected_channels"].append("STIM")

    print(f"MESSAGE: Selected channels are: {', '.join(config['choosing_channels']['selected_channels']) if isinstance(config['choosing_channels']['selected_channels'], list) else config['choosing_channels']['selected_channels']}.")


    # ----dropped channels----
    # list of dropped channels
    if isinstance(config["choosing_channels"]["dropped_channels"], list):
        config["choosing_channels"]["dropped_channels"] = [ch for ch in config["choosing_channels"]["dropped_channels"] if
                                                           ch in signals[0].info["ch_names"]]
        if "STIM" in config["choosing_channels"]["dropped_channels"]:
            config["choosing_channels"]["dropped_channels"].remove("STIM")

    # None dropped channels
    elif config["choosing_channels"]["dropped_channels"] in [None, "", "None", "none"]:
        config["choosing_channels"]["dropped_channels"] = []

    # all dropped channels
    elif config["choosing_channels"]["dropped_channels"] in ["all", "every"]:
        print("WARNING: You can't dropped all channels. None of channels will be dropped.")
        config["choosing_channels"]["dropped_channels"] = []

    print(f"MESSAGE: Dropped channels are: {', '.join(config['choosing_channels']['dropped_channels'])}.")

    # ----final channels----
    config["choosing_channels"]["selected_channels_final"] = [ch for ch in signals[0].info['ch_names'] if ch in config["choosing_channels"]["selected_channels"] and ch not in config["choosing_channels"]["dropped_channels"]]
    config["choosing_channels"]["dropped_channels_final"] = [ch for ch in signals[0].info["ch_names"] if ch in config["choosing_channels"]["dropped_channels"] or ch not in config["choosing_channels"]["selected_channels_final"]]

    if config["choosing_channels"]["check_final_channels"]:
        signals[0].info["bads"].extend(config["choosing_channels"]["dropped_channels_final"])
        signals[0].plot(events=mne.find_events(signals[0], stim_channel="STIM"), event_id=tags_desc_id, block=True)

        config["choosing_channels"]["dropped_channels_final"] = signals[0].info["bads"]
        if "STIM" in config["choosing_channels"]["dropped_channels_final"]:
            config["choosing_channels"]["dropped_channels_final"].remove("STIM")

        config["choosing_channels"]["selected_channels_final"] = [ch for ch in config["choosing_channels"]["selected_channels_final"] if ch not in config["choosing_channels"]["dropped_channels_final"] and ch in signals[0].info['ch_names']]
        if "STIM" not in config["choosing_channels"]["selected_channels_final"]:
            config["choosing_channels"]["selected_channels_final"].append("STIM")

        signals[0].info["bads"] = []

    signals = [signal.drop_channels(config["choosing_channels"]["dropped_channels_final"]) for signal in signals]
    print(f"MESSAGE: Final channels are: {', '.join(config['choosing_channels']['selected_channels_final'])}.")


    return signals


def baseline(signal, config):
    '''
    Preparing baseline for signal.

    Parameters
    ----------
    signal: mne.io.array.array.RawArray
        Signal in mne Raw format.
    config: dict
        Dictionary of configuration variables.

    Returns
    -------
    signal: mne.io.array.array.RawArray
        Signal in mne Raw format.
    '''

    if config["baseline"] in ["", None, "None", False, "False", 0, " "]:
        print(f"MESSAGE: Baseline will be not prepared.")
        return signal
    else:
        numpy_signal_channels = signal.get_data(picks=["eeg"])
        numpy_signal_nonchannels = signal.get_data(
            picks=[i for i in list(set(signal.get_channel_types())) if i != "eeg"])

        print(f"MESSAGE: Baseline for data is: {config['baseline']}.")

        if config["baseline"] == "mean":
            numpy_signal_channels = numpy_signal_channels - np.repeat(np.mean(numpy_signal_channels, axis=1),
                                                                      numpy_signal_channels.shape[1]).reshape(
                numpy_signal_channels.shape)
            return mne.io.RawArray(np.r_[numpy_signal_channels, numpy_signal_nonchannels], signal.info)

        elif config["baseline"] == "median":
            numpy_signal_channels = numpy_signal_channels - np.repeat(np.median(numpy_signal_channels, axis=1),
                                                                      numpy_signal_channels.shape[1]).reshape(
                numpy_signal_channels.shape)
            return mne.io.RawArray(np.r_[numpy_signal_channels, numpy_signal_nonchannels], signal.info)

        else:
            sys.exit("ERROR: Bad baseline method - should be mean or median. ")


def montage_and_reference(signal, config):
    '''
    Preparing montage and reference for signal.

    Parameters
    ----------
    signal: mne.io.array.array.RawArray
        Signal in mne Raw format.
    config: dict
        Dictionary of configuration variables.

    Returns
    -------
    signal: mne.io.array.array.RawArray
        Signal in mne Raw format after montage and reference.
    '''

    signal = signal.copy()

    # preparing montage
    if config["electrodes_layout"] in ["None", None, ""]:
        config["electrodes_layout"] = "standard_1005"
        signal.set_montage("standard_1005")
    else:
        try:
            signal.set_montage(config["electrodes_layout"])
        except:
            sys.exit("ERROR: Pick montage provided by MNE (more: https://mne.tools/dev/auto_tutorials/intro/40_sensor_locations.html#sphx-glr-auto-tutorials-intro-40-sensor-locations-py).")
    print(f"MESSAGE: Picked electrodes layout is {config['electrodes_layout']}.")

    # preparign rereference
    if config["re_reference"] in ["None", None, [], ""]:
        print(f"MESSAGE: Rereference will be not prepare.")
        return signal

    elif config["re_reference"] in ["average", "mean", "all", "CAR", "car"]:
        print(f"MESSAGE: Rereference will be Common Average Reference (CAR).")
        return signal.set_eeg_reference(ref_channels="average")

    else:
        ch_ref = [ch for ch in config["re_reference"] if
                  ch not in signal.info["bads"] and ch in signal.info["ch_names"]]
        signal.set_eeg_reference(ref_channels=ch_ref)
        signal.info["bads"].extend(ch_ref)
        if "bad_channels" in config["choosing_channels"]:
            config["choosing_channels"]["bad_channels"].extend(ch_ref)
        else:
            config["choosing_channels"]["bad_channels"] = ch_ref
        signal.pick(picks=None, exclude="bads")
        print(f"MESSAGE: Rereferance channels are: {', '.join(ch_ref)}")
        return signal


def EOG_with_ICA(signal, config, path, file_name):
    '''
    Preparing ICA for signal and deleting EOG artifacts.

    Parameters
    ----------
    signal: mne.io.array.array.RawArray
        Signal in mne Raw format.
    config: dict
        Dictionary of configuration variables.
    path: str
        Path to the output directory.
    file_name: str
        Name of the file with raw signal.

    Returns
    -------
    ica: mne.preprocessing.ica.ICA
        ICA computed with signal. It contains ica.exclude - components classified as EOG source.
    '''

    ica_path = os.path.join(path, "artifacts_detection")
    if not os.path.isdir(ica_path):
        os.mkdir(ica_path)

    ica_path = os.path.join(ica_path, "ICA_EOG")
    if not os.path.isdir(ica_path):
        os.mkdir(ica_path)

    title = ""

    # searching for channels
    signal = signal.copy()
    eog_channels_names = [ch for ch in config["ICA_EOG"]["eog_channels_names"] if
                          ch in signal.info["ch_names"] and ch not in signal.info["bads"]]
    if len(eog_channels_names) == 0:
        print("WARNING: Provide channels for ICA that are not chosen because they classified as 'bad' or they are not in the signal. Automatic ICA will be not prepared.")
        if config["ICA_EOG"]["choose_bad_ICA_source"] == "auto":
            return signal
        else:
            config["ICA_EOG"]["choose_bad_ICA_source"] = "manually"
    else:
        eog_channels_names = eog_channels_names[0]  # taking first channel after compare eog_channels_names from config with left channels and bad channels

    if config["ICA_EOG"]["n_components"] == "all":
        n_components = len(mne.pick_types(signal.info, eeg=True, eog=True))
    elif isinstance(config["ICA_EOG"]["n_components"], int) or isinstance(config["ICA_EOG"]["n_components"], float):
        n_components = config["ICA_EOG"]["n_components"]
    else:
        n_components = None

    # fitting ICA
    ica = mne.preprocessing.ICA(method=config["ICA_EOG"]["ica_method"], n_components=n_components)
    ica.fit(signal, picks=mne.pick_types(signal.info, eeg=True, eog=True, exclude="bads"))

    # searching for EOG components
    if config["ICA_EOG"]["choose_bad_ICA_source"] in ["auto", "both"]:
        print(f"MESSAGE: Channel that will be used to calculate correlations is: {eog_channels_names}.")

        scores = ica.score_sources(signal, target=eog_channels_names, l_freq=config["ICA_EOG"]["score_l_freq"], h_freq=config["ICA_EOG"]["score_h_freq"])

        bads = list(np.arange(scores.size)[np.abs(scores) > config["ICA_EOG"][
            "score_correlation_treshhold"]])  # list of components from EOG
        if len(bads) == 0 and max(np.abs(scores)) > config["ICA_EOG"]["score_correlation_treshhold"] / 2.:
            bads = [np.argmax(np.abs(scores))]

        if bads:
            winnig_correlations = np.abs(scores[bads])
        else:
            winnig_correlations = None

        log = open(os.path.join(ica_path, file_name + '_ICA_correlation_scores.txt'), 'w')
        log.write('CORRELATION SCORES:\n')
        log.write(f'component_number score abs(score) > {config["ICA_EOG"]["score_correlation_treshhold"]}\n')
        for nr, score in enumerate(scores):
            msg = '{} {} {}'.format(nr, score,
                                    '*' if np.abs(score) > config["ICA_EOG"]["score_correlation_treshhold"] else ' ')
            log.write(msg + '\n')
        log.close()

        if np.any(winnig_correlations):
            title = 'Components with EOG: {}, Corr. with {} = {} > thr = {}'.format(bads, eog_channels_names, winnig_correlations, config["ICA_EOG"]["score_correlation_treshhold"])
        else:
            title = 'Components with EOG: None, (corr. with {}), thr = {}'.format(eog_channels_names, config["ICA_EOG"]["score_correlation_treshhold"])

        if len(bads) > 0:
            ica.exclude.extend(bads)

    # showing ICA sources
    if config["ICA_EOG"]["choose_bad_ICA_source"] in ["manually", "both"]:
        ica.plot_sources(signal, block=True)
        if title == "":
            title = 'Components by hand: {}.'.format(", ".join(map(str, ica.exclude)))

    # plots
    fig_comp = ica.plot_components(res=128, show=False, title=title, colorbar=True)
    if isinstance(fig_comp, list):
        for nr, fig_c in enumerate(fig_comp):
            fig_c.savefig(os.path.join(ica_path, file_name + f'_ICA_component_maps_{nr}.png'))
            plt.close(fig_c)
    else:
        fig_comp.savefig(os.path.join(ica_path, file_name + '_ICA_component_maps.png'))
        plt.close(fig_comp)

    fig_properties = ica.plot_properties(signal, show=False, picks=np.arange(0, ica.n_components_))
    for nr, fig_p in enumerate(fig_properties):
        fig_p.savefig(os.path.join(ica_path, file_name + f'_ICA_component_properties_{nr}.png'))
        plt.close(fig_p)

    # saving ica
    ica.save(os.path.join(ica_path, file_name + '-ica.fif'), overwrite=True)
    config["ICA_EOG"]["bad_sources"] = ica.exclude

    if len(config['ICA_EOG']['bad_sources']) > 1:
        print(f"MESSAGE: ICA components classified as bad: {', '.join(list(map(str, config['ICA_EOG']['bad_sources'])))}.")
    elif len(config['ICA_EOG']['bad_sources']) == 1:
        print(f"MESSAGE: ICA components classified as bad: {config['ICA_EOG']['bad_sources'][0]}.")
    else:
        print(f"MESSAGE: ICA components classified as bad: None.")

    return ica


def prepare_tags_and_rest(signal, config, events_desc_id):
    '''
    Preparing informations and descriptions about tags from events and rest segments.

    Parameters
    ----------
    signal: mne.io.array.array.RawArray
        Signal in mne Raw format.
    config: dict
        Dictionary of configuration variables.
    events_desc_id: dict
        Dictionary of the names of events and their values in the STIM signal.

    Returns
    -------
    signal: mne.io.array.array.RawArray
        Signal in mne Raw format with prepared STIM channel.
    tags_desc_id: dict
        Dictionary of the names of prepared tags (with rest segments) and their values in the new STIM signal.
    config: dict
        Dictionary of configuration variables.
    '''

    # preparing tags info and occurrence
    tags_desc_id = {}
    numpy_signal = signal.get_data()
    stim_signal = np.zeros(numpy_signal.shape[1])
    stim_idx = signal.info["ch_names"].index("STIM")

    #preparing tags
    if "epochs" in config.keys() and "tags" in config["epochs"].keys() and config["prepare_rest_or_epochs"] == "epochs":

        # automatic finder of the targets and nontargets
        if "AUTO" in config["epochs"]["tags"].keys():

            if config["epochs"]["tags"]["AUTO"] == True:
                config["epochs"]["tags"] = {}
                config["epochs"]["tags"]["target"] = []
                config["epochs"]["tags"]["nontarget"] = []
                for event in events_desc_id.keys():
                    if "nontarget" in event:
                        config["epochs"]["tags"]["nontarget"].append(event)
                    elif "target" in event:
                        config["epochs"]["tags"]["target"].append(event)

                if len(config["epochs"]["tags"]["target"]) == 0:
                    sys.exit("ERROR: Automatic finder of targets returns empty list - provide the exact names of target and nontarget events in the 'tags' sector of the config file.")

                if len(config["epochs"]["tags"]["nontarget"]) == 0:
                    sys.exit("ERROR: Automatic finder of nontargets returns empty list - provide the exact names of target and nontarget events in the 'tags' sector of the config file.")
            else:

                del config["epochs"]["tags"]["AUTO"] #we want only tags - if AUTO

        # creating description of tags and their id's
        for i, tag in enumerate(config["epochs"]["tags"].keys()):
            tags_desc_id[tag] = i+1

        # creating tags in stim channel
        for event in events_desc_id.keys():
            for tag in config["epochs"]["tags"].keys():
                if event in config["epochs"]["tags"][tag]:
                    stim_signal[numpy_signal[stim_idx, :] == events_desc_id[event]] = tags_desc_id[tag]

        print(f"MESSAGE: Data has {np.sum(stim_signal != 0)} epochs.")

    # preparing rest segments
    if "rest" in config.keys() and config["prepare_rest_or_epochs"] == "rest":

        start_rest_n, stop_rest_n = config["rest"]["rest_duration"][0] * signal.info["sfreq"], config["rest"]["rest_duration"][1] * signal.info["sfreq"]
        rest_window_n = config["rest"]["window_length"] * signal.info["sfreq"]
        rest_id = np.max(list(tags_desc_id.values()))+1 if len(tags_desc_id.values()) > 0 else 1

        # todo - na przyszlosc, gdy bedziemy chcieli robic obie ściezki na raz, to trzeba to tutaj poprawić
        # checking if stop_rest_n is greater than first event - if so, stop_rest will be before 1.5 rest_window plus start_offset of the epoch
        # if stop_rest_n >= np.where(numpy_signal[signal.info["ch_names"].index("STIM"), :] > 0)[0][0]:
        #     stop_rest_n = np.where(numpy_signal[signal.info["ch_names"].index("STIM"), :] > 0)[0][0] - (1.5*rest_window_n - config["epochs"]["start_offset"])
        #     config["rest"]["rest_duration"][1] = stop_rest_n/signal.info["sfreq"]

        # creating stim points for rest
        for id in [int(start_rest_n+i*rest_window_n) for i in range(int((stop_rest_n - start_rest_n)//rest_window_n))]:
            stim_signal[id] = rest_id

        # adding id of the rest for tags description
        tags_desc_id["rest"] = rest_id

        print(f"MESSAGE: Data has {np.sum(stim_signal == rest_id)} rest segments.")

    # checking existence of two paths - epochs and rest
    if ("epochs" in config.keys() and config["prepare_rest_or_epochs"] == "epochs") or ("rest" in config.keys() and config["prepare_rest_or_epochs"] == "rest"):
        numpy_signal[stim_idx, :] = stim_signal
    else:
        sys.exit("ERROR: Prepare at least one module in config file - for 'rest' path or for 'epochs' path.")

    # saving sfreq and channels for future
    config["sfreq"] = signal.info["sfreq"]
    config["choosing_channels"]["all_channels_names"] = signal.info["ch_names"]
    config["choosing_channels"]["all_channels_types"] = [mne.channel_type(signal.info, i) for i in range(len(signal.info["ch_names"]))]

    return mne.io.RawArray(numpy_signal, signal.info), tags_desc_id, config


def pick_signal(signal, config):
    '''
    The function cuts signal around events and rest with additional signal and connects it to the one continuous signal.
    This is possible thanks to two parameters from the config: cover_time and threshold_time. Signal has groups of
    events (for example survey was really long and patient had several breaks) with resting time for patient between
    them. The cover_time adds a signal before the first event of the group and after the last event of the group
    (the same with rest). The threshhold_time checks gaps between groups of the events - if the gap is longer than
    this parameter, groups won't be connected.

    For example: we have 2 min signal: rest in 1-20 s, 3 events in 25s, 30s, 35s and 2 events in 60s and 65s.
    The cover_time = 2s, and threshhold_time = 10s. So we will have two groups 1s - 37s and 58 - 67s. This two groups
    will be cut from signal and will be connect to one continuous signal.

    However, if cover_time = None and threshold_time = None, all signal will before first tag (rest or event) minus 5
    seconds will be not taken for the rest of the analysis.

    Parameters
    ----------
    signal: mne.io.array.array.RawArray
        Signal in mne Raw format.
    config: dict
        Dictionary of configuration variables.

    Returns
    -------
    signal: mne.io.array.array.RawArray
        Signal in mne Raw format without signal between groups of events.
    picked_signal: numpy.ndarray
        The array which shows which parts of the signal were taken.
    '''

    if "cover_time" in config and "threshold_time" in config:

        if config["cover_time"] != None and config["threshold_time"] != None:

            numpy_signal = signal.get_data()
            picked_signal = np.zeros(np.shape(numpy_signal)[1])
            stim_signal = numpy_signal[signal.info["ch_names"].index("STIM"), :]

            n_cover, n_threshold = int(config["cover_time"] * signal.info["sfreq"]), int(config["threshold_time"] * signal.info["sfreq"])

            # picking signal from rest path
            if "rest" in config.keys() and config["prepare_rest_or_epochs"] == "rest":
                picked_signal[int(config["rest"]["rest_duration"][0] * signal.info["sfreq"]):int(config["rest"]["rest_duration"][1] * signal.info["sfreq"])] = 1 # resting signal
                rest_idx = np.max(stim_signal)
            else:
                rest_idx = np.max(stim_signal)+1

            # picking signal from epoch path
            if "epochs" in config.keys() and config["prepare_rest_or_epochs"] == "epochs":

                stim_signal = np.where((stim_signal > 0) & (stim_signal != rest_idx))[0] # taking positions of epochs without rest

                picked_signal[stim_signal[0] - n_cover:stim_signal[0]] = 1 # first epoch
                picked_signal[stim_signal[-1]:stim_signal[-1] + n_cover] = 1 # last epoch

                # checking if "rest" is in the n_threshold before first epoch
                if np.sum(picked_signal[stim_signal[0] - n_cover - n_threshold:stim_signal[0] - n_cover]) > 0:
                    picked_signal[stim_signal[0] - n_cover - n_threshold:stim_signal[0] - n_cover] = 1

                for i in range(len(stim_signal[:-1])):

                    picked_signal[stim_signal[i]] = 1

                    # checking if the length between event i and i+1 are greater than n_threshold
                    if stim_signal[i + 1] - stim_signal[i] > 1 and stim_signal[i + 1] - stim_signal[i] < n_threshold:
                        picked_signal[stim_signal[i]:stim_signal[i + 1]] = 1

                    # if length between event i and i+1 are greater than n_threshold - event i is the last event in this group and event i+1 is the first in the next group, so we add cover_time to them
                    else:
                        picked_signal[stim_signal[i]:stim_signal[i] + n_cover] = 1 # i event
                        picked_signal[stim_signal[i + 1] - n_cover:stim_signal[i + 1]] = 1 # i+1 event

        else:

            numpy_signal = signal.get_data()
            picked_signal = np.zeros(np.shape(numpy_signal)[1])
            stim_signal = numpy_signal[signal.info["ch_names"].index("STIM"), :]
            first_tag = np.where(stim_signal != 0)[0][0]

            if first_tag - int(signal.info["sfreq"] * 5) > 0:
                picked_signal[first_tag - int(signal.info["sfreq"] * 5):] = 1
            else:
                picked_signal = np.ones(np.shape(numpy_signal)[1])


    else:

        numpy_signal = signal.get_data()
        picked_signal = np.ones(np.shape(numpy_signal)[1])

    return mne.io.RawArray(numpy_signal[:, picked_signal == 1], signal.info), picked_signal


def Power(channel_signal, width, band, Fs):
    '''
    Marian Dovgialo 2019 from helper_functions\helper_functions.py

    Finding the estimated power of signal s in the defined frequency band.

    Parameters
    ----------
    channel_signal: list
        List of the potentials from one channel from the signal.
    width: int
        The duration of the window (not in time, it the points).
    band: list
        Two-element list with the beginning and end of the frequency band.
    Fs: float
        Frequency sampling of the signal s.

    Returns
    -------
    ps: list
        List of the estimated power for frequencies from band [freq[a], freq[b]].
    freq[a]: float
         The beginning of the frequency band of the estimated power.
    freq[b]: float
         The end of the frequency band of the estimated power.
    '''

    ps = []
    freq = np.fft.rfftfreq(width, 1. / Fs)
    a = np.argmin(np.abs(band[0] - freq))
    b = np.argmin(np.abs(band[1] - freq))
    if a == b:
        print("WARNING: In this range {} is only one frequency {}".format(band, freq[a]))

    window = ss.windows.hann(width)
    for i in range(0, channel_signal.size // width * width, width):
        f = np.fft.rfft(window * channel_signal[i:i + width], norm="ortho")
        p = np.mean(np.abs(f[a:b + 1]) ** 2)
        ps.append(p)

    ps = np.array(ps)
    return ps, (freq[a], freq[b])


def BandPower(signal, width=2., band=[49.5, 50.5], thrs_type="-", thrs=[1e5, 1e6, 1e7], path="\\", file_name="signal"):
    """
    Marian Dovgialo 2019 from https://gitlab.com/BudzikFUW/budzik-analiza-py-3.git helper_functions\helper_functions.py

    Function will cut windows from signal and estimated power for specified frequency band for every channel. After this
    it will find how many points crossed thresholds (sum of number of points which crossed thrs[1] and doubled number of
    points which crossed thrs[2]). If number of this points are higher than 20% of all points, channel will be classified
    as "bad". In the next step function will find automatic threshold which depends on number of points which crossed
    thrs for all channels. Finally, when number of points which crossed thrs for specific channel is greater than
    automatic threshold, then this channel will be classified as "bad" (if it wasn't classified in the first time).

    The function will draw estimated power for windows with thresholds too. The color of hotizontal lines are:
        * green -> thrs[0]
        * yellow -> thrs[1]
        * red -> thrs[2]
    The face color of the plot will be:
        * green -> all points are below/above thrs[0]
        * white -> number of points, that crossed thresholds, is smaller than 10%
        * yellow -> number of points, that crossed thresholds, is between 10% and 20%
        * red -> number of points, that crossed thresholds, is greater than 20%
        * orange -> number of points, that crossed thresholds, is greater than the automated threshold

    Parameters
    ----------
    signal: mne.io.array.array.RawArray
        Signal in mne Raw format.
    width: float
        Function will cut windows from signal with this width (in seconds) and calculate spectral density for them.
    band: list
        List with two floats/integers representing frequency band for the noise.
    thrs_type: str
        Funtion check two possible types:
            "+" -> power above thrs will be classified as "good" (the more points above thrs[0], the better)
            "-" -> power below thrs will be classified as "good" (the more points below thrs[0], the better)
    thrs: list
        List with three floats/integers (in μV^2) for thresholds to calculate which channel has too much noise. So if
        thrs_type is:
            "+" -> then thrs[0] > thrs[1] > thrs[2]
            "-" -> then thrs[0] < thrs[1] < thrs[2]
    path: str
        Path to the output directory.
    file_name: str
        Name of the file with raw signal.

    Returns
    -------
    Pgs: dict
        Estimated power from windows for channels - {"ch1": [...], "ch2": [...], ...}.
    BadChannels: list
        List of the channels which classified as "bad".
    """
    thrs = np.array(thrs)
    Fs = signal.info["sfreq"]
    width = int(width * Fs) + 1
    numpy_signal_channels = signal.get_data(picks=["eeg"])
    T = np.arange(0, numpy_signal_channels.shape[1] / Fs, 1. * width / Fs)
    channels = [signal.info["ch_names"][i] for i in mne.pick_types(signal.info, eeg=True)]
    plt_facecolor = None

    # fig, axs = pb.subplots(5, 5, figsize=(5 * 5 * 2, 2.5 * 5 * 2))
    # for ch in channels_not2draw(channels):
    #     pos = map1020[ch]
    #     axs[pos.y, pos.x].axis("off")

    A = []
    Pgs = dict()
    for ch, channel in enumerate(channels):
        c = numpy_signal_channels[ch, :]
        Pg, RealBand = Power(c, width, band, Fs)

        Pgs[channel] = Pg
        A.append(Pg)
    A = np.array(A)

    # fig.canvas.manager.set_window_title("Energy in band [{:.2f}, {:.2f}]".format(*RealBand))

    ymax = np.max((np.max(A), thrs[2])) * 2.
    ymin = np.min((np.min(A), thrs[0])) / 2.
    if ymax > 10 * max(thrs):
        ymax = 10 * max(thrs)

    BadScore = {}
    BadChannels = set()

    for ch, channel in enumerate(channels):
        Pg = Pgs[channel]

        t = T[:Pg.size]

        # pos = map1020[ch]
        # ax = axs[pos.y, pos.x]
        # ax.plot(t, Pg)
        # ax.plot([t[0], t[-1]], [thrs[0], thrs[0]], 'g--')
        # ax.plot([t[0], t[-1]], [thrs[1], thrs[1]], 'y--')
        # ax.plot([t[0], t[-1]], [thrs[2], thrs[2]], 'r--')
        #
        # ax.set_title(ch)
        # ax.set_yscale('log')
        # ax.set_ylim([ymin, ymax])
        # ax.set_xlim((t[0], t[-1]))

        if thrs_type == "-":  # below is good
            BadScore[channel] = np.sum(Pg > thrs[1]) + 2 * np.sum(Pg > thrs[2])
            # if BadScore[ch] > 0.1 * Pg.size:
            #     ax.set_facecolor((0.95, 0.95, 0.65))  # żółty kolor
            # if BadScore[ch] > 0.2 * Pg.size:
            #     ax.set_facecolor((1.00, 0.70, 0.70))  # różowy kolor
            # if Pg.max() < thrs[0]:
            #     ax.set_facecolor((0.80, 1.00, 0.80))  # zielony kolor

            if BadScore[channel] > 0.1 * Pg.size:
                plt_facecolor = (0.95, 0.95, 0.65)  # żółty kolor
            if BadScore[channel] > 0.2 * Pg.size:
                plt_facecolor = (1.00, 0.70, 0.70)  # różowy kolor
                BadChannels.add(channel)
            if Pg.max() < thrs[0]:
                plt_facecolor = (0.80, 1.00, 0.80)  # zielony kolor

        if thrs_type == "+":  # above is good
            BadScore[channel] = np.sum(Pg < thrs[1]) + 2 * np.sum(Pg < thrs[2])
            # if BadScore[ch] > 0.1 * Pg.size:
            #     ax.set_facecolor((0.95, 0.95, 0.65))  # żółty kolor
            # if BadScore[ch] > 0.2 * Pg.size:
            #     ax.set_facecolor((1.00, 0.70, 0.70))  # różowy kolor
            #     BadChannels.add(ch)
            # if Pg.min() > thrs[0]:
            #     ax.set_facecolor((0.80, 1.00, 0.80))  # zielony kolor

            if BadScore[channel] > 0.1 * Pg.size:
                plt_facecolor = (0.95, 0.95, 0.65)  # żółty kolor
            if BadScore[channel] > 0.2 * Pg.size:
                plt_facecolor = (1.00, 0.70, 0.70)  # różowy kolor
                BadChannels.add(channel)
            if Pg.min() > thrs[0]:
                plt_facecolor = (0.80, 1.00, 0.80)  # zielony kolor


    # fig.subplots_adjust(left=0.03, bottom=0.03, right=0.98, top=0.98, wspace=0.16, hspace=0.4)
    # plt.tight_layout()

    BS_list = np.array(list(BadScore.values()))

    Thr = np.inf
    s_rel = 4
    thr = np.median(BS_list) + s_rel * np.std(BS_list[BS_list < Thr], ddof=1)
    while thr < Thr:
        Thr = thr
        thr = np.median(BS_list) + s_rel * np.std(BS_list[BS_list < Thr], ddof=1)

    if Thr < 0.2 * Pg.size:
        # print("Thr", Thr, "New Thr", 0.2 * Pg.size)
        Thr = 0.2 * Pg.size

    if Thr > 0.6 * Pg.size:
        # print("Thr", Thr, "New Thr", 0.6 * Pg.size)
        Thr = 0.6 * Pg.size

    for channel in channels:
        # print(channel, BadScore[channel], BadScore[channel] > Thr, Thr)
        if BadScore[channel] > Thr:
            # # pos = map1020[ch]
            # # ax = axs[pos.y, pos.x]
            # ax.set_facecolor((1.00, 0.60, 0.40))  # pomarańczowy kolor
            plt_facecolor = (1.00, 0.60, 0.40)
            BadChannels.add(channel)

        plot_BandPower(T[:Pgs[channel].size], Pgs[channel], thrs, channel, band, width/Fs, ymin, ymax, plt_facecolor, path, file_name)

    badscore_info = pd.DataFrame({"channels": channels,
                     "bad_samples [%]": [BadScore[channel]/Pgs[channel].size * 100 for channel in channels],
                     "bad_channel": ["*" if channel in BadChannels else "" for channel in channels]
                     })
    with open(os.path.join(path, f"{file_name}___{band[0]}_{band[1]}Hz_{width/Fs}s.txt"), 'a') as f:
        f.write(f"n_windows = {Pg.size}\nauto_threshold = {Thr/Pg.size}\n\n")
        f.write(badscore_info.to_string(header=True, index=False))
    f.close()


    # print("BdCh", zip(BadChannels, [BadScore[ch] for ch in BadChannels]))
    # print()

    return Pgs, BadChannels


def ValidateChannels_Noise(signal, width, band, thresholds_type, thresholds, path, file_name):
    '''
    Marcin Pietrzak 2017-03-01 based on Klekowicz et al. DOI 10.1007/s12021-009-9045-2 from
    https://gitlab.com/BudzikFUW/budzik-analiza-py-3.git helper_functions\artifacts_mark.py

    Function finds bad channels with calculating power in the band and classifying bad channels. From budzik-analiza
    params for checking channels for noise are:
        width = 1.0
        band = [52, 98]
        thresholds_type = "-"
        thresholds = [2e1, 2e2, 2e3]

    Parameters
    ----------
    signal: mne.io.array.array.RawArray
        Signal in mne Raw format.
    width: float
        Function will cut windows from signal with this width and calculate spectral density for them.
    band: list
        List with two floats/integers representing frequency band for the noise.
    thresholds_type: str
        "+" (power above thresholds will be classified as good) or "-" (power below thresholds will be classified as good).
    thresholds: list
        List with three floats/integers (in μV^2) for thresholds to calculate which channel has too much noise.
    path: str
        Path to the output directory.
    file_name: str
        Name of the file with raw signal.

    Returns
    -------
    bad_channels: list
        List of the channels which classified as "bad".
    '''

    _, bad_channels = BandPower(signal, width, band, thresholds_type, thresholds, path, file_name)

    return bad_channels


def find_bad_channels(signal, config, tags_desc_id, path, file_name):
    """
    Finding bad channels with ValidateChannels_Noise function.

    Parameters
    ----------
    signal: mne.io.array.array.RawArray
        Signal in mne Raw format which is raw (without any preanalyse like filtering or montage).
    config: dict
        Dictionary of configuration variables.
    tags_desc_id: dict
        Dictionary of the names of prepared tags and their values in the new STIM signal.
    path: str
        Path to the output directory.
    file_name: str
        Name of the file with raw signal.

    Returns
    -------
    config: dict
        Dictionary of configuration variables with found bad channels.
    """

    bch_path = os.path.join(path, "bad_channels_detection")
    if not os.path.isdir(bch_path):
        os.mkdir(bch_path)

    signal = signal.copy()

    bad_channels = []
    if "bad_channels" not in config["choosing_channels"]:
        config["choosing_channels"]["bad_channels"] = []
    else:
        config["choosing_channels"]["bad_channels"] = [ch for ch in config["choosing_channels"]["bad_channels"] if ch in signal_clean.info["ch_names"]]

    if config["choosing_channels"]["choose_bad_channels"] in ["auto", "both"]:
        for badchs_method in config["choosing_channels"]["badchs_params"].keys():
            bad_channels.extend(ValidateChannels_Noise(signal,
                                                  config["choosing_channels"]["badchs_params"][badchs_method][0],
                                                  config["choosing_channels"]["badchs_params"][badchs_method][1],
                                                  config["choosing_channels"]["badchs_params"][badchs_method][2],
                                                  config["choosing_channels"]["badchs_params"][badchs_method][3],
                                                  bch_path, file_name))

        if len(bad_channels) > 0:
            bad_channels = list(set(bad_channels))
            config["choosing_channels"]["bad_channels"].extend(bad_channels)

    if config["choosing_channels"]["choose_bad_channels"] in ["manually", "both"]:
        if config["re_reference"] not in ["", [], None, "None", "none"]:
            signal.set_eeg_reference(ref_channels=["Cz"])
        signal.info['bads'].extend(bad_channels)
        scalings = dict(mag=1e-12, grad=4e-11, eeg=30e-6, eog=150e-6, ecg=5e-4, emg=1e-3, ref_meg=1e-12, misc=1e-3, stim=1, resp=1, chpi=1e-4, whitened=1e2)
        signal.plot(events=mne.find_events(signal, stim_channel="STIM"), event_id=tags_desc_id, block=True,
                    highpass=1,
                    lowpass=40, scalings=scalings)
        if "STIM" in signal.info["bads"]:
            signal.info["bads"].remove("STIM")
        bad_channels.extend(signal.info["bads"])
        bad_channels = list(set(bad_channels))
        config["choosing_channels"]["bad_channels"] = bad_channels

    if "STIM" in config["choosing_channels"]["bad_channels"]:
        config["choosing_channels"]["bad_channels"].remove("STIM")

    print(f"MESSAGE: Bad channels are: {', '.join(config['choosing_channels']['bad_channels'])}.")

    return config

def filtering(signal, config, path):
    '''
    Filtering signal from directory in path with filters in config["filters"]

    Parameters
    ----------
    signal: mne.io.array.array.RawArray
        Signal in mne Raw format.
    config: dict
        Dictionary of configuration variables.
    path: str
        Path to the output directory.

    Returns
    -------
    signal: mne.io.array.array.RawArray
        Signal in mne Raw format after filtration.
    '''

    if config["filters"]["plot_filt"] and not os.path.isdir(os.path.join(path, "filters_plot")):
        os.mkdir(os.path.join(path, "filters_plot"))

    signal = signal.copy()
    signal_info = dict(signal.info)
    numpy_signal_channels = signal.get_data(picks=["eeg"])
    numpy_signal_nonchannels = signal.get_data(picks=[i for i in list(set(signal.get_channel_types())) if i != "eeg"])

    # highpass filter
    if config["filters"]["highpass_name"] not in [None, "None", ""]:
        hfname = config["filters"]["highpass_name"]

        if hfname not in config["filters"].keys():
            sys.exit("ERROR: Provide name of the defined high pass filter.")

        try:
            b, a = ss.iirdesign(wp = config["filters"][hfname]["wp"],
                                ws = config["filters"][hfname]["ws"],
                                gpass = config["filters"][hfname]["gpass"],
                                gstop = config["filters"][hfname]["gstop"],
                                analog = 0,
                                ftype = config["filters"][hfname]["ftype"],
                                output = "ba",
                                fs = signal_info["sfreq"])

            if config["filters"][hfname]["use_filtfilt"]:
                numpy_signal_channels = ss.filtfilt(b, a, numpy_signal_channels)
            else:
                numpy_signal_channels = ss.lfilter(b, a, numpy_signal_channels)

            signal_info["highpass"] = config["filters"][hfname]["wp"]
            plots_for_filters(config, hfname, path, config["filters"]["show_filt"], config["filters"]["plot_filt"])
        except:
            sys.exit(f"ERROR: Check variables of the {hfname} filter - should be wp, ws, gpass, gstop, ftype and use_filtfilt. More: https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.iirdesign.html.")


    # lowpass filter
    if config["filters"]["lowpass_name"] not in [None, "None", ""]:
        lfname = config["filters"]["lowpass_name"]

        if lfname not in config["filters"].keys():
            sys.exit("ERROR: Provide name of the defined low pass filter.")

        try:
            b, a = ss.iirdesign(wp = config["filters"][lfname]["wp"],
                                ws = config["filters"][lfname]["ws"],
                                gpass = config["filters"][lfname]["gpass"],
                                gstop = config["filters"][lfname]["gstop"],
                                analog = 0,
                                ftype = config["filters"][lfname]["ftype"],
                                output = "ba",
                                fs = signal_info["sfreq"])

            if config["filters"][lfname]["use_filtfilt"]:
                numpy_signal_channels = ss.filtfilt(b, a, numpy_signal_channels)
            else:
                numpy_signal_channels = ss.lfilter(b, a, numpy_signal_channels)

            signal_info["lowpass"] = config["filters"][lfname]["wp"]
            plots_for_filters(config, lfname, path, config["filters"]["show_filt"], config["filters"]["plot_filt"])
        except:
            sys.exit(f"ERROR: Check variables of the {lfname} filter - should be wp, ws, gpass, gstop, ftype and use_filtfilt. More: https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.iirdesign.html.")


    # stopband filter
    if config["filters"]["stopband_name"] not in [None, "None", ""]:
        bfname = config["filters"]["stopband_name"]

        if bfname not in config["filters"].keys():
            sys.exit("ERROR: Provide name of the defined stop band filter.")

        try:
            b, a = ss.iirdesign(wp = config["filters"][bfname]["wp"],
                                ws = config["filters"][bfname]["ws"],
                                gpass = config["filters"][bfname]["gpass"],
                                gstop = config["filters"][bfname]["gstop"],
                                analog = 0,
                                ftype = config["filters"][bfname]["ftype"],
                                output = "ba",
                                fs = signal_info["sfreq"])

            if config["filters"][bfname]["use_filtfilt"]:
                numpy_signal_channels = ss.filtfilt(b, a, numpy_signal_channels)
            else:
                numpy_signal_channels = ss.lfilter(b, a, numpy_signal_channels)

            plots_for_filters(config, bfname, path, config["filters"]["show_filt"], config["filters"]["plot_filt"])
        except:
            sys.exit(f"ERROR: Check variables of the {bfname} filter - should be wp, ws, gpass, gstop, ftype and use_filtfilt. More: https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.iirdesign.html.")

    print(f"MESSAGE: Filters for provided data are:\n    * {hfname},\n    * {lfname},\n    * {bfname}.")

    return mne.io.RawArray(np.r_[numpy_signal_channels, numpy_signal_nonchannels], mne.Info(signal_info))

