# Wioleta Dołowa 2023
# Sebastian Kamiński 2023

from tqdm import tqdm
import xml.etree.ElementTree as ET
from importlib.resources import path
import numpy as np
import os

import cv2
from mtcnn import MTCNN

import sys

AGE_BUCKETS = ["0-2", "4-6", "8-12", "15-20", "25-32", "38-43", "48-53", "60-100"]
CUTOFF_AGE = 12


def is_facing_front(detection, tolerance=0.1):
    left_eye = detection['keypoints']['left_eye']
    right_eye = detection['keypoints']['right_eye']
    nose = detection['keypoints']['nose']
    a = abs(
        (left_eye[0]-nose[0])**2+(left_eye[1]-nose[1])**2
    )
    b = abs(
        (right_eye[0]-nose[0])**2+(right_eye[1]-nose[1])**2
    )
    a, b = min(a, b), max(a, b)

    if a/b < tolerance:
        return 0
    return 1


def write_tags_to_xml(tag_list, output_path):
    root = ET.Element('tagFile', formatVersion='1.0')
    ET.SubElement(root, 'paging', blocks_per_page="5", page_size="20.0")
    tagData = ET.SubElement(root, 'tagData')
    tags = ET.SubElement(tagData, 'tags')

    tags_description = []

    for tag in tag_list:
        start, stop = tag
        start = start / 1000 # convert back to s
        length = stop / 1000 - start
        ET.SubElement(tags, 'tag', type="CHANNEL", channelNumber="-1", length=str(length),
                      name="not_looking", position=str(start))
        tags_description.append({"channels_number": -1, "start_timestamp": start, "name": "not_looking", "end_timestamp": stop/1000, "desc": {"typ": "video"}})
    
    tree = ET.ElementTree(root)
    ET.indent(tree, space="\t", level=0)
    tree.write(output_path, xml_declaration=True, encoding="utf-8")

    return tags_description


def init_age_model():
    prototxtPath = os.path.join(os.path.dirname(__file__), 'facetag_models/deploy.prototxt')
    weightsPath = os.path.join(os.path.dirname(__file__), 'facetag_models/age_net.caffemodel')
    ageNet = cv2.dnn.readNet(prototxtPath, weightsPath)
    
    return ageNet


def check_frame_of_interest(image, detections, age_model):        
    if len(detections) > 0:        
        for _, detection in enumerate(detections):
            if not is_facing_front(detection, 0.80):                
                x, y, w, h = detection["box"]
                face = image[y:y+h, x:x+w]
                faceBlob = cv2.dnn.blobFromImage(face, 1.0, (227, 227), (78.4263377603, 87.7689143744, 114.895847746), swapRB=False)
                age_model.setInput(faceBlob)
                preds = age_model.forward()
                i = preds[0].argmax()
                age = int(AGE_BUCKETS[i].split('-')[1])
                if age <= CUTOFF_AGE:
                    return True
    
    return False


def interpolate_result_matrix(result_matrix, output_shape):
    channel_count, sample_count = output_shape
    frame_multiplier = int(np.round(sample_count / len(result_matrix), 0))
    tags_as_np = np.array(result_matrix).astype(float)
    tags_interpolated = np.resize(tags_as_np, (frame_multiplier,))

    if tags_interpolated.shape[0] > sample_count:
        trim_width = tags_interpolated.shape[0] - sample_count
        tags_interpolated = tags_interpolated[:trim_width]
    elif tags_interpolated.shape[0] < sample_count:
        pad_width = sample_count - tags_interpolated.shape[0]
        tags_interpolated = np.pad(tags_interpolated, (0, pad_width), constant_values=0)
    
    return np.array([tags_interpolated for _ in range(channel_count)])


def facetag_video(video_path, output_shape=None, frame_rate=29.97, offset_frames=0, frame_cap=None, output_path=None):

    if output_path == None:
        output_path = video_path.replace('.mp4', '_tags.xml')

    cap = cv2.VideoCapture(video_path)
    detector = MTCNN()
    ageNet = init_age_model()

    ms_per_frame = 1000 / frame_rate

    frames_of_interest = []
    tag_bounds = []

    for i in range(offset_frames):
        _, image = cap.read()
        frames_of_interest.append(False)

    area_of_interest = False
    cap_frames_at = frame_cap if frame_cap != None else int(cap.get(cv2.CAP_PROP_FRAME_COUNT)) - offset_frames
    processed_frames = 0

    for frame in tqdm(range(cap_frames_at), position=0, leave=True):
        current_frame = frame + offset_frames
        _, image = cap.read()
        detections = detector.detect_faces(image)
        
        frame_of_interest = check_frame_of_interest(image, detections, ageNet)
        
        processed_frames += 1
        frames_of_interest.append(frame_of_interest)
        if area_of_interest != frame_of_interest:
            tag_bounds.append(current_frame)
            area_of_interest = frame_of_interest

    cap.release()

    if len(tag_bounds) % 2 != 0:
        tag_bounds.append(cap_frames_at)

    tags = [(tag_bounds[idx]*ms_per_frame, tag_bounds[idx+1]*ms_per_frame) for idx in range(0, len(tag_bounds), 2)]

    tags_description = write_tags_to_xml(tags, output_path)

    print(f'Saved tags to file {output_path}')
    print(f'Processed frames: {processed_frames}')

    if output_shape != None:
        return interpolate_result_matrix(frames_of_interest, output_shape), tags_description
    else:
        return frames_of_interest, tags_description


if __name__ == "__main__":

    # video_path = r"C:\Users\Bidon\Desktop\budzik 2\filmy\N400_9add_PW.mp4"
    video_path = sys.argv[1]
    frame_rate = 4.263
    video_tags_occurrence, video_tags_description = facetag_video(video_path, frame_rate=frame_rate)