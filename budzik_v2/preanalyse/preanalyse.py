#Szymon Bocian 2022
import numpy as np

from .channels_and_signal import (resample_data,
                                  pick_signal,
                                  chosen_channels,
                                  baseline,
                                  montage_and_reference,
                                  filtering, EOG_with_ICA,
                                  prepare_tags_and_rest,
                                  find_bad_channels)
from .artifacts import artifacts
from .rest_and_epochs import (dirty_rest_and_epochs_ids,
                              cutting_rest_and_epochs,
                              remove_bad_rest_and_epochs,
                              save_rest_and_epochs_to_epofif,
                              check_epochs_and_rest,
                              prepare_dataframe)

def preanalyse(signal, config, path, events_desc_id, file_name):
    '''
    Preparing all preanalyse for signal.

    Parameters
    ----------
    signal: mne.io.array.array.RawArray
        Signal in mne Raw format.
    config: dict
        Dictionary of configuration variables.
    path: str
        Path to the output directory.
    events_desc_id: dict
        Dictionary of the names of events and their values in the STIM signal.
    file_name: str
        Name of the file with raw signal.

    Returns
    -------
    rest: mne.epochs.Epochs or None
        Prepared rest segments from the signal. If None, REST path wasn't prepare in config file.
    rest_clean: mne.epochs.Epochs or None
        Prepared rest segments from the signal without segments with artifacts. If None, REST path wasn't
        prepare in config file.
    epochs: mne.epochs.Epochs or None
        Prepared epochs from the signal. If None, EPOCHS path wasn't prepare in config file.
    epochs_clean: mne.epochs.Epochs or None
        Prepared epochs from the signal without epochs with artifacts. If None, REST path wasn't
        prepare in config file.
    data: pandas.core.frame.DataFrame
        Dataframe with basic information about tags.
    config: dict
        Dictionary of configuration variables.
    '''

    # preparing tags
    signal, tags_desc_id, config = prepare_tags_and_rest(signal, config, events_desc_id)

    if config["prepare_eeg_artifacts"]:
        # picking signal around events or without beginning
        signal, picked_signal = pick_signal(signal, config) # basically raw signal to find artifacts

        # resampling signal
        signal = resample_data(signal, config)

        # classifying bad channels
        config = find_bad_channels(signal, config, tags_desc_id, path, file_name)
        signal.info["bads"] = config["choosing_channels"]["bad_channels"]
        signal.pick(picks=None, exclude="bads")

        # montage and rereference channels
        signal_mounted = montage_and_reference(signal, config)

        # filtering signal
        signal_mounted_filtered = filtering(signal_mounted, config, path)

        # adding baseline
        signal_mounted_filtered = baseline(signal_mounted_filtered, config)

        # removing ocular artifacts with ICA and EOG/frontal electrodes
        ica_EOG = EOG_with_ICA(signal_mounted_filtered, config, path, file_name)
        signal_mounted_filtered_EOG = ica_EOG.apply(signal_mounted_filtered, exclude=ica_EOG.exclude) # signal to cut
        signal_mounted_EOG = ica_EOG.apply(signal_mounted, exclude=ica_EOG.exclude) # signal to find artifacts

        # taking and dropping channels chosen by user
        signal_mounted_filtered_EOG, signal_mounted_EOG = chosen_channels([signal_mounted_filtered_EOG, signal_mounted_EOG], tags_desc_id, config)


    else:
        picked_signal = np.ones(signal[0][0][0].size)  # signal to pick - if config["prepare_eeg_artifacts"] is False, then we will pick all signal
        signal_mounted_EOG = signal.copy()  # mounted signal (resampled, montage, referenced) and after deleting EOG compontents from ICA - if config["prepare_eeg_artifacts"] is False, then we will pick basic signal
        signal_mounted_filtered_EOG = signal.copy()  # mounted signal (resampled, montage, referenced, baseline), filtered and after deleting EOG compontents from ICA - if config["prepare_eeg_artifacts"] is False, then we will pick basic signal


    if config["prepare_eeg_artifacts"] or config["prepare_video_artifacts"]:
        # finding artifacts (muscles, slopes, outliers, not_looking)
        artifacts_occurrence = artifacts(signal_mounted_EOG, picked_signal, config, path, file_name)
        dirty_rest_idx, dirty_epochs_idx = dirty_rest_and_epochs_ids(signal_mounted_filtered_EOG, config, artifacts_occurrence, tags_desc_id)

    else:
        artifacts_occurrence = None
        dirty_rest_idx = None
        dirty_epochs_idx = None


    # cutting rest and epochs
    rest, rest_events, epochs, epochs_events = cutting_rest_and_epochs(signal_mounted_filtered_EOG, config, tags_desc_id)

    # deleting rest and epochs with artifacts or with crossed Peak-to-Peak thresholds
    rest_clean, dirty_rest_idx, epochs_clean, dirty_epochs_idx = remove_bad_rest_and_epochs(rest, dirty_rest_idx, epochs, dirty_epochs_idx, config)

    # showing rest and epochs and deleting bad segments by hand
    rest_clean, dirty_rest_idx, epochs_clean, dirty_epochs_idx = check_epochs_and_rest(rest_clean, epochs_clean, config)

    # saving rest and epochs to -epo.fif
    save_rest_and_epochs_to_epofif(rest, epochs, tags_desc_id, config, path, file_name) # all epochs and rest
    save_rest_and_epochs_to_epofif(rest_clean, epochs_clean, tags_desc_id, config, path, file_name + "_clean") # only clean epochs and rest

    # preparing dataframe with basic stats
    data = prepare_dataframe(rest_events, dirty_rest_idx, epochs_events, dirty_epochs_idx, tags_desc_id, artifacts_occurrence, signal_mounted_filtered_EOG.info, config)

    return rest, rest_clean, epochs, epochs_clean, tags_desc_id, data, config
