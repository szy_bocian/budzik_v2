# Szymon Bocian 2022

import os

import mne
import numpy as np
import pandas as pd


def dirty_rest_and_epochs_ids(signal, config, artifacts_occurrence, events_desc_id):
    '''
    The program takes occurrences of artifacts from the artifacts_occurrence and multiplies them by the weight defined
    in the config file. In the next step, the program adds all the artifacts and checks how many points of the epochs
    and rest are dirty by artifacts. Finally, it checks which epochs and rest have more dirty points than the threshold
    defined in the config file. If the epoch and rest has more dirty points than the threshold at least on one channel,
    this epoch or rest segment will be defined as bad and will remove from epochs for analysis.

    Parameters
    ----------
    signal: mne.io.array.array.RawArray
        Signal in mne Raw format.
    config: dict
        Dictionary of configuration variables.
    artifacts_occurrence: dict
        Keys are names of artifacts, values are matrices of occurred artifacts in the specific channel and time.
        Each matrix has the shape of [number of channels, duration of signal] with two variables {0, 1}. 1 means that
        the artifact occurred in this specific point of time and space. Dictionary was prepared by the "artifacts"
        function from preanalyse/artifatcs.py.
    events_desc_id: dict
        Dictionary of the names of events and their values in the STIM signal.

    Returns
    -------
    bad_rest: list or None
        List of indexes of bad rest segments (percent of dirty points on at least one channel are higher than
        the threshold for artifacts). If None, REST path wasn't prepare in config file.
    bad_epochs: list or None
        List of indexes of bad epochs (percent of dirty points on at least one channel are higher than
        the threshold for artifacts). If None, EPOCHS path wasn't prepare in config file.
    '''

    numpy_signal_channels = signal.get_data(picks=mne.pick_types(signal.info, eeg=True, exclude="bads")) # eeg signals

    # adding artifacts with their weights
    art_thrd = config["artifacts"]["artifact_threshold"]  # threshold for bad rest and epochs
    artifact_detect = np.zeros(np.shape(numpy_signal_channels)) # points in time and space represented by weighted artifacts

    for art_name in artifacts_occurrence:
        artifact_detect = np.logical_or(artifact_detect, artifacts_occurrence[art_name])

    # checking rest segments
    if "rest" in config.keys() and config["prepare_rest_or_epochs"] == "rest":
        bad_rest = [] # list of bad rest segments indexes

        start_n = 0  # start point of rest segment in points
        stop_n = int(signal.info["sfreq"] * config["rest"]["window_length"])  # stop point of epoch in points
        rest_duration_n = stop_n - start_n  # duration of epoch in points

        stim_rest = signal["STIM"][0]  # occurrence of stimulation
        if "epochs" in config.keys() and config["prepare_rest_or_epochs"] == "epochs":
            stim_rest[stim_rest != events_desc_id["rest"]] = 0

        epoch_idx = 0

        for t, epoch in enumerate(np.transpose(stim_rest)):
            if epoch[0] != 0:

                t_start = t + start_n
                t_stop = t + stop_n + 1

                arts_in_epoch = np.sum(artifact_detect[:, t_start:t_stop], axis=1) / rest_duration_n

                if np.sum(arts_in_epoch > art_thrd):
                    bad_rest.append(epoch_idx)

                epoch_idx += 1
    else:
        bad_rest = None

    # checking epochs
    if "epochs" in config.keys() and config["prepare_rest_or_epochs"] == "epochs":
        bad_epochs = []  # list of bad epoch indexes

        start_n = int(signal.info["sfreq"] * config["epochs"]["start_offset"])  # start point of epoch in points
        stop_n = int(signal.info["sfreq"] * config["epochs"]["stop_offset"])  # stop point of epoch in points
        epoch_duration_n = stop_n - start_n  # duration of epoch in points

        stim_epochs = signal["STIM"][0]  # occurrence of stimulation
        if "rest" in config.keys() and config["prepare_rest_or_epochs"] == "rest":
            stim_epochs[stim_epochs == events_desc_id["rest"]] = 0

        epoch_idx = 0

        for t, epoch in enumerate(np.transpose(stim_epochs)):
            if epoch[0] != 0:

                t_start = t + start_n
                t_stop = t + stop_n + 1

                arts_in_epoch = np.sum(artifact_detect[:, t_start:t_stop], axis=1) / epoch_duration_n

                if np.sum(arts_in_epoch > art_thrd):
                    bad_epochs.append(epoch_idx)

                epoch_idx += 1
    else:
        bad_epochs = None

    return bad_rest, bad_epochs


def cutting_rest_and_epochs(signal, config, events_desc_id):
    '''
    Cutting epochs from the signal with STIM channel.

    Parameters
    ----------
    signal: mne.io.array.array.RawArray
        Signal in mne Raw format.
    config: dict
        Dictionary of configuration variables.
    events_desc_id: dict
        Dictionary of the names of events and their values in the STIM signal.

    Returns
    -------
    rest: mne.epochs.Epochs or None
        Prepared segments of signal in the rest state. If None, REST path wasn't prepare in config file.
    rest_events: numpy.ndarray or None
        Each row is the info about one segment - first number is the position fo the segment (0 s point), second number
        is length of the segment (0 s for us) and last number is the id of the tag (id of rest segments - this the
        greatest id from other tags + 1). If None, REST path wasn't prepare in config file.
    epochs: mne.epochs.Epochs or None
        Prepared epochs from the signal. If None, EPOCHS path wasn't prepare in config file.
    epochs_events: numpy.ndarray or None
        Each row is the info about one epoch - first number is the position fo the segment (0 s point), second number
        is length of the segment (0 s for us) and last number is the id of the tag (id of tags - look events_desc_id).
        If None, EPOCHS path wasn't prepare in config file.
    '''

    # prepared segments from rested signal
    if "rest" in config.keys() and config["prepare_rest_or_epochs"] == "rest":

        rest_stim = mne.channel_indices_by_type(signal.info)["stim"][0]

        # preparing STIM channel if it has epochs in it
        if "epochs" in config.keys() and config["prepare_rest_or_epochs"] == "epochs":
            signal_rest = signal.copy().get_data()

            signal_rest[rest_stim, signal_rest[rest_stim] != events_desc_id["rest"]] = 0
            signal_rest = mne.io.RawArray(signal_rest, signal.info)
        else:
            signal_rest = signal.copy()

        # rest id in STIM
        rest_desc_id = {"rest": events_desc_id["rest"]}

        # creating rest segments
        rest_events = mne.find_events(signal_rest, stim_channel=signal.info["ch_names"][rest_stim], initial_event=True)
        rest = mne.Epochs(signal_rest, rest_events, tmin=0, tmax=config["rest"]["window_length"], event_id=rest_desc_id,
                          baseline=None, reject=None)

        # dropping STIM channel - we dont need it
        rest = rest.load_data().drop_channels("STIM")

    else:
        rest_events = None
        rest = None

    # prepared epochs from evoked signals
    if "epochs" in config.keys() and config["prepare_rest_or_epochs"] == "epochs":

        epochs_stim = mne.channel_indices_by_type(signal.info)["stim"][0]

        # preparing STIM channel if it has rest in it
        if "rest" in config.keys() and config["prepare_rest_or_epochs"] == "rest":
            signal_epochs = signal.copy().get_data()

            signal_epochs[epochs_stim, signal_epochs[epochs_stim] == events_desc_id["rest"]] = 0
            signal_epochs = mne.io.RawArray(signal_epochs, signal.info)
        else:
            signal_epochs = signal.copy()

        # epochs id in STIM
        epochs_desc_id = {tag: events_desc_id[tag] for tag in events_desc_id if tag != "rest"}

        # metaparameters for cutting
        if isinstance(config["epochs"]["epochs_baseline"], list):
            config["epochs"]["epochs_baseline"] = tuple([None if i in ["None", "none", ""] else i for i in config["epochs"]["epochs_baseline"]])

        # creating epochs
        epochs_events = mne.find_events(signal_epochs, stim_channel=signal.info["ch_names"][epochs_stim])
        epochs = mne.Epochs(signal_epochs, epochs_events, tmin=config["epochs"]["start_offset"], tmax=config["epochs"]["stop_offset"],
                            event_id=epochs_desc_id, baseline=config["epochs"]["epochs_baseline"])

        # dropping STIM channel - we dont need it
        epochs = epochs.load_data().drop_channels("STIM")

    else:
        epochs_events = None
        epochs = None

    return rest, rest_events, epochs, epochs_events


def remove_bad_rest_and_epochs(rest, bad_rest, epochs, bad_epochs, config):
    '''
    Removing rest and epochs with artifacts.

    Parameters
    ----------
    rest: mne.epochs.Epochs or None
        Prepared epochs from the signal. If None, REST path wasn't prepare in config file.
    bad_rest: list or None
        List of indexes of bad epochs. If None, REST path wasn't prepare in config file.
    epochs: mne.epochs.Epochs or None
        Prepared epochs from the signal. If None, EPOCHS path wasn't prepare in config file.
    bad_epochs: list or None
        List of indexes of bad epochs. If None, EPOCHS path wasn't prepare in config file.
    config: dict
        Dictionary of configuration variables.

    Returns
    -------
    clean_rest: mne.epochs.Epochs or None
        Set of rest segments without rest segments with artifacts (see prenalyse\artifacts). If None, REST path wasn't
        prepare in config file.
    bad_rest: list or None
        List of indexes of bad epochs. If None, REST path wasn't prepare in config file.
    clean_epochs: mne.epochs.Epochs or None
        Set of epochs without epochs with artifacts (see prenalyse\artifacts). If None, EPOCHS path wasn't prepare in
        config file.
    bad_epochs: list or None
        List of indexes of bad epochs. If None, EPOCHS path wasn't prepare in config file.
    '''

    # build-in parameters for bad epochs and rest
    if config["epochs"]["reject_dict"] in ["None", None, "", {}, []]:
        reject_dict = None
    elif isinstance(config["epochs"]["reject_dict"], dict):
        reject_dict = config["epochs"]["reject_dict"]

    if config["epochs"]["flat_dict"] in ["None", None, "", {}, []]:
        flat_dict = None
    elif isinstance(config["epochs"]["flat_dict"], dict):
        flat_dict = config["epochs"]["flat_dict"]

    if rest != None and bad_rest != None:
        print("MESSAGE: Dropping bad rest segments from artifacts and Peak-to-Peak thresholds.")
        clean_rest = rest.copy().drop(bad_rest)
        clean_rest.drop_bad(reject=reject_dict, flat=flat_dict)
        bad_rest = [i for i in range(len(rest.events)) if clean_rest.drop_log[i] != ()]

    elif rest != None and bad_rest == None:
        print("MESSAGE: Dropping bad rest from Peak-to-Peak thresholds.")
        clean_rest = rest.copy()
        clean_rest.drop_bad(reject=reject_dict, flat=flat_dict)
        bad_rest = [i for i in range(len(rest.events)) if clean_rest.drop_log[i] != ()]

    else:
        clean_rest = None

    if epochs != None and bad_epochs != None:
        print("MESSAGE: Dropping bad epochs from artifacts and Peak-to-Peak thresholds.")
        clean_epochs = epochs.copy().drop(bad_epochs)
        clean_epochs.drop_bad(reject=reject_dict, flat=flat_dict)
        bad_epochs = [i for i in range(len(epochs.events)) if clean_epochs.drop_log[i] != ()]

    elif epochs != None and bad_epochs == None:
        print("MESSAGE: Dropping bad epochs from Peak-to-Peak thresholds.")
        clean_epochs = epochs.copy()
        clean_epochs.drop_bad(reject=reject_dict, flat=flat_dict)
        bad_epochs = [i for i in range(len(epochs.events)) if clean_epochs.drop_log[i] != ()]

    else:
        clean_epochs = None

    return clean_rest, bad_rest, clean_epochs, bad_epochs


def check_epochs_and_rest(rest, epochs, config):
    '''
    Checking rest and epochs to deleting bad segments by hand.

    Parameters
    ----------
    rest: mne.epochs.Epochs or None
        Prepared epochs from the signal. If None, REST path wasn't prepare in config file.
    epochs: mne.epochs.Epochs or None
        Prepared epochs from the signal. If None, EPOCHS path wasn't prepare in config file.
    config: dict
        Dictionary of configuration variables.

    Returns
    -------
    clean_rest: mne.epochs.Epochs or None
        Set of rest segments without rest segments dropped by hand. If None, REST path wasn't prepare in config file.
    bad_rest: list or None
        List of indexes of bad epochs. If None, REST path wasn't prepare in config file.
    clean_epochs: mne.epochs.Epochs or None
        Set of epochs without epochs dropped by hand. If None, EPOCHS path wasn't prepare in config file.
    bad_epochs: list or None
        List of indexes of bad epochs. If None, EPOCHS path wasn't prepare in config file.
    '''

    # REST path
    if rest != None and config["rest"]["check_rest"]:

        clean_rest = rest.copy()
        scale = np.round(np.quantile(rest.load_data().pick_types(eeg=True, exclude=epochs.info["bads"]).get_data(), 0.995), 5)

        clean_rest.plot(block=True, event_id=rest.event_id, events=rest.events, n_epochs=10, scalings=dict(eeg=scale))

        bad_rest = [i for i in range(len(clean_rest.events)) if clean_rest.drop_log[i] != ()]

    elif rest == None:

        clean_rest = None
        bad_rest = None

    else:

        clean_rest = rest.copy()
        bad_rest = [i for i in range(len(clean_rest.events)) if clean_rest.drop_log[i] != ()]


    # EPOCHS path
    if epochs != None and config["epochs"]["check_epochs"]:

        clean_epochs = epochs.copy()
        scale = np.round(np.quantile(epochs.load_data().pick_types(eeg=True, exclude=epochs.info["bads"]).get_data(), 0.995), 5)

        clean_epochs.plot(block=True, event_id=epochs.event_id, events=epochs.events, n_epochs=10, scalings=dict(eeg=scale))

        bad_epochs = [i for i in range(len(clean_epochs.events)) if clean_epochs.drop_log[i] != ()]

    elif epochs == None:

        clean_epochs = None
        bad_epochs = None

    else:

        clean_epochs = epochs.copy()
        bad_epochs = [i for i in range(len(clean_epochs.events)) if clean_epochs.drop_log[i] != ()]

    return clean_rest, bad_rest, clean_epochs, bad_epochs


def save_rest_and_epochs_to_epofif(rest, epochs, tags_desc_id, config, path, file_name):
    '''
    Save epochs to the -epo.fif file in the 'clean_epochs' catalog.

    Parameters
    ----------
    rest: mne.epochs.Epochs or None
        Prepared epochs from the signal. If None, REST path wasn't prepare in config file.
    epochs: mne.epochs.Epochs or None
        Prepared epochs from the signal. If None, EPOCHS path wasn't prepare in config file.
    tags_desc_id: dict
        Dictionary of the names of prepared tags and their values in the prepared STIM signal.
    config: dict
        Dictionary of configuration variables.
    path: str
        Path to directory with signal.
    file_name: str
        Name of the file with raw signal.
    '''

    fifpath = os.path.join(path, "saved_signals")
    if not os.path.isdir(fifpath):
        os.mkdir(fifpath)

    if rest != None:
        rest = rest.copy()
        rest.reject = None
        rest.flat = None
        rest.save(os.path.join(fifpath, file_name + "_rest-epo.fif"), overwrite=True)

    if epochs != None:
        epochs = epochs.copy()
        epochs.reject = None
        epochs.flat = None
        epochs.save(os.path.join(fifpath, file_name + "_epochs-epo.fif"), overwrite=True)

    if not os.path.exists(os.path.join(fifpath, "tag_id.txt")):
        with open(os.path.join(fifpath, "tag_id.txt"), "w") as file:
            file.write("~~~~~~TAGS INDEX~~~~~~\n\n")
            for tag in tags_desc_id.keys():
                file.write(f"{tag}: {tags_desc_id[tag]}\n")
                if tag != "rest":
                    for event in config["epochs"]["tags"][tag]:
                        file.write(f"    {event}\n")
                else:
                    file.write(f"    Resting state.\n")
        file.close()


def prepare_dataframe(rest_events, bad_rest, epochs_events, bad_epochs, tags_desc_id, artifacts_occurrence, signal_info, config):
    '''
    Preparing data frame with basic information (like tag position in signal, how many points classified as artifact
    in tag etc.) about tags.

    Parameters
    ----------
    rest_events: numpy.ndarray or None
        Each row is the info about one segment - first number is the position fo the segment (0 s point), second number
        is length of the segment (0 s for us) and last number is the id of the tag (id of rest segments - this the
        greatest id from other tags + 1). If None, REST path wasn't prepare in config file.
    bad_rest: list or None
        List of indexes of bad epochs. If None, REST path wasn't prepare in config file.
    epochs_events: numpy.ndarray or None
        Each row is the info about one epoch - first number is the position fo the segment (0 s point), second number
        is length of the segment (0 s for us) and last number is the id of the tag (id of tags - look events_desc_id).
        If None, EPOCHS path wasn't prepare in config file.
    bad_epochs: list or None
        List of indexes of bad epochs. If None, EPOCHS path wasn't prepare in config file.
    tags_desc_id: dict
        Dictionary of the names of prepared tags and their values in the prepared STIM signal.
    artifacts_occurrence: dict
        Keys are names of artifacts, values are matrices of occurred artifacts in the specific channel and time.
        Each matrix has the shape of [number of channels, duration of signal] with two variables {0, 1}. 1 means that
        the artifact occurred in this specific point of time and space. Dictionary was prepared by the "artifacts"
        function from preanalyse/artifatcs.py.
    signal_info:  mne.io.meas_info.Info
        Signal information created by mne - more: https://mne.tools/1.3/generated/mne.Info.html
    config: dict
        Dictionary of configuration variables.

    Returns
    -------
    data: pandas.core.frame.DataFrame
        Dataframe with basic information about tags.
    '''

    data = {"id": [], "tag": [], "channel": [], "t_start": [], "t_stop": [], "bad_segment": [], "bad_channel": []}
    Fs = signal_info["sfreq"]
    signal_info["ch_names"].remove("STIM")
    ch_not_bad = [ch for ch in signal_info["ch_names"] if ch not in signal_info["bads"]]

    if artifacts_occurrence != None:
        for art_name in artifacts_occurrence.keys():
            data[art_name + "_arts"] = []

    if rest_events is not None:

        rest_length = config["rest"]["window_length"]

        for s, segment in enumerate(rest_events):

            for ch_name in signal_info["ch_names"]:

                data["id"].append(f"rest_{ch_name}_{s}")

                data["tag"].append("rest")
                data["channel"].append(ch_name)

                data["t_start"].append(segment[0]/Fs)
                data["t_stop"].append(segment[0]/Fs + rest_length)

                if bad_rest is not None:
                    if s in bad_rest:
                        data["bad_segment"].append(True)
                    else:
                        data["bad_segment"].append(False)
                else:
                    data["bad_segment"].append(False)

                if ch_name in signal_info["bads"]:
                    data["bad_channel"].append(True)
                else:
                    data["bad_channel"].append(False)

                if artifacts_occurrence != None:
                    for art_name in artifacts_occurrence.keys():
                        if ch_name not in signal_info["bads"]:
                            try:
                                ch = ch_not_bad.index(ch_name)
                                data[art_name + "_arts"].append(np.sum(artifacts_occurrence[art_name][ch, segment[0]:int(segment[0]+rest_length*Fs)])/int(Fs*rest_length))
                            except:
                                data[art_name + "_arts"].append(np.nan)
                        else:
                            data[art_name + "_arts"].append(np.nan)


    if epochs_events is not None:

        start_offset = config["epochs"]["start_offset"]
        stop_offset = config["epochs"]["stop_offset"]

        for s, segment in enumerate(epochs_events):

            for ch, ch_name in enumerate(signal_info["ch_names"]):

                data["id"].append(f"{list(tags_desc_id.keys())[segment[2]-1]}_{ch_name}_{s}")

                data["tag"].append(list(tags_desc_id.keys())[segment[2]-1])
                data["channel"].append(ch_name)

                data["t_start"].append(segment[0]/Fs + start_offset)
                data["t_stop"].append(segment[0]/Fs + stop_offset)

                if bad_epochs is not None:
                    if s in bad_epochs:
                        data["bad_segment"].append(True)
                    else:
                        data["bad_segment"].append(False)
                else:
                    data["bad_segment"].append(False)

                if ch_name in signal_info["bads"]:
                    data["bad_channel"].append(True)
                else:
                    data["bad_channel"].append(False)

                if artifacts_occurrence != None:
                    for art_name in artifacts_occurrence.keys():
                        if ch_name not in signal_info["bads"]:
                            try:
                                ch = ch_not_bad.index(ch_name)
                                data[art_name + "_arts"].append(np.sum(artifacts_occurrence[art_name][ch, int(segment[0]+start_offset*Fs):int(segment[0]+stop_offset*Fs)+1])/int(Fs*(stop_offset - start_offset)))
                            except:
                                data[art_name + "_arts"].append(np.nan)
                        else:
                            data[art_name + "_arts"].append(np.nan)

    return pd.DataFrame(data)
