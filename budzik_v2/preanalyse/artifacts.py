'''
All codes copied from https://gitlab.com/BudzikFUW/budzik-analiza-py-3.git (helper_functions\artifacts_mark.py,
helper_functions\artifact_reject and helper_functions\helper_functions.py) and prepared for this project.

The main flow of detecting bad epochs based on Klekowicz et al. DOI 10.1007/s12021-009-9045-2.
'''
import sys

import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as ss
import mne
import os
import json
from obci_readmanager.signal_processing.tags.tags_file_writer import TagsFileWriter

from .channels_and_signal import Power
from .facetag import facetag_video
from ..plots.plots import prepare_topo_map

artifact_labels = ["arbitrary", "median", "std"]


# Marcin Pietrzak 2017-03-01 based on Klekowicz et al. DOI 10.1007/s12021-009-9045-2 from helper_functions\artifacts_mark.py and helper_functions\artifact_reject

def artifacts(signal, picked_signal, config, path, file_name):
    '''
    Finding occurances of defined artifacts in the time and space (channels).

    Parameters
    ----------
    signal: mne.io.array.array.RawArray
        Signal in mne Raw format.
    picked_signal: numpy.ndarray
        Array which show which parts of the signal were cutted araound events.
    config: dict
        Dictionary of configuration variables.
    path: str
        Path to directory with signal.
    file_name: str
        Name of the file with raw signal.

    Returns
    -------
    artifacts_occurrence: dict
        Keys are names of artifacts, values are matrices of occurred artifacts in the specific channel and time.
        Each matrix has the shape of [number of channels, duration of signal] with two variables {0, 1}. 1 means that
        the artifact occurred in this specific point of time and space.
    '''

    art_path = os.path.join(path, "artifacts_detection")
    if not os.path.isdir(art_path):
        os.mkdir(art_path)
    art_path = os.path.join(art_path, "found_artifacts")
    if not os.path.isdir(art_path):
        os.mkdir(art_path)

    numpy_signal_channels = signal.get_data()[mne.pick_types(signal.info, eeg=True, exclude="bads"), :] #eeg signal without bads
    signal_info = signal.info #signal info

    artifacts_occurrence = {} # artifact occurrence in binary (1 is artifact occurrence, 0 otherwise) as {"art_name": [n_channels, sig_length], ...}
    artifacts_description = [] # list of dictionaries which describes artifact position in the space+time, length and small description

    # artifact searching
    if config["prepare_eeg_artifacts"]:
        art_det_kargs = config["artifacts"]["rejection_parameters"] #dict with thresholds for artifacts

        all_channels = config["choosing_channels"]["all_channels_names"] #channels names

        art_desc, art_occ = RM_ArtifactDetection(numpy_signal_channels, signal_info, all_channels, art_det_kargs, art_path)

        artifacts_description = artifacts_description + art_desc
        artifacts_occurrence = {**artifacts_occurrence, **art_occ}

    #todo - jak na razie to jest zrobione na odwal sie, bo tylko dla sytuacji "usuwamy wszystko do pierwszego taga" zadziała
    art_real_time = np.sum(picked_signal!=1)/config["sfreq"] #we want to save tags with real time from the raw signal
    for art in artifacts_description:
        art["start_timestamp"] += art_real_time
        art["end_timestamp"] += art_real_time

    # video (looking/not_looking)
    if config["prepare_video_artifacts"]:
        video_filename = file_name.split(".")[0] + ".mp4"
        path_to_video = os.path.join(path, video_filename)
        if os.path.exists(path_to_video):
            facetag_filename = file_name.split(".")[0] + "_facetags.xml"
            facetag_output_path = os.path.join(art_path, facetag_filename)
            video_tags_occurrence, video_tags_description = facetag_video(path_to_video, output_shape=[mne.pick_types(signal_info, eeg=True, exclude="bads").shape[0], picked_signal.shape[0]], output_path=facetag_output_path)

            artifacts_description = artifacts_description + video_tags_description
            artifacts_occurrence["not_looking"] = video_tags_occurrence[:, picked_signal == 1]
        else:
            print(f"WARNING: Video for {os.path.join(path, file_name)} doesn't exist - please check the name of the video (should be same as signal file), place of video (should be in the same folder as signal) or the video format (should be mp4). Searching for video artifacts will be not prepared.")

    # saving artifacts to the .tag file
    ArtTagsSaver(art_path, artifacts_description, config["artifacts"]["rejection_parameters"], file_name)

    return artifacts_occurrence

def RM_ArtifactDetection(numpy_signal_channels, signal_info, all_channels, ArtDet_kargs={}, outpath=""):
    '''
    Marcin Pietrzak 2017 from helper_functions\artifacts_mark.py.

    Finding artifacts.

    Parameters
    ----------
    numpy_signal_channels: numpy.array
        EEG signal in numpy.array.
    signal_info: mne.Info
        Info of signal.
    all_channels: list
        List of channels.
    ArtDet_kargs: dict
        Dictionary of essentials values of artifact detection.
    outpath: str
        Path for plots.

    Returns
    -------
    artifacts_description: list
        Full description of detected artifacts prepared for .tag format.
    artifacts_occurrence: dict
        Keys are names of artifacts, values are matrices of occurred artifacts in the specific channel and time.
        Each matrix has the shape of [number of channels, duration of signal] with two variables {0, 1}. 1 means that
        the artifact occurred in this specific point of time and space.
'''

    # tested channels
    ch_2be_tested = list(np.array(signal_info["ch_names"])[mne.pick_types(signal_info, eeg=True)]) # because we are after dropping bad channels and we want pnlu eeg

    # filters for non-muscle artifacts
    filters = [[1., 0.5, 3, 6.97, "butter"],
               [30, 60, 3, 12.4, "butter"],
               [[47.5, 52.5], [49.9, 50.1], 3, 25, "cheby2"]]

    # filters for muscle artifacts
    line_fs = [50.]
    filters_muscle = []
    for line_f in line_fs:
        filters_muscle.append([[line_f - 2.5, line_f + 2.5], [line_f - 0.1, line_f + 0.1], 3.0, 25., "cheby2"])

    forget = ArtDet_kargs.get('forget', 2.)

    try:
        #widnows
        SlopeWindow = ArtDet_kargs['SlopeWindow']
        MusclesWindow = ArtDet_kargs['MusclesWindow']
        OutliersMergeWin = ArtDet_kargs['OutliersMergeWin']
        MusclesFreqRange = ArtDet_kargs['MusclesFreqRange']

        # Slope threshold
        SlopesAbsThr = ArtDet_kargs['SlopesAbsThr']
        SlopesStdThr = ArtDet_kargs['SlopesStdThr']
        SlopesMedThr = ArtDet_kargs['SlopesMedThr']
        SlopesThrs = (SlopesAbsThr, SlopesStdThr, SlopesMedThr)

        # Outlires threshold
        OutliersAbsThr = ArtDet_kargs['OutliersAbsThr']
        OutliersStdThr = ArtDet_kargs['OutliersStdThr']
        OutliersMedThr = ArtDet_kargs['OutliersMedThr']
        OutliersThrs = (OutliersAbsThr, OutliersStdThr, OutliersMedThr)

        # Muscle threshold
        MusclesAbsThr = ArtDet_kargs['MusclesAbsThr']
        MusclesStdThr = ArtDet_kargs['MusclesStdThr']
        MusclesMedThr = ArtDet_kargs['MusclesMedThr']
        MusclesThrs = (MusclesAbsThr, MusclesStdThr, MusclesMedThr)
    except:
        sys.exit("ERROR: Provide proper dictionary in segment artifacts.rejection_parameters of "
                 "config file. It should contain: SlopeWindow, MusclesWindow, OutliersMergeWin, MusclesFreqRange_start, "
                 "MusclesFreqRange_stop, SlopesAbsThr, SlopesStdThr, SlopesMedThr, SlowsAbsThr, SlowsStdThr, "
                 "SlowsMedThr, OutliersAbsThr, OutliersStdThr, OutliersMedThr, MusclesAbsThr, MusclesStdThr, "
                 "MusclesMedThr.")

    return detect_artifacts(numpy_signal_channels, signal_info, ch_2be_tested, all_channels, filters, filters_muscle,
                            forget, OutliersMergeWin, SlopeWindow, MusclesWindow, MusclesFreqRange, SlopesThrs,
                            OutliersThrs, MusclesThrs, outpath)


def detect_artifacts(numpy_signal_channels, signal_info, channels, all_channels, filters, filters_muscle, forget,
                     OutliersMergeWin, SlopeWindow, MusclesWindow, MusclesFreqRange, SlopesThrs, OutliersThrs,
                     MusclesThrs, outpath=""):
    '''
    Marcin Pietrzak 2017 from helper_functions\artifacts_mark.py.

    Finding artifacts.

    Parameters
    ----------
    numpy_signal_channels: numpy.array
        EEG signal in numpy.array.
    signal_info: mne.Info
        Info of signal.
    channels: list
        List of tested channels (not "bad" channels).
    all_channels: list
        List of all channels.
    filters: list
        List of lists contain essential values to create filters for all artifacts.
    filters_muscle: list
        List of lists contain essential values to create filters for muscle artifacts.
    forget: float
        Duration (in seconds) of fragments in the beginning and ending of signal for which we aren't save the tags.
    OutliersMergeWin: float
        Length of window (in seconds) in which we connect nearest areas in outliers artifact.
    SlopeWindow: float
        Length of window (in seconds) in which we are searching slopes.
    MusclesWindow: float
        Length of window (in seconds) in which we are searching slows.
    MusclesFreqRange: list
        Two-element list with start and end frequency in which band we are searching muscle artifacts.
    SlopesThrs: list
        Three-element list with thresholds (absolute, standard deviation and median) for slopes artifacts.
    SlowsThrs:
        Three-element list with thresholds (absolute, standard deviation and median) for slows artifacts.
    OutliersThrs:
        Three-element list with thresholds (absolute, standard deviation and median) for outliers artifacts.
    MusclesThrs:
        Three-element list with thresholds (absolute, standard deviation and median) for muscles artifacts.
    outpath: str
        Path for plots.

    Returns
    -------
    artifacts_description: list
        Full description of detected artifacts prepared for .tag format (list of dicts).
    artifacts_occurrence: dict
        Keys are names of artifacts, values are matrices of occurred artifacts in the specific channel and time.
        Each matrix has the shape of [number of channels, duration of signal] with two variables {0, 1}. 1 means that
        the artifact occurred in this specific point of time and space.
'''

    SlopesAbsThr, SlopesStdThr, SlopesMedThr = SlopesThrs
    OutliersAbsThr, OutliersStdThr, OutliersMedThr = OutliersThrs
    MusclesAbsThr, MusclesStdThr, MusclesMedThr = MusclesThrs

    Fs = signal_info["sfreq"]

    artifacts_description = []
    artifacts_occurrence = {}

    #outliers
    bad_samples, thresholds = outliers(numpy_signal_channels, signal_info, channels, filters, StdThr=OutliersStdThr, AbsThr=OutliersAbsThr,
                                       MedThr=OutliersMedThr,
                                       outpath=outpath)  # zaznaczanie outliers
    bad_samples = ForgetBirthAndDeath(bad_samples, Fs, forget)  # zapominamy wadliwe próbki na początku i końcu sygnału
    bad_samples = ArtTagsWriter(bad_samples, Fs, all_channels, MergWin=OutliersMergeWin, ArtName="outlier",
                              Thresholds=thresholds)
    artifacts_description.extend(bad_samples[0])
    artifacts_occurrence["outliers"] = bad_samples[1]

    #slopes
    bad_samples, thresholds = P2P(numpy_signal_channels, signal_info, channels, SlopeWindow, filters, StdThr=SlopesStdThr, AbsThr=SlopesAbsThr,
                                  MedThr=SlopesMedThr,
                                  kind='slopes', outpath=outpath)  # zaznaczanie iglic i stromych zboczy
    bad_samples = ForgetBirthAndDeath(bad_samples, Fs, forget)  # zapominamy wadliwe próbki na początku i końcu sygnału
    bad_samples = ArtTagsWriter(bad_samples, Fs, all_channels, MergWin=SlopeWindow, ArtName="slope", Thresholds=thresholds)
    artifacts_description.extend(bad_samples[0])
    artifacts_occurrence["slopes"] = bad_samples[1]

    #muscles
    bad_samples, thresholds = muscles(numpy_signal_channels, signal_info, channels, MusclesWindow, MusclesFreqRange, filters_muscle,
                                      StdThr=MusclesStdThr, AbsThr=MusclesAbsThr,
                                      MedThr=MusclesMedThr, outpath=outpath)  # zaznaczanie mięśni
    bad_samples = ForgetBirthAndDeath(bad_samples, Fs, forget)  # zapominamy wadliwe próbki na początku i końcu sygnału
    bad_samples = ArtTagsWriter(bad_samples, Fs, all_channels, MergWin=MusclesWindow, ArtName="muscle", Thresholds=thresholds)
    artifacts_description.extend(bad_samples[0])
    artifacts_occurrence["muscles"] = bad_samples[1]

    return artifacts_description, artifacts_occurrence

def outliers(numpy_signal_channels, signal_info, channels, filters=[], StdThr=np.inf, AbsThr=np.inf, MedThr=np.inf,
             full_output=False, outpath=""):
    '''
    Marcin Pietrzak 2017 from helper_functions\artifacts_mark.py.

    Finding outliers.

    Parameters
    ----------
    numpy_signal_channels: numpy.array
        EEG signal in numpy.array.
    signal_info: mne.Info
        Info of signal.
    channels: list
        List of channels.
    filters: list
        List of lists contain essential values to create filters for all artifacts.
    StdThr: float
        Multiply of std threshold.
    AbsThr: float
        Absolute threshold.
    MedThr: float
        Multiply of median threshold.
    full_output: bool
        If True, returns bad_samples and AllThresholdsDict, else bad_samples and ThresholdsDict.
    outpath: str
        Path to directory with signal.

    Returns
    -------
    bad_samples: dict
        Samples over the selected threshold for each channel.
    ThresholdsDict: dict
        Type and value of select threshold for each channel.
    AllThresholdsDict: dict
        Values of all prepared thresholds for each channel.
    '''
    # w gruncie rzeczy to jeszcze można zrobić test, czy rozkład jest gaussowski,
    # bo czasami nie jest i wtedy można sugerować kanał, jako wadliwy
    # może to m.in. sugerować, że właściwości statystyczne kanału nie są stałe w czasie, np. odkleił się
    # if outpath:
    #     fig, axs, axs_chs = prepare_topo_map(signal_info)

    for filter in filters:
        print("Filtering...", filter)
        numpy_signal_channels = mgr_filter(numpy_signal_channels, signal_info, filter[0], filter[1], filter[2],
                                           filter[3], filter[4])

    bad_samples = dict()
    ThresholdsDict = dict()  # contains selected threshold and its value
    AllThresholdsDict = dict()  # contains all thresholds with values

    for ch_idx, ch in enumerate(channels):
        c = numpy_signal_channels[ch_idx, :]
        c -= c.mean()

        # obliczenie progu dla std
        C = c.copy()
        s = np.inf
        while C.std() < s:
            s = C.std()
            C = C[np.abs(C) < StdThr * s + np.median(C)]

        C_abs = np.abs(C)
        Thresholds = (AbsThr*10**-6, MedThr * np.median(C_abs), StdThr * s + np.median(C_abs))

        Threshold = min(Thresholds)  # wybieramy najostrzejszy (najniższy) próg
        ThresholdType = ['abs', 'med', 'std'][np.argmin(Thresholds)]

        print("{:>4}(outliers) - abs: {:.3g}, med: {:.3g}, std: {:.3g}, chosen: {}".format(
            *(ch,) + Thresholds + (ThresholdType,)))
        if Threshold <= 0:
            raise Exception("Threshold = {}\nYou must give some threshold in range (0,+oo)!".format(Threshold))

        ThresholdsDict[ch] = (Threshold, ThresholdType)
        AllThresholdsDict[ch] = Thresholds
        c_abs = np.abs(c)
        bad_samples[ch] = (c_abs > Threshold) * c_abs

        if outpath:
            fig, ax = plt.subplots(dpi=1000)
            bins = max(20, c_abs.size // 1000)
            c_abs[
                c_abs > 3 * Threshold] = 3 * Threshold  # trzeba przyciąć takie przypadki, bo potrafią rozwalić cały histogram

            ax.hist(c_abs,
                    bins=bins)  # rysowanie histogramu modułu sygnału - na OX jest potencjał w wart. bezwgl., na OY ilość zliczeń
            mx = ax.get_ylim()[1]
            for i in range(3):
                if Thresholds[i] < np.inf:
                    ax.plot([Thresholds[i], Thresholds[i]], [0, mx],
                            label=artifact_labels[i])  # rysowanie granic, które nie są nieskończone
            ax.plot([Threshold], [mx], "o",
                    label=ThresholdType)  # oznaczenie kropką granicy na podstawie której zdecydowano się usuwać epoki zaszumione artefaktami
            ax.set_title("{} for {}".format("Outliers", ch))
            ax.set_xlabel(r"Potential $[V]$")
            ax.set_ylabel("Frequency")
            # set_axis_fontsize(ax, fontsize * figure_scale / 8)
            # ax.legend(fontsize=fontsize * figure_scale / 8)
            ax.legend()
            fig.savefig(os.path.join(outpath, f"outliers_{ch}.png"))
            plt.close(fig)

    if full_output:
        return bad_samples, AllThresholdsDict
    else:
        return bad_samples, ThresholdsDict


def P2P(numpy_signal_channels, signal_info, channels, width, filters=[], StdThr=np.inf, AbsThr=np.inf, MedThr=np.inf,
        kind='P2P', full_output=False, outpath=""):
    '''
    Marcin Pietrzak 2017 from helper_functions\artifacts_mark.py.

    Subtracting min and max value in all signal bands from the width parameter and finding artifacts with this
    peak-to-peak value.

    Parameters
    ----------
    numpy_signal_channels: numpy.array
        EEG signal in numpy.array.
    signal_info: mne.Info
        Info of signal.
    channels: list
        List of channels.
    width: float
        Width (in seconds) of window.
    filters: list
        List of lists contain essential values to create filters for all artifacts.
    StdThr: float
        Multiply of std threshold.
    AbsThr: float
        Absolute threshold.
    MedThr: float
        Multiply of median threshold.
    kind: str
        Kind of the artifact (for example slow or slope).
    full_output: bool
        If True, returns bad_samples and AllThresholdsDict, else bad_samples and ThresholdsDict.
    outpath: str
        Path to directory with signal.

    Returns
    -------
    bad_samples: dict
        Samples over the selected threshold for each channel.
    ThresholdsDict: dict
        Type and value of select threshold for each channel.
    AllThresholdsDict: dict
        Values of all prepared thresholds for each channel.
    '''

    for filter in filters:
        print("Filtering...", filter)
        numpy_signal_channels = mgr_filter(numpy_signal_channels, signal_info, filter[0], filter[1], filter[2],
                                           filter[3], filter[4])

    Fs = int(signal_info["sfreq"])
    width = int(width * Fs)
    bad_samples = dict()
    ThresholdsDict = dict() # contains selected threshold and its value
    AllThresholdsDict = dict() # contains all thresholds with values
    for ch_idx, ch in enumerate(channels):
        c = numpy_signal_channels[ch_idx, :]

        mm = MiniMax(c, width)
        # przesuniecie o polowe szerokosci okna i ponowne obliczenie
        mm_2 = MiniMax(np.roll(c, -width // 2), width)

        # obliczenie progu dla std
        MM = np.concatenate([mm, mm_2])
        if outpath:
            mm_for_hist = MM.copy()
        s = np.inf
        while MM.std() < s:
            s = MM.std()
            MM = MM[MM < StdThr * s + np.median(MM)]

        Thresholds = (AbsThr*10**-6, MedThr * np.median(MM), StdThr * s + np.median(MM))
        Threshold = min(Thresholds)  # wybieramy najostrzejszy (najnizszy) prog
        ThresholdType = ['abs', 'med', 'std'][np.argmin(Thresholds)]
        print("{:>4}({}) - abs: {:.3g}, med: {:.3g}, std: {:.3g}, chosen: {}".format(
            *(ch, kind) + Thresholds + (ThresholdType,)))

        if Threshold <= 0:
            raise Exception("Threshold = {}\nYou must give some threshold in range (0,+oo)!".format(Threshold))
        bad_segment = mm * (
                    mm > Threshold)  # jednostronnie, bo interesują nas tylko przypadki o zbyt dużej, a nie zbyt małej amplitudzie
        bad_segment_2 = mm_2 * (mm_2 > Threshold)  # jednostronnie przesunięte

        ThresholdsDict[ch] = (Threshold, ThresholdType)
        AllThresholdsDict[ch] = Thresholds
        bad_samples[ch] = np.max(
            [BadSeg2BadSamp(bad_segment, width, c.size), BadSeg2BadSamp(bad_segment_2, width, c.size, width // 2)],
            axis=0)

        if outpath:
            fig, ax = plt.subplots(dpi=1000)
            bins = max(20, mm_for_hist.size // 100)
            mm_for_hist[
                mm_for_hist > 3 * Threshold] = 3 * Threshold  # trzeba przyciąć takie przypadki, bo potrafią rozwalić cały histogram
            ax.hist(mm_for_hist, bins=bins)
            mx = ax.get_ylim()[1]
            for i in range(3):
                if Thresholds[i] < np.inf:
                    ax.plot([Thresholds[i], Thresholds[i]], [0, mx], label=artifact_labels[i])
            ax.plot([Threshold], [mx], "o", label=ThresholdType)
            ax.set_title("{} for {}".format(kind, ch))
            ax.set_xlabel(r"Potential $[V]$")
            ax.set_ylabel("Frequency")
            # set_axis_fontsize(ax, fontsize * figure_scale / 8)
            # ax.legend(fontsize=fontsize * figure_scale / 8)
            ax.legend()

            fig.savefig(os.path.join(outpath, "{}_{}.png".format(kind, ch)))
            plt.close(fig)

    # plt.plot(c[bad_samples[ch] > 0])
    # plt.plot(bad_samples[ch][bad_samples[ch] > 0], 'r')
    # plt.title(ch)
    # plt.show()

    if full_output:
        return bad_samples, AllThresholdsDict
    else:
        return bad_samples, ThresholdsDict


def muscles(numpy_signal_channels, signal_info, channels, width, rng, filters=[], StdThr=np.inf, AbsThr=np.inf,
            MedThr=np.inf, full_output=False, outpath=""):
    '''
    Marcin Pietrzak 2017 from helper_functions\artifacts_mark.py.

    Finding muscle artifacts.

    Parameters
    ----------
    numpy_signal_channels: numpy.array
        EEG signal in numpy.array.
    signal_info: mne.Info
        Info of signal.
    channels: list
        List of channels.
    filters: list
        List of lists contain essential values to create filters for all artifacts.
    StdThr: float
        Multiply of std threshold.
    AbsThr: float
        Absolute threshold.
    MedThr: float
        Multiply of median threshold.
    full_output: bool
        If True, returns bad_samples and AllThresholdsDict, else bad_samples and ThresholdsDict.
    outpath: str
        Path to directory with signal.

    Returns
    -------
    bad_samples: dict
        Samples over the selected threshold for each channel.
    ThresholdsDict: dict
        Type and value of select threshold for each channel.
    AllThresholdsDict: dict
        Values of all prepared thresholds for each channel.
    '''

    for filter in filters:
        print("Filtering...", filter)
        numpy_signal_channels = mgr_filter(numpy_signal_channels, signal_info, filter[0], filter[1], filter[2],
                                           filter[3], filter[4])

    Fs = int(signal_info["sfreq"])
    width = int(width * Fs)
    bad_samples = dict()
    ThresholdsDict = dict()  # contains selected threshold and its value
    AllThresholdsDict = dict()  # contains all thresholds with values
    for ch_idx, ch in enumerate(channels):
        c = numpy_signal_channels[ch_idx, :]
        c -= c.mean()

        Pmsc, freqs = Power(c, width, rng, Fs)
        # przesunięcie o połowę szerokości okna i ponowne obliczenie
        Pmsc_2, freqs = Power(np.roll(c, -width // 2), width, rng, Fs)

        # obliczenie progu dla std
        PP = np.concatenate([Pmsc, Pmsc_2])
        if outpath:
            pp_for_hist = PP.copy()
        s = np.inf
        while PP.std() < s:
            s = PP.std()
            PP = PP[PP < StdThr * s + np.median(PP)]

        Thresholds = (AbsThr*10**-12, MedThr * np.median(PP), StdThr * s + np.median(PP))
        Threshold = min(Thresholds)  # wybieramy najostrzejszy (najnizszy) prog
        ThresholdType = ['abs', 'med', 'std'][np.argmin(Thresholds)]
        print("{:>4}(muscles) - abs: {:.3g}, med: {:.3g}, std: {:.3g}, chosen: {}".format(
            *(ch,) + Thresholds + (ThresholdType,)))

        if Threshold <= 0:
            raise Exception("Threshold = {}\nYou must give some threshold in range (0,+oo)!".format(Threshold))

        bad_segment = Pmsc * (
                    Pmsc > Threshold)  # jednostronnie, bo interesują nas tylko przypadki o zbyt dużej, a nie zbyt małej mocy
        bad_segment_2 = Pmsc_2 * (Pmsc_2 > Threshold)  # jednostronnie

        ThresholdsDict[ch] = (Threshold, ThresholdType)
        AllThresholdsDict[ch] = Thresholds
        bad_samples[ch] = np.max(
            [BadSeg2BadSamp(bad_segment, width, c.size), BadSeg2BadSamp(bad_segment_2, width, c.size, width // 2)],
            axis=0)

        if outpath:
            fig, ax = plt.subplots(dpi=1000)
            bins = max(20, pp_for_hist.size // 100)
            pp_for_hist[
                pp_for_hist > 3 * Threshold] = 3 * Threshold  # trzeba przyciąć takie przypadki, bo potrafią rozwalić cały histogram
            ax.hist(pp_for_hist, bins=bins)
            mx = ax.get_ylim()[1]
            for i in range(3):
                if Thresholds[i] < np.inf:
                    ax.plot([Thresholds[i], Thresholds[i]], [0, mx], label=artifact_labels[i])
            ax.plot([Threshold], [mx], "o", label=ThresholdType)
            ax.set_title("{} for {}".format("Muscles", ch))
            ax.set_xlabel(r"Power $[V^2]$")
            ax.set_ylabel("Frequency")
            # set_axis_fontsize(ax, fontsize * figure_scale / 8)
            # ax.legend(fontsize=fontsize * figure_scale / 8)
            ax.legend()

            fig.savefig(os.path.join(outpath, f"muscles_{ch}.png"))
            plt.close(fig)

    if full_output:
        return bad_samples, AllThresholdsDict
    else:
        return bad_samples, ThresholdsDict

def ForgetBirthAndDeath(bs, Fs, forget = 1.):
    '''
    Marcin Pietrzak 2017 from helper_functions\artifacts_mark.py.

    Deleting artifact variables on the begining and ending.

    Parameters
    ----------
    bs: dict
        Dict of detected artifacts on the specific channels.
    Fs: float
        Sampling frequency.
    forget: float
        How long (in seconds) we will delete possible artifacts at the beginning and end.

    Returns
    -------
    bs: dict
        Dict of detected artifacts on the specific channels without beginning and ending points.
    '''
    for ch in bs:
        bs[ch][:int(Fs * forget)] = 0.0
        bs[ch][-int(Fs * forget):] = 0.0
    return bs

def ArtTagsWriter(bad_samples, Fs, all_channels, MergWin, ArtName='artifact', Thresholds=np.inf, MinDuration=0.03):
    '''
    Marcin Pietrzak 2017 from helper_functions\artifacts_mark.py.

    Binding nearest points detected as artifacts.

    Parameters
    ----------
    bad_samples: dict
        Dictionary of detected artifacts on the specific channels.
    Fs: float
        Sampling frequency.
    all_channels: list
        List of channels.
    MergWin: float
        Length of window (in seconds) for checking nearest artifacts to connect.
    ArtName: str
        Name of artifact.
    Thresholds: dict or np.inf
        Type and value of select threshold for each channel.
    MinDuration: float
        Minimal length of detected artifact.

    Returns
    -------
    artifacts_description: list
        Full description of detected artifacts prepared for .tag format (list of dicts).
    artifacts_occurrence: dict
        Keys are names of artifacts, values are matrices of occurred artifacts in the specific channel and time.
        Each matrix has the shape of [number of channels, duration of signal] with two variables {0, 1}. 1 means that
        the artifact occurred in this specific point of time and space.
    '''

    artifacts_description = [] #all informations for file with .tag format
    artifacts_occurrence = np.zeros((len(bad_samples.keys()), len(list(bad_samples.values())[0]))) #matrix of 0 and 1, where 1 means artifact

    for ch_idx, ch in enumerate(bad_samples):
        if Thresholds == np.inf:
            threshold, threshold_type = np.inf, 'art'  # default
        else:
            threshold, threshold_type = Thresholds[ch]

        bads = bad_samples[ch]

        start = stop = 0.
        artifact = False
        value = -np.inf
        for i in range(bads.size):
            if bads[i]:
                if bads[i] > value:
                    value = bads[i]
                stop = i
                if not artifact:
                    start = i
                    artifact = True
            elif artifact:
                if i - stop > MergWin * Fs:
                    if stop - start < int(
                            MinDuration * Fs):  # symetryczne rozszerzenie tagu, żeby nie był węższy niż MinDuration
                        Ext = int(MinDuration * Fs) - stop + start
                        stop += Ext / 2
                        start -= Ext / 2
                    # if all_channels.index(ch) == -1:
                    #     print(all_channels)
                    #     print(ch)
                    #     raise IndexError("Bad channelNumber!")
                    tag_all = {'channelNumber': "{}".format(all_channels.index(ch)), 'start_timestamp': 1. * start / Fs, 'name': ArtName,
                           'end_timestamp': 1. * stop / Fs,
                           'desc': {'ch': ch, 'thr': "{:2.2}".format(threshold * 10**-6), 'val': "{:2.2}".format(value * 10**-6),
                                    'typ': threshold_type}}

                    artifacts_description.append(tag_all)
                    artifacts_occurrence[ch_idx, int(start):int(stop) + 1] = 1
                    value = -np.inf
                    artifact = False

    return artifacts_description, artifacts_occurrence


def ArtTagsSaver(artifacts_file_path, tags, rej_dict, file_name):
    '''
    Saving artifacts to .tag file.

    Parameters
    ----------
    artifacts_file_path: str
        Path to the directory for data about artifacts.
    tags: list
        Full description of detected artifacts prepared for .tag format.
    rej_dict: dict

    '''

    try:
        os.makedirs(os.path.dirname(artifacts_file_path))
    except OSError:
        pass

    writer = TagsFileWriter(os.path.join(artifacts_file_path, f"{file_name}___artifacts.tag"))
    for tag in tags:
        writer.tag_received(tag)
    writer.finish_saving(0.0)

    rej_dict_file = open(os.path.join(artifacts_file_path, f"{file_name}____artifact_rej_dict.json"), 'w')
    json.dump(rej_dict, rej_dict_file, indent=4, sort_keys=True)
    rej_dict_file.close()


def MiniMax(s, w):
    '''
    Marcin Pietrzak 2017 from helper_functions\artifacts_mark.py.

    Substract min and max value in the all parts of divided signal by w variable.

    Parameters
    ----------
    s: list or np.array
        Values of signal.
    w: float
        Width (in seconds) of window.

    Returns
    -------
    mm: np.array
        Array of the all min-to-max values in the all parts of signal divided by widnow.
    '''
    mm = []
    for i in range(0, s.size, w):
        m = s[i:i + w].max() - s[i:i + w].min()
        mm.append(m)
    return np.array(mm)

def BadSeg2BadSamp(bad_segments, width, size, shift = 0):
    y = np.zeros(size, dtype = bad_segments.dtype)
    for i in range(bad_segments.size):
        y[i * width + shift:(i + 1) * width + shift] = bad_segments[i]
    return y

def mgr_filter(numpy_signal_channels, signal_info, wp, ws, gpass, gstop, ftype):
    '''
    Marian Dovgialo 2019 from helper_functions\helper_functions.py

    Filtering signals.

    Parameters
    ----------
    numpy_signal_channels: numpy.array
        EEG signal in numpy.array.
    signal_info: mne.Info
        Info of signal.
    wp: float or list
        Passband of filter.
    ws: float or list
        Stopband of filter.
    gpass: float
        The maximum loss in the passband (dB).
    gstop: float
        The minimum attenuation in the stopband (dB).
    ftype: str
        The type of IIR filter to design.

    Returns
    -------
    numpy_signal_channels: numpy.array
        Filtered EEG signal in numpy.array.
    '''

    nyquist = float(signal_info['sfreq']) / 2.0
    try:
        wp = wp / nyquist
        ws = ws / nyquist
    except TypeError:
        wp = [i / nyquist for i in wp]
        ws = [i / nyquist for i in ws]
    b, a = ss.iirdesign(wp, ws, gpass, gstop, analog=0, ftype=ftype, output='ba')
    for i in range(int(numpy_signal_channels.shape[0])):
        numpy_signal_channels[i, :] = ss.filtfilt(b, a, numpy_signal_channels[i] - np.mean(numpy_signal_channels[i]))
    return numpy_signal_channels
