# 1. Introduction and installation

The main goals of this project are:
* preparing signals to gather epoched data which could be prepared in two possible routes:
    * epochs branch - event-related signal which demands tags and 
    * rest branch - continuous signal will be cut for segments
* collecting a wide range of data from epoched data for further analysis by the user
* analysing epoched data (in time domain, frequency domain and time-frequency domain) with qualitative and quantitative
methods

## 1.1 Technical info

The budzik_v2 program was prepared on Windows 10 - it should work on macOS or Linux, however it hasn't been tested. 

## 1.2 Installation on Windows

The budzik_v2 program could be install with two ways: old-school and poetry. We are recommending the poetry way to 
install our program because this is easier to handle but repairing possible bugs could be harder.

|                   |                                        old-school                                       |               poetry               |
|-------------------|-----------------------------------------------------------------------------------------|------------------------------------|
| Process           |                                          harder                                         |               easier               |
| Repairing bugs    |                                          easier                                         |               harder               |
| No. possible bugs |                                           more                                          |                less                |
| Helpfull links:   | PIP: https://pypi.org/project/pip/ <br>venv: https://docs.python.org/3/library/venv.html| Poetry: https://python-poetry.org/ |

### 1.2.1 The old-school way

#### 1.2.1.1 Requirements for installation

Download below programs in this order:

| Required things | Installation guides and files                                  |
|-----------------|----------------------------------------------------------------|
| git             | https://git-scm.com/download/win                               |
| Python 3.9.13   | https://www.python.org/downloads/release/python-3913/          |
| pip             | https://pip.pypa.io/en/stable/installation/                    |
| venv            | https://virtualenv.pypa.io/en/stable/installation.html#via-pip |


#### 1.2.1.2 Instalation steps

0. With Command Line open folder where you want to keep program (for example C:\path_to_program). More info about commands in Windows cmd you can find here https://learn.microsoft.com/en-us/windows-server/administration/windows-commands/windows-commands.

1. Download project to this folder.
```commandline
C:\path_to_program> git clone https://gitlab.com/szy_bocian/budzik_v2.git
```

2. Go to the program folder. You should have in C:\path_to_pogram\budzik_v2 few files (config.toml, README.md, requirements.txt and run.py) and two folders (budzik_v2 and budzik_v2.tests).
```commandline
C:\path_to_program> cd budzik_v2
```

3. Create virtual environment. After this line you should find new *venv* folder in the C:\path_to_program\budzik_v2.
```commandline
C:\path_to_program\budzik_v2> python -m venv C:\path_to_program\budzik_v2\venv
```

4. Activate virtual environment - after this step *(venv)* should appear before path in the line.
```commandline
C:\path_to_program\budzik_v2> C:\path_to_program\venv\Scripts\activate
```

5. Install requirements.
```commandline
(venv) C:\path_to_program\budzik_v2> pip install -r requirements.txt
```

### 1.2.2 Poetry way

#### 1.2.2.1 Requirements for installation

Download below programs in this order:

| Required things | Installation guides and files                                          |
|-----------------|------------------------------------------------------------------------|
| git             | https://git-scm.com/download/win                                       |
| Python 3.9.13   | https://www.python.org/downloads/release/python-3913/                  |
| Poetry          | https://python-poetry.org/docs/#installing-with-the-official-installer |

#### 1.2.2.2 Instalation steps

0. With Command Line open folder where you want to keep program (for example C:\path_to_program). More info about commands in Windows cmd you can find here https://learn.microsoft.com/en-us/windows-server/administration/windows-commands/windows-commands.

1. Download project to this folder.
```commandline
C:\path_to_program> git clone https://gitlab.com/szy_bocian/budzik_v2.git
```

2. Go to the program folder. You should have in C:\path_to_pogram\budzik_v2 few files (config.toml, README.md, requirements.txt and run.py) and two folders (budzik_v2 and budzik_v2.tests).
```commandline
C:\path_to_program> cd budzik_v2
```

3. Install requirements.
```commandline
C:\path_to_program> poetry install
```

4. Activate virtual environment - after this step *(budzik-v2-py3.9)* should appear before path in the line.
```commandline
C:\path_to_program\budzik_v2> poetry shell
```

## 1.3 First run of the program on Windows - inputs and outputs

### 1.3.1 Analysing example_real_data.raw

The budzik_v2 provides example real data to test first run. For the first test run after installation:

1. Open folder with project (for example C:\path_to_program\budzik_v2).

2. Activate virtual environment - (venv) or (budzik-v2-py3.9) should appear in the front of line. 
The running and using of the program is independent of the chosen installation way, so we will be using examples (venv) 
in the front of the line in the Command Line.

3. Start your first run:
```commandline
(venv) C:\path_to_program\budzik_v2> python run.py 
```

The program will analyse data from budzik_v2.tests\example_real_data\example_real_data.obci.raw by using main config. If
you want to use config from other place (in our example config from budzik_v2.tests\example_real_data\config.toml) use 
like this command:

```commandline
(venv) C:\path_to_program\budzik_v2> python run.py budzik_v2.tests\example_real_data\config_example_real_data.toml
```

However, both configs (main and config_example_real_data) are the same, so analysis will be the same.

Data was gathered by BrainTech device on standardized 10-20 cap in the oddball paradigm from adult. Basic parameters from
this signal you can check in the table below:

| Variable | Info |
|---|---|
| Date | 2016-07-18 08:43:53 UTC |
| Number of channels | 30 (22 EEG, 8 technical) |
| Electrodes layout | 10-20 |
| Sampling freq. | 512 Hz |
| Length | ~659.27 s |
| Tags | target (80) and nontarget(320) |
| Filters | None |
| Baseline | None |
| Reference | None |

**IMPORTANT:** You have to activate the virtual environment in every "first run of the day" - (venv) or (budzik-v2-py3.9) 
have to be in the front of the line, otherwise, budzik_v2 will work badly or doesn't work at all.

### 1.3.2 Analysing example_simulated_data-raw.fif

If you want, you can check other signal for you first run - simulated signal. 

1. Open folder with project (for example C:\path_to_program\budzik_v2).

2. Activate virtual environment - (venv) or (budzik-v2-py3.9) should appear in the front of line. 
The running and using of the program is independent of the chosen installation way, so we will be using examples (venv) 
in the front of the line in the Command Line.

3. Start run for epochs:
```commandline
(venv) C:\path_to_program\budzik_v2> python run.py budzik_v2.tests/example_simulated_data/config_simulated_data_epochs.toml
```

4. Start run for rest:
```commandline
(venv) C:\path_to_program\budzik_v2> python run.py budzik_v2.tests/example_simulated_data/config_simulated_data_rest.toml
```

Basic information about signal is below.

| Variable | Info |
|---|---|
| Date | None |
| Number of channels | 3 (3 EEG) |
| Electrodes layout | 10-20 |
| Sampling freq. | 512 Hz |
| Length | 80 s |
| Tags | target (9) and nontarget(21) |
| Filters | None |
| Baseline | None |
| Reference | None |

Signal is idealized and has three channels: C3, C4 and Cz. There is Gaussian noise along the entire length of the signal, 
sine at the beginning, and Gabor curves in events(target and nontarget). Each channel has slightly different values for 
each of these 3 components of the simulated signal. For more info check out this table:

| Component | C3 | C4 | Cz |
|---|---|---|---|
| Gaussian Noise | N(0, 0.09) along signal | N(0, 0.16) along signal | N(0, 0.25) along signal |
| Sine | 8Hz from 0s to 16s | 10Hz from 0s to 16s | 12Hz from 0s to 16s |
| Gabor Curve - nontarget | 3Hz and N(random(0.2s, 0.4s), 0.05) | 3Hz and N(random(0.2s, 0.4s), 0.05) | 3Hz and N(random(0.2s, 0.4s), 0.05) |
| Gabor Curve - target | 3Hz and N(0.3, 0.05) and 4 times larger amplitude than Cz | 3Hz and N(0.3, 0.05) and 3 times larger amplitude than Cz | 3Hz and N(0.3, 0.05) |

More info you can find in signal_seed.toml in the same folder.

### 1.3.3 Output files and paths

After running program, you should get folders for output data like this:

| Folder                 | Description                                                         |
|------------------------|---------------------------------------------------------------------|
| artifacts_detection    | output from removing EOG artifacts with ICA and detecting artifacts |
| clean_signals          | clean signals (filtered, montaged, cutted etc.) from tags and rest  |
| filters_plot           | plots about filters                                                 |
| frequency_analyse      | PSD plots for tags and rest                                         |
| stats                  | basic stats gathered/calculated from epochs                         |
| time_analyse           | basic plots + cluster test in time domain                           |
| time_frequency_analyse | TF maps calculated with STFT or WT                                  |

Path to output folder depends on the *output_path* and *overwrite_output* in config file. If output_path is None, then 
output folder will be in the location of input data, but the name of the output folder will be the same: 
*budzik_output__name_of_input_file*. If overwrite_output is false, then the program will create new folders for every 
run in this pattern: budzik_output__name_of_input_file\YYYYMMDD_HHMMSS_usedpath. If overwrite_output is true, then output 
folders will be in budzik_output__name_of_input_file and they will overwrite in every run. 

For example, we want to prepare epochs analysis for 
C:\path_to_program\budzik_v2\budzik_v2.tests\example_real_data\example_real_data.obci.raw, we don't want to overwrite our output, 
we want output in the same location, and we run program in 15.03.2024 at 13:15:30, then the output path will be like 
this: C:\path_to_program\budzik_v2\budzik_v2.tests\example_real_data\budzik_output__example_real_data.obci\20240315_131530_epochs.

# 2. Using program

**IMPORTANT:** Please be present during your first of budzik_v2 after changing config file. If you provide wrong vaule for
variables, program will shut down and should describe what you prepare wrong.

## 2.1 Config file - basic info and variables

The *config.toml* file is a heart of our program - this is a file with all the parameters necessary to prepare signals 
and conduct the analysis. The example config is located in the main folder of budzik_v2. 

The most important "administrative" parameters are:

| Variable | Description | Type |
|---|---|---|
| input_path | The path to the data – file or directory. | str |
| output_path | The path for directory for output files – if None, program will take location of the input data. | str or None |
| prepare_rest_or_epochs | Which path do you want to choose - "rest" (analyse resting signal) or "epochs" (analyse event related signal) | str |
| overwrite_output | Do you want to overwrite output files? If no (False), with every start of the program, will be created new folder with outputs in the output_path. | bool |
| prepare_eeg_artifacts | Do you want to prepare eeg preanalyse like: resampling + finding bad channels + montage + rereference + filtering + finding artifacts with ICA + finding artifacts with alghoritms (outliers, slopes, muscles) + baseline? | bool |
| prepare_video_artifacts | Do you want to find when examined not looking? | bool |
| prepare_time_analysis | Do you want to prepare time analysis? | bool |
| prepare_frequency_analysis | Do you want to prepare frequency analysis? | bool |
| prepare_time_frequency_analysis | Do you want to prepare time-frequency analysis? | bool |

More info about all parameters is in Appendix A or the config file. 

### 2.1.1 Preanalyse

| Variable | Description | Type | Subblock |
|---|---|---|---|
| baseline | Baseline – mean, median or nothing | str or None |  |
| electrodes_layout | Layout of electrodes provided by MNE. | str |  |
| re_reference | Pick reference channels for signal, possible inputs: ["ch1", "ch2", "ch3", ...] - will take average of provided electrodes, "average" - will take average of all the electrodes, None - return original signal without rereferencing | list or None |  |
| resample_freq | Resampling frequency [in Hz]. | int or float |  |
| choose_bad_channels | How do you want to choose bad channels. Possible choices: "manually" (checking channels on the window with plots with signals which will pop out), "auto" (bad channels will be choose automatic badchs_* params) and "both" (sum of sets with channels from manually and auto decisions). | str | choosing_channels |
| dropped_channels | List of channels to drop – if None, no channels will be dropped. | list or None | choosing_channels |
| selected_channels | List of selected channels – of “all”, all channels will be taken (except those dropped). | list or “all” | choosing_channels |
| highpass_name | Name of defined highpass filter – possible: “highpass_01hz” (from 0.1 Hz), “highpass_05hz” (from 0.5 Hz), and “highpass_1hz” (from 1 Hz)  or None (without filter). | str or None | filters |
| lowpass_name | Name of defined lowpass filter – possible names: “lowpass_10hz” (to 10 Hz), “lowpass_20hz” (to 2- Hz), and “lowpass_30hz” (to 30 Hz) or None (without filter). | str or None | filters |
| stopband_name | Name of defined stopbad filter – possible: “stopband_50hz” (without 50 Hz) and “stopband_60hz” (without 60 Hz) or None (without filter). | str or None | filters |
| artifact_threshold | Threshold (from 0 to 1) for classifying epoch with too many points classified as artefacts | float | artifacts |
| choose_bad_ICA_source | How do you want to choose bad sources in ICA. possible choices: "manually" (checking source on the window with plots with sources which will pop out), "auto" (bad sources will be choose automatic score_* params) and "both" (sum of sets with sources from manually and auto decisions). | str | ICA_EOG |

### 2.1.2 Epoched data

Each branch (rest and epochs) have different subblock in the config file. Please check the *prepare_rest_or_epochs* 
variable and prepare variables in proper subblock. However, you can have this two subblocks in the same time without 
errors.

| Variable | Description | Type | Subblock |
|---|---|---|---|
| rest_duration | Time (in seconds) from signal which will be taken to the rest analysis. | list | rest |
| window_length | Length (in seconds) of window used for cutting rest signal for epochs | int or float | rest |
| start_offset | Starting point of epoch (in seconds). | int or float | epochs |
| stop_offset | Ending point of epoch (in seconds). | int or float | epochs |
| start_baseline | Starting point of the baseline of the epoch (in seconds) or None.  | int or float or None | epochs |
| stop_baseline | Ending point of the baseline of the epoch (in seconds) or None.  | int or float or None | epochs |

### 2.1.3 Analyse

For rest branch you can only prepare analysis in frequency domain and for epochs branch you can prepare analysis in time, 
frequency and time-frequency analysis. Please check *prepare_time_analysis*, *prepare_frequency_analysis*, and 
*prepare_time_frequency_analysis* variables before you prepare proper subblocks for analysis.

| Variable | Description | Type | Subblock |
|---|---|---|---|
| estimation_method | Method to estimate frequency domain (spectral density) of signal – “welch” or “multitaper”. | str | frequency_domain |
| window_duration | Window duration (in seconds). | int or float | time_frequency_domain.stft |
| window_overlap | Percent (from 0 to 1) of window will overlap on the next window. | float | time_frequency_domain.stft |
| wavelet_name | Wavelet name. | str | time_frequency_domain.wt |
| wavelet_f_lim_n | How many frequencies for wavelets do you want between f_lim_start and f_lim_stop frequency? | int | time_frequency_domain.wt |
| wavelet_parameters | Other parameters necessary for wavelet. | dict | time_frequency_domain.wt |

**IMPORTANT:** You can prepare STFT analysis with every window defined by scipy.stats (more: https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.get_window.html#scipy.signal.get_window),
but only wavelet *morlet2* during WT - other wavelets will be available in the next versions. 

## 2.2 Running analysis

After preparing the config file, we can run program. In *input_path* you can assign one file:

```python
input_path = "C:\\folder_with_signals\\raw_signal.raw"
```

or folder with files:

```python
input_path = "C:\\folder_with_signals"
```

In the second situation budzik_v2 will find every file with formats: .raw (BrainTech), .vhdr (Brain Vision), .fif and .set.
Of course user have to provide every necessary file from these solutions.

After this, you can run budzik_v2 with this command:

```commandline
(venv) C:\path_to_program\budzik_v2> python run.py 
```

During running program you could get a lot of info in log. Every message prepared by budzik_v2 will start with: MESSAGE,
ERROR, START or STOP. MESSAGE shows very often - will give you metadata about prepared signals, most important 
findings during analysing, or general info about taking next steps of program. START and STOP show only one time, and 
they give information about time of running program. If you get ERROR, budzik_v2 will shut down and describe what went 
wrong (wrong files for signals, wrong values for variables in config and so on). Every other message in the logs will be
from used libraries. 

## 2.3 Checking video without signal

If you want to prepare tags where the subject was not looking, use this command for video_example in C:\video_path:

```commandline
(venv) C:\path_to_program\budzik_v2> python budzik_v2\preanalyse\facetag.py C:\path_to_video\example_video.mp4
```

**IMPORTANT**: Video should be in the mp4 format.


# FAQ - Frequently Asked Questions

## Generally questions

### How do I know, I have installed XYZ?

#### Python
Write command:
```commandline
C:\path_to_program\budzik_v2> python --version
```

#### Git
Write command:
```commandline
C:\path_to_program\budzik_v2> git --version
```

#### pip
Write command:
```commandline
C:\path_to_program\budzik_v2> pip --version
```

#### venv
Write command:
```commandline
C:\path_to_program\budzik_v2> pip show virtualenv
```
#### poetry
Write command:
```commandline
C:\path_to_program\budzik_v2> poetry --version
```

### How to exit virtual environment?

Virtual environment from old-school way:

```commandline
(venv) C:\path_to_program\budzik_v2> venv\Scripts\deactivate.bat
```

Virtual environment from poetry way:

```commandline
(budzik-v2-py3.9) C:\path_to_program\budzik_v2> exit
```

### How to handle ModuleNotFoundError or NameError?

Probably you don't activate virtual environment. Please check front of your line in Command Line or Power Shell - do 
you have (venv) or (budzik-v2-py3.9)? If not, please look into 1.2.1.2 or 1.2.2.2.

## Questions about reading files

### The budzik_v2 doesn't want work with my file - why?

The budzik_v2 can prepare data from this formats:

| File convention | File in input-path | Necessery files |
|---|---|---|
| BrainTech | .raw | .raw, .tag and .xml |
| Brain Vision  | .vhdr | .vhdr, .vmrk and .eeg |
| fif | .fif | .fif |
| EEGLAB | .set | .set |

Do you have proper data for your format? Do you write good path in your config?

### How to add new format?

Every function that reads a file is defined in budzik_v2\read_files\read_files.py. The function has to get the path to 
the main file (for example for BrainTech solution is .raw and for BrainVision is .vhdr) for input and should give two 
things: signal in mne.io.array.array.RawArray (variable mne_signal) and dictionary like this 
{'event_name_1': 1, 'event_name_2': 2, ...} (variable events_desc_id). The mne_signal variable should have EEG channels 
and one stimulus channel "STIM" with information about occurring in the time domain of events - 0 for no event and an 
integer number which is the id number corresponding with the name in events_desc_id.

In the final step, you have to describe in *signal_formats* in budzik_v2\read_files\read_files.py main format and the name of
which correspond to its function. This is crucial step, because when budzik_v2 gets path to directory in input_path, 
it finds all files with defined formats in signal_formats (check find_file_paths function in budzik_v2\read_files\read_files.py),
and use function assign to this format.

## Questions about preanalyse

### How to add own filters?

Check out *filters_dict* in budzik_v2\read_files\read_files.py - this dictionary has all filters. You can add other 
filter the same way as others. Here are some rules:

1. Each filter should start with: "highpass_", "lowpass_" or "stopband_". The budzik_v2 program has only three types of 
   filters, and your new filter should be one of them. For example you can have "lowpass_rabbit_filt" but now "rabbit_filr".
2. Names of filters will be keys, and their variables will dictionaries with values of filters defined by scipy.stats (more
   https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.iirdesign.html).
   
### My events don't have "target" and "nontarget" in their names - how can I prepare own tags?

The config file is set up to automatic finding "target" and "nontarget" tags by searching this words in names of provided 
events. This is possible with *AUTO* varibale in *epochs.tags* subblock. However, you can declare your own tags in the
same *epochs.tags* subblock. Delete *AUTO* variable and prepare lines like this:

```python
tag_name = ['eventname1', 'eventname2', ...]
```

For example if you have EEG signal with events: picture_1, picture_2, picture_A, picture_B, and picture_blank events. 
You can prepare tags like this:

```python
pic_num = ['picture_1', 'picture_2']
pic_letter = ['picture_A', 'picture_B']
pic_trigger = ['picture_blank']
```

**IMPORTANT:** Events names must be provided with signals.

### Where can I find prepared epoched data after preanalyse?

In *saved_signals* folder.

## Questions about analyse

### Every epoch from signal was deleted by preanalyse - what should I do?

Check variables below - maybe they are too strict for your data?

| Variable | Description | Type | Subblock |
|---|---|---|---|
| artifact_threshold | Threshold (from 0 to 1) for classifying epoch with too many points classified as artefacts | float | artifacts |
| reject_dict | Reject dictionary, if peak-to-peak of min-max in epoch is greater than value in reject_dict, then epoch will be rejected. | dict | epochs | 
| flat_dict | Reject dictionary, if peak-to-peak of min-max in epoch is smaller than value in reject_dict, then epoch will be rejected. | dict | epochs |

Also, you can check *stat_table* in *stats folder*. In this file you can check which channels and epochs were classified
as bad, how many points were classified as artifact and other basic stats about signals (for example min and max values
in time domain and frequency domain).

### What does mean columns in *stat_table*?

| Column | Description |
|---|---|
| id | Row id. |
| tag | Tag. |
| channel | Channel's name. |
| t_start, t_stop | Start and stop of segment regarding to saved signals (for epochs in default config signal before first tag is deleted). |
| bad_segment | Whether this segment has been classified as bad relative to the artifact detection program used? |
| bad_channel | Whether this channel has been classified as bad by program or user? |
| *_arts | Percent of points classified as artifacts in segment. |
| *_amp | Statistics collected from the segment in the time domain (amplitude [V]). |
| *_abs_amp | Statistics collected from the segment's absolute value in the time domain (absolute amplitude [V]). |
| *_spec_dens | Statistics collected from the segment in the frequency domain (spectral density [V^2]). |

### Plots from cluster test don't have clusters, but logs told me something different. Where can I find info about clusters?

In .txt file in *time_analyse* folder after analysis.

### Time-Frequency maps have two variants - TF and ERP. What does it mean?

**TF maps:** The budzik_v2 calculate map for each segment. It then calculates the average.

**ERP maps:** The budzik_v2 calculate ERPs (average in time domain) for each segment. it then calculates time-frequency 
map from ERP.

### My wavelet doesn't work - why?

At the moment we only have the *morlet2* wavelet prepared -  in the next versions we will provide other wavelets.

# Authors

* Szymon Bocian @szy_bocian
* Anna Duszyk-Bogorodzka @Duszyk
* Piotr Durka @durka

# Appendix A - table with config variables grouped by subblocks

| Variable | Description | Type | Default  | Subblock | Example | Comment |
|---|---|---|---|---|---|---|
| input_path | The path to the data – file or directory. | str |  |  | File: C:\\Users\\User1\\raw_data.raw<br>Directory: C:\\Users\\User1\\eeg_data |  |
| output_path | The path for directory for output files – if None, program will take location of the input data. | str or None |  |  | Directory: C:\\Users\\User1\\output_data |  |
| prepare_rest_or_epochs | Which path do you want to choose - "rest" (analyse resting signal) or "epochs" (analyse event related signal) | str |  |  |  | Connected with subblocks “rest” and “epochs” |
| overwrite_output | Do you want to overwrite output files? If no (False), with every start of the program, will be created new folder with outputs in the output_path. | bool |  |  | true or false | Connected with variable: “output_path”. |
| prepare_eeg_artifacts | Do you want to prepare eeg preanalyse like: resampling + finding bad channels + montage + rereference + filtering + finding artifacts with ICA + finding artifacts with alghoritms (outliers, slows, slopes, muscles) + baseline? | bool |  |  | true or false | Connected with subblocks: “choosing_channels”, “filters”, “artifacts” and “ICA_EOG”. |
| prepare_video_artifacts | Do you want to find when examined not looking? | bool |  |  | true or false | Connected with subblock “artifacts”.<br>The video file must be in the signals folder. |
| prepare_time_analysis | Do you want to prepare time analysis? | bool |  |  | true or false | Connected with subblock: “time_domain”. |
| prepare_frequency_analysis | Do you want to prepare frequency analysis? | bool |  |  | true or false | Connected with subblock: “frequency_domain”. |
| prepare_time_frequency_analysis | Do you want to prepare time-frequency analysis? | bool |  |  | true or false | Connected with subblock: “time_frequency_domain”. |
| baseline | Baseline – mean, median or nothing | str or None |  |  |  |  |
| electrodes_layout | Layout of electrodes provided by MNE. | str |  |  | 10-20 cap: “standard_1020 | All layouts are defined here: https://mne.tools/dev/auto_tutorials/intro/40_sensor_locations.html#sphx-glr-auto-tutorials-intro-40-sensor-locations-py |
| re_reference | Pick reference channels for signal, possible inputs: ["ch1", "ch2", "ch3", ...] - will take average of provided electrodes, "average" - will take average of all the electrodes, None - return original signal without rereferencing | list or None |  |  | Cz reference: [“Cz”]<br>Ears reference: [“M1”, “M2”] | If you give channels, check if this channels exist in signal.  |
| resample_freq | Resampling frequency [in Hz]. | int or float | 512 |  |  |  |
| cover_time | How many signal [in seconds] around group of events will you take for analysis. If cover_time = None and threshold_time = None, all signal before first tag (rest or event) minus 5 seconds will be not taken for the rest of the analysis. | float or None | None |  |  | Connected with variable: “threshold_time”.<br><br>For more, check budzik_v2/preanalyse/channels_and_signal.py/pick_signal. |
| threshold_time | Checking gaps between groups of events, if the gap is longer than this parameter, groups won't be connected - if cover_time = None and threshold_time = None, all signal will before first tag (rest or event) minus 5 seconds will be not taken for the rest of the analysis. | float or None | None |  |  | Connected with variable: “cover_time”.<br><br>For more, check budzik_v2/preanalyse/channels_and_signal.py/pick_signal. |
| show_signal | Do you want see signal after selecting, dropping and finding bad channels? Signal will be showed with default filters to pass 1-30 Hz. | bool |  | choosing_channels | true or false |  |
| choose_bad_channels | How do you want to choose bad channels. Possible choices: "manually" (checking channels on the window with plots with signals which will pop out), "auto" (bad channels will be choose automatic badchs_* params) and "both" (sum of sets with channels from manually and auto decisions). | str |  | choosing_channels |  | Connected with variable: “badchs_params”. |
| dropped_channels | List of channels to drop – if None, no channels will be dropped. | list or None |  | choosing_channels | Dropping possible technical channels: ["Audio", "Sample_Counter", "Photo", "Driver_Saw"] | Connected with variable: “selected_channels”. |
| selected_channels | List of selected channels – of “all”, all channels will be taken (except those dropped). | list or “all” |  | choosing_channels | Selecting only central electrodes: [“Cz”, “C3”, “C4”] | Connected with variable: “dropped_channels”. |
| badchs_params | Parameters to find bad channels.  | dict | {<br>slow_oscilations = [1, [2, 10], "-", [1e3, 1e4, 1e5]], <br>Noise_power = [1, [52, 98], "-", [2e1, 2e2, 2e3]]<br>} | choosing_channels |  | Check ValidateChannels_Noise in budzik_v2/preanalyse/channels_and_signal. |
| show_filt | Show filter's plots. | bool |  | filters | true or false |  |
| plot_filt | Save filter's plots. | bool |  | filters | true or false |  |
| highpass_name | Name of defined highpass filter – possible: “highpass_01hz” (from 0.1 Hz), “highpass_05hz” (from 0.5 Hz), and “highpass_1hz” (from 1 Hz)  or None (without filter). | str or None |  | filters |  |  |
| lowpass_name | Name of defined lowpass filter – possible names: “lowpass_10hz” (to 10 Hz), “lowpass_20hz” (to 2- Hz), and “lowpass_30hz” (to 30 Hz) or None (without filter). | str or None |  | filters |  |  |
| stopband_name | Name of defined stopbad filter – possible: “stopband_50hz” (without 50 Hz) and “stopband_60hz” (without 60 Hz) or None (without filter). | str or None |  | filters |  |  |
| artifact_threshold | Threshold (fro 0 to 1) for classifying epoch with too many points classified as artefacts | float |  | artifacts |  |  |
| rejection_parameters | Dictonary with parameters for slopes, outliers and muscles. | dict | {<br>    SlopeWindow = 0.0704, <br>    MusclesWindow = 0.5, <br>    OutliersMergeWin = .1,  <br>    MusclesFreqRange = [40, 90], <br>    SlopesAbsThr = np.inf,  <br>    SlopesStdThr = 7,  <br>    SlopesMedThr = 6,  <br>    OutliersAbsThr = np.inf, <br>    OutliersStdThr = 7,  <br>    OutliersMedThr = 13,  <br>    MusclesAbsThr = np.inf,<br>    MusclesStdThr = 7,  <br>    MusclesMedThr = 5,<br>} | artefacts |  |  |
| show_ICA_sources | Show ICA sources during process. | bool |  | ICA_EOG | true or false |  |
| choose_bad_ICA_source | How do you want to choose bad sources in ICA. possible choices: "manually" (checking source on the window with plots with sources which will pop out), "auto" (bad sources will be choose automatic score_* params) and "both" (sum of sets with sources from manually and auto decisions). | str |  | ICA_EOG |  |  |
| ica_method | Method to calculate ICA - possible: "fastica", "infomax" or "picard". | str | “infomax” | ICA_EOG |  |  |
| n_components | Possible variables: "all" (program will take number of ICA components equal to number of channels), integer number (how many ICA components you will get - it must be equal or smaller than number of channels) or float number (from range [0, 1] and will select the smallest possible number of components required to explain cumulative variance of the signal). | “all” or int or float | “all” | ICA_EOG |  | More about float choice: https://mne.tools/1.3/generated/mne.preprocessing.ICA.html. |
| eog_channels_names | Names of eog channels - will be taken from possible channels (after dropped by the user and found bad channels). | list | ["Fp1", "Fp2", "Fpz"] | ICA_EOG |  | More about creating EOG events: https://mne.tools/1.3/generated/mne.preprocessing.create_eog_epochs.html. |
| score_correlation_treshhold | Threshold score for ICA. | float | 0.5 | ICA_EOG |  | More about checking score for ICA: https://mne.tools/1.3/generated/mne.preprocessing.ICA.html#mne.preprocessing.ICA.score_sources. |
| score_l_freq | Frequency from high pass filter used in calculating score from ICA sources with one of the eog_channels_names. | int or float | 1 | ICA_EOG |  | More about checking score for ICA: https://mne.tools/1.3/generated/mne.preprocessing.ICA.html#mne.preprocessing.ICA.score_sources. |
| score_h_freq | Frequency from low pass filter used in calculating score from ICA sources with one of the eog_channels_names. | int or float | 9 | ICA_EOG |  | More about checking score for ICA: https://mne.tools/1.3/generated/mne.preprocessing.ICA.html#mne.preprocessing.ICA.score_sources. |
| rest_duration | Time (in seconds) from signal which will be taken to the rest analysis. | list |  | rest | Signal from 10 s to 2 min will be cutted for rest events: [10, 120]  |  |
| window_length | Length (in seconds) of window used for cutting rest signal for epochs | int or float |  | rest | Rest window will be 2.5 s length: 2.5 |  |
| start_offset | Starting point of epoch (in seconds). | int or float |  | epochs |  |  |
| stop_offset | Ending point of epoch (in seconds). | int or float |  | epochs |  |  |
| start_baseline | Starting point of the baseline of the epoch (in seconds) or None.  | int or float or None |  | epochs |  |  |
| stop_baseline | Ending point of the baseline of the epoch (in seconds) or None.  | int or float or None |  | epochs |  |  |
| AUTO | Automatic finding position of “target” and “nontarget” tags. | bool |  | epochs.tags |  |  |
| reject_dict | Reject dictionary, if peak-to-peak of min-max in epoch is greater than value in reject_dict, then epoch will be rejected. | dict | {<br>eeg = 500e-6<br>} | epochs |  |  |
| flat_dict | Reject dictionary, if peak-to-peak of min-max in epoch is smaller than value in reject_dict, then epoch will be rejected. | dict | {<br>eeg = 1e-6<br>} | epochs |  |  |
| step_down_p | To perform a step-down-in-jumps test, pass a p-value for clusters to exclude from each successive iteration. Default is zero, perform no step-down test (since no clusters will be smaller than this value). Setting this to a reasonable value, e.g. 0.05, can increase sensitivity but costs computation time. | float |  | time_domain.cluster_test |  |  |
| n_permutations | The number of permutations to compute. | int |  | time_domain.cluster_test |  |  |
| estimation_method | Method to estimate frequency domain (spectral density) of signal – “welch” or “multitaper”. | str |  | frequency_domain |  | More https://mne.tools/1.3/generated/mne.Epochs.html#mne.Epochs.compute_psd. |
| f_lim_start | Starting frequency in the frequency domain on the TF maps - if None take frequency of highpass filter saved in signal.info. | int or float |  | time_frequency_domain |  |  |
| f_lim_end | Ending frequency in the frequency domain on the TF maps - if None take frequency of highpass filter saved in signal.info. | int or float |  | time_frequency_domain |  |  |
| window_name | Window name. | str |  | time_frequency_domain.stft |  | Window provided by scipy.signal package - more: https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.get_window.html#scipy.signal.get_window. |
| window_duration | Window duration (in seconds). | int or float |  | time_frequency_domain.stft |  |  |
| window_overlap | Percent (from 0 to 1) of window will overlap on the next window. | float |  | time_frequency_domain.stft |  |  |
| wavelet_name | Wavelet name. | str |  | time_frequency_domain.wt |  | Wavelet provide by scipy.signal package - more: https://docs.scipy.org/doc/scipy/reference/signal.html#wavelets. |
| wavelet_f_lim_n | How many frequencies for wavelets do you want between f_lim_start and f_lim_stop frequency? | int |  | time_frequency_domain.wt |  |  |
| wavelet_parameters | Other parameters necessary for wavelet. | dict |  | time_frequency_domain.wt |  | More: https://docs.scipy.org/doc/scipy/reference/signal.html#wavelets |
| cmap | Color map for heatmaps of stft and wt. | str | “jet” | time_frequency_domain |  | More: https://matplotlib.org/stable/users/explain/colors/colormaps.html. |
| fontsize | Size of font on TF maps. | int | 26 | time_frequency_domain |  |  |
| figurescale | Scale of figures on TF maps. | int | 10 | time_frequency_domain |  |  |
