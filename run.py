from budzik_v2.main import main
from datetime import datetime
import sys

if __name__ == "__main__":

    print(f"\nSTART: {datetime.now().strftime('%d/%m/%Y %H:%M:%S')}\n")

    # checking config path
    if len(sys.argv) == 1:
        print("MESSAGE: Preparing signal with default configuration file prepared by authors (it will be in the main folder of the project).")
        main("config.toml")
    else:
        print(f"MESSAGE: Preparing signal with configuration file from {sys.argv[1]}.\n")
        main(sys.argv[1])

    print(f"STOP: {datetime.now().strftime('%d/%m/%Y %H:%M:%S')}\n")